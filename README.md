# README #

DACCAD is a computer-assisted design software for DNA computing systems.
It allows the quick creation and edit of DNA toolbox chemical reaction networks
as well as parameter optimization.

A full description of the capabilities of DACCAD can be found in the article
*Computer-assisted design for scaling up systems based on DNA reaction networks*
Aubert *et al*, Journal of The Royal Society Interface, 2014

Additional functionalities (enzymatic saturation and pseudo templates) are described in the article
*Designing dynamical molecular systems with the PEN Toolbox*
Aubert-Kato and Cazenille, New Generation Computing, 2020

A tutorial on using DACCAD can be found [here](http://web.is.ocha.ac.jp/~naubertkato/DACCAD_Tutorial.pdf)

## Building code ##

You can download the required libraries and build the code automatically by running:

```
./gradlew dist
```

This command will produce an executable jar file in the build/libs folder.
Compiled java classes can be directly accessed in the build/classes folder.

Required libraries:

* commons-math3.jar
* commons-collections4.jar
* jcommon-1.0.jar
* jfreechart-1.0.jar
* jung-api-2.1.1.jar
* jung-graph-impl-2.1.1.jar
* jung-visualization-2.1.1.jar

It was created by Nathanael Aubert-Kato.

