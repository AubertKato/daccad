#!/bin/bash

CMS_OPTS='-XX:+CMSParallelRemarkEnabled -XX:+CMSClassUnloadingEnabled -XX:CMSInitiatingOccupancyFraction=75 -XX:+UseCMSInitiatingOccupancyOnly'

JAVA_OPTS='-Xmx4024m -Xms256m -XX:+HeapDumpOnOutOfMemoryError -XX:+OptimizeStringConcat  -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=1 -Djava.awt.headless=false -server -Dlog.mode=production'

version=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}')
#CMS options got removed in version 14
if [[ "$version" < "14" ]];
then
    JAVA_OPTS+=$CMS_OPTS
fi

# Quote because path may contain spaces
if [ -h $0 ]
then
  ROOT_DIR="$(cd "$(dirname "$(readlink -n "$0")")" && pwd)"
else
  ROOT_DIR="$(cd "$(dirname $0)" && pwd)"
fi
cd "$ROOT_DIR"

CLASS_PATH="$ROOT_DIR/build/dist/lib/*:config:$ROOT_DIR/build/classes/java/main:"

exec java $JAVA_OPTS -cp $CLASS_PATH $@
