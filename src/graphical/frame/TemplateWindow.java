package graphical.frame;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ToolTipManager;

import model.OligoGraph;
import model.PseudoTemplateGraph;
import model.SlowdownConstants;

public class TemplateWindow<E> extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final E template;
	final OligoGraph<?,E> graph;
	
	double[] custValues = null;

	TemplateWindow(E template, OligoGraph<?,E> graph){
		super(template.toString());
		this.template = template;
		this.graph = graph;
		
		initComponent();
	}

	protected void initComponent() {
		
		ToolTipManager.sharedInstance().setDismissDelay(Short.MAX_VALUE);
    	ToolTipManager.sharedInstance().setInitialDelay(0);
		
		JPanel panelup = new JPanel();
		JPanel panelmiddle = new JPanel();
		JPanel paneldown = new JPanel();
		panelup.setLayout(new BoxLayout(panelup,BoxLayout.LINE_AXIS));
		panelmiddle.setLayout(new BoxLayout(panelmiddle,BoxLayout.PAGE_AXIS));
		paneldown.setLayout(new BoxLayout(paneldown, BoxLayout.LINE_AXIS));
		
		Container content = this.getContentPane();
		content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
		
		JLabel concLab = new JLabel("Template concentration: ");
		final JTextField conc = new JTextField(""+graph.getTemplateConcentration(template));
		conc.setPreferredSize(new Dimension(100,18));
		conc.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				setTemplateValue(Double.parseDouble(conc.getText()));
			}
			
		});
		conc.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {
				
				
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				setTemplateValue(Double.parseDouble(conc.getText()));
				
			}
			
		});
		panelup.add(concLab);
		panelup.add(conc);
		content.add(panelup);
		
		JLabel nickSeq = new JLabel("Sequence at nick: ");
		final JComboBox<String> box = new JComboBox<String>();
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
		model.addElement("Default");
		model.addElement("AA");
		model.addElement("AT");
		model.addElement("AG");
		model.addElement("AC");
		model.addElement("TA");
		model.addElement("TT");
		model.addElement("TG");
		model.addElement("TC");
		model.addElement("GA");
		model.addElement("GT");
		model.addElement("GG");
		model.addElement("GC");
		model.addElement("CA");
		model.addElement("CT");
		model.addElement("CG");
		model.addElement("CC");
		model.addElement("Custom");
		box.setModel(model);
		
		model.setSelectedItem(graph.getType(template));
		
		nickSeq.setLabelFor(box);
		JPanel p1 = new JPanel();
		p1.setLayout(new BoxLayout(p1,BoxLayout.LINE_AXIS));
		
		p1.add(nickSeq);
		p1.add(box);
		
		panelmiddle.add(p1);
		
		JLabel nickLab = new JLabel("Denaturation slowdown: ");
		nickLab.setToolTipText("<html>Represents the increased stability of<br>a structure where both the input and<br>the ouput are attached to the<br>template. In this case, both the input<br>and the output will denature with the<br>specified slowdown.<br>This stability increase is due to coaxial<br>stacking and as such is template<br>dependent"
				+"<br><br>Slowdown values, based on <br>Padirac et al.'s conditions:" +
						"<br>Sequence at the nick/Slowdown" +
						"<br>TA:      1.2" +
						"<br>CA = TG: 0.62" +
						"<br>CG:      0.19" +
						"<br>AG = CT: 0.16" +
						"<br>AA = TT: 0.16" +
						"<br>GA = TC: 0.16" +
						"<br>AT:      0.12" +
						"<br>GG = CC: 0.12" +
						"<br>GT = AC: 0.04" +
						"<br>GC:      0.04</html>");
		final JTextField nick = new JTextField(""+graph.getStacking(template));
		
		nickLab.setLabelFor(nick);
		
		JPanel p2 = new JPanel();
		
		p2.setLayout(new BoxLayout(p2,BoxLayout.LINE_AXIS));
		
		
		p2.add(nickLab);
		p2.add(nick);
		panelmiddle.add(p2);
		nick.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				setStackValue(Double.parseDouble(nick.getText()));
			}
			
		});
		nick.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {
				
				
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				setStackValue(Double.parseDouble(nick.getText()));
				
			}
			
		});
		
		final JTextField dangleL = new JTextField(""+graph.getDangleL(template));
		final JTextField dangleR = new JTextField(""+graph.getDangleR(template));
		
		if(graph.dangle){
		JLabel dangleLLab = new JLabel("Left dangle slowdown: ");
		dangleLLab.setToolTipText("<html>Represents the increased stability of<br>a structure where the input is<br>present.<br>Based on Santa Lucia et al.:" +
						
						"</html>");
		
		
		dangleLLab.setLabelFor(dangleL);
		
		JPanel p3 = new JPanel();
		
		p3.setLayout(new BoxLayout(p3,BoxLayout.LINE_AXIS));
		
		
		p3.add(dangleLLab);
		p3.add(dangleL);
		panelmiddle.add(p3);
		dangleL.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				setDangleLValue(Double.parseDouble(dangleL.getText()));
			}
			
		});
		dangleL.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {
				
				
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				setDangleLValue(Double.parseDouble(dangleL.getText()));
				
			}
			
		});
		
		JLabel dangleRLab = new JLabel("Right dangle slowdown: ");
		dangleRLab.setToolTipText("<html>Represents the increased stability of<br>a structure where the output is<br>present.<br>Based on Santa Lucia et al.:" +
						
						"</html>");
		
		
		dangleRLab.setLabelFor(dangleR);
		
		JPanel p4 = new JPanel();
		
		p4.setLayout(new BoxLayout(p4,BoxLayout.LINE_AXIS));
		
		
		p4.add(dangleRLab);
		p4.add(dangleR);
		panelmiddle.add(p4);
		dangleR.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				setDangleRValue(Double.parseDouble(dangleR.getText()));
			}
			
		});
		dangleR.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {
				
				
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				setDangleRValue(Double.parseDouble(dangleR.getText()));
				
			}
			
		});
		
		}
		
		box.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				double[] values = SlowdownConstants.getSlowdown((String)box.getSelectedItem());
				if(values == null && custValues != null){
					nick.setText(""+custValues[0]);
					dangleL.setText(""+custValues[1]);
					dangleR.setText(""+custValues[2]);
				} else if (values != null){
					nick.setText(""+values[0]);
					dangleL.setText(""+values[1]);
					dangleR.setText(""+values[2]);
				}
				
				graph.setStacking(template, Double.parseDouble(nick.getText()));
				graph.setDangleL(template, Double.parseDouble(dangleL.getText()));
				graph.setDangleR(template, Double.parseDouble(dangleR.getText()));
				graph.setType(template, (String) box.getSelectedItem());
			}});
		
		if(PseudoTemplateGraph.class.isAssignableFrom(graph.getClass())){
			final PseudoTemplateGraph<?,E> pGraph = (PseudoTemplateGraph<?,E>) graph;
			JPanel p5 = new JPanel();
		
			p5.setLayout(new BoxLayout(p5,BoxLayout.LINE_AXIS));
			final JTextField missingBases = new JTextField(""+pGraph.getInputSlowdown(template));
		

			JLabel missingLab = new JLabel("Missing base input slowdown: ");
			missingLab.setToolTipText("<html>Represents the lowered stability of<br>a structure where the input domain is<br>missing a few bases." +	
						"</html>");
			
		
			missingLab.setLabelFor(missingBases);
			p5.add(missingLab);
			p5.add(missingBases);
			panelmiddle.add(p5);
			missingBases.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					pGraph.setInputSlowdown(template, (Double.parseDouble(missingBases.getText())));
				}
				
			});
			missingBases.addFocusListener(new FocusListener(){

				@Override
				public void focusGained(FocusEvent arg0) {}

				@Override
				public void focusLost(FocusEvent arg0) {
					pGraph.setInputSlowdown(template, (Double.parseDouble(missingBases.getText())));
				}
				
			});
			
		}
		
		content.add(panelmiddle);
		
		
		
		
		paneldown.add(Box.createHorizontalGlue());
		JButton remove = initRemoveButton();
		paneldown.add(remove);
		content.add(paneldown);
		this.pack();
	}
	
	protected JButton initRemoveButton(){
		JButton button =new JButton("Remove template");
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				graph.removeEdge(template);
				dispose();
			}
			
		});
		return button;
	}
	
	protected void setTemplateValue(double value){
		graph.setTemplateConcentration(template, value);
	}
	
	protected void setStackValue(double value){
		custValues = new double[3];
		graph.setStacking(template, value);
		custValues[0] = graph.getStacking(template);
		custValues[1] = graph.getDangleL(template);
		custValues[2] = graph.getDangleR(template);
		graph.setType(template, "Custom");
	}
	
	protected void setDangleLValue(double value){
		custValues = new double[3];
		graph.setDangleL(template, value);
		custValues[0] = graph.getStacking(template);
		custValues[1] = graph.getDangleL(template);
		custValues[2] = graph.getDangleR(template);
		graph.setType(template, "Custom");
	}
	
	protected void setDangleRValue(double value){
		custValues = new double[3];
		graph.setDangleR(template, value);
		custValues[0] = graph.getStacking(template);
		custValues[1] = graph.getDangleL(template);
		custValues[2] = graph.getDangleR(template);
		graph.setType(template, "Custom");
	}
	
	
}
