package graphical.frame;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.picking.MultiPickedState;
import graphical.frame.GraphicInterfaceMouseAdapter;
import graphical.frame.MyActionlistener;
import graphical.frame.MyVisualizationServer;
import graphical.graphRendering.DoubleVertexRenderer;
import graphical.graphRendering.MyVertexFillPaintTransformer;
import graphical.graphRendering.OligoRenderer;
import java.awt.Dimension;
import javax.swing.border.LineBorder;


import utils.EdgeFactory;
import utils.VertexFactory;
import model.PseudoTemplateGraph;
import model.chemicals.PseudoExtendedSequence;
import model.chemicals.SequenceVertex;

public class MainPseudoTempFrame extends MainFrame  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	public MainPseudoTempFrame(){
		super("Pseudo-DACCAD");
	}
	
	@Override
	protected void initGraph(){
		final PseudoTemplateGraph<SequenceVertex, String> g = new PseudoTemplateGraph<SequenceVertex,String>();
	    g.initFactories(new VertexFactory<SequenceVertex>(g){
	    	
			public SequenceVertex create() {
				SequenceVertex newvertex = associatedGraph.popAvailableVertex();
				if (newvertex == null){
					newvertex = new SequenceVertex(associatedGraph.getVertexCount() + 1);
				} else {
					newvertex = new SequenceVertex(newvertex.ID);
				}
				return newvertex;
			}

			@Override
			public SequenceVertex copy(SequenceVertex original) {
				 SequenceVertex ret = new SequenceVertex(original.ID);
				 ret.inputs = original.inputs;
				 return ret;
			} 	
	    }, new EdgeFactory<SequenceVertex,String>(g){
	    	public String createEdge(SequenceVertex v1, SequenceVertex v2){
	    		if(PseudoExtendedSequence.class.isAssignableFrom(v2.getClass())){
	    			return "Pseudo"+v1.ID;
	    		}
	    		return v1.ID+"->"+v2.ID;
	    	}
	    	public String inhibitorName(String s){
	    		return "Inhib"+s;
	    	}
	    });
	    this.graph = g;
	}
	
	@Override
	protected MyVisualizationServer<SequenceVertex,String> initVisualizationServer(DataPanel<String> dataPanel){
GraphicInterfaceMouseAdapter myadapter = new GraphicInterfaceMouseAdapter();
        
        Layout<SequenceVertex, String> frlayout = new FRLayout<SequenceVertex, String>(graph);
        frlayout.setSize(new Dimension(300,300)); // sets the initial size of the space
         // The BasicVisualizationServer<V,E> is parameterized by the edge types
         MyVisualizationServer<SequenceVertex,String> vv = 
                  new MyPseudoTempVisualizationServer<String>(frlayout, myadapter,dataPanel);
         vv.setRenderer(new OligoRenderer<SequenceVertex,String>());
         vv.setBorder(LineBorder.createBlackLineBorder());
         vv.addMouseListener(myadapter);
         vv.addMouseMotionListener(myadapter);
         vv.getRenderContext().setPickedVertexState(new MultiPickedState<SequenceVertex>());
         vv.getRenderContext().setVertexFillPaintTransformer(new MyVertexFillPaintTransformer<SequenceVertex>(graph));
         vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
         
         //ADDED
         vv.getRenderer().setVertexRenderer(new DoubleVertexRenderer((PseudoTemplateGraph<SequenceVertex,String>)graph));
        
        return vv;
	}
	
	@Override
	protected MyActionlistener<SequenceVertex,String> initActionListener(MyVisualizationServer<SequenceVertex,String> vv, DataPanel<String> dataPanel){
		return new MyPseudoTemplateActionListener(vv.getGraphLayout(),dataPanel);
	}
	
	public static void main(String[] args){
		
		MainPseudoTempFrame myframe = new MainPseudoTempFrame();
		myframe.setVisible(true);
		
	}
}
