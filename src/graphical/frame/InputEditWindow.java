package graphical.frame;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.input.AbstractInput;
import model.input.PulseInput;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InputEditWindow extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	InputManageWindow<?> master;
	AbstractInput input;
	JTextField timefield;
	JCheckBox periodic;
	JTextField periodfield;
    JTextField ammountfield;
	
InputEditWindow (InputManageWindow<?> master, AbstractInput input){
	super("Edit input");
	this.master = master;
	this.input = input;
	JPanel panel = new JPanel();
	if(input.getClass() == PulseInput.class){
	periodic = new JCheckBox("Periodic",((PulseInput)input).isPeriodic());
	periodic.addActionListener(this);
	}
	JPanel periodpanel = new JPanel();
	JPanel ammountpanel = new JPanel();
	JPanel buttonpanel = new JPanel();
	
	panel.setLayout(new BoxLayout(panel,BoxLayout.LINE_AXIS));
	periodpanel.setLayout(new BoxLayout(periodpanel,BoxLayout.LINE_AXIS));
	buttonpanel.setLayout(new BoxLayout(buttonpanel,BoxLayout.LINE_AXIS));
	
	JLabel label = new JLabel("Offset (min): ");
	timefield = new JTextField(""+this.input.getNextSingularityTime());
	
	JLabel periodlabel = new JLabel("Period (min): ");
	JLabel ammountlabel = new JLabel("Amount (nM): ");
	if(input.getClass() == PulseInput.class){

	periodfield = new JTextField(""+((PulseInput) this.input).getPeriod());
	periodfield.setEnabled(((PulseInput)this.input).isPeriodic());
	

	ammountfield = new JTextField(""+((PulseInput)this.input).ampli);
	}
	JButton set = new JButton("set");
	set.setActionCommand("set");
	set.addActionListener(this);
	
	JButton removebutton = new JButton("remove");
	removebutton.setActionCommand("remove");
	removebutton.addActionListener(this);
	
	Container content = this.getContentPane();
	content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
	panel.add(label);
	panel.add(timefield);
	if(this.input.getClass() == PulseInput.class){
	periodpanel.add(periodlabel);
	periodpanel.add(periodfield);
	ammountpanel.add(ammountlabel);
	ammountpanel.add(ammountfield);
	}
	content.add(panel);
	if(this.input.getClass() == PulseInput.class){
	content.add(periodic);
	content.add(periodpanel);
	content.add(ammountpanel);
	}
	buttonpanel.add(set);
	buttonpanel.add(removebutton);
	content.add(buttonpanel);
	this.pack();
}

@Override
public void actionPerformed(ActionEvent arg0) {
	if (arg0.getSource().getClass() == JCheckBox.class){
		periodfield.setEnabled(((JCheckBox) arg0.getSource()).isSelected());
	} else if (arg0.getActionCommand() == "set"){
		this.input.setTime(Double.valueOf(timefield.getText()));
		if(input.getClass() == PulseInput.class){
		
		((PulseInput)this.input).setPeriod(Double.valueOf(periodfield.getText()));
		((PulseInput)this.input).setPeriodic(periodic.isSelected());
		((PulseInput)this.input).ampli = Double.parseDouble(ammountfield.getText());
		} 
		master.graph.replot();
	} else if (arg0.getActionCommand() == "remove"){
		master.deleteInput(this.input);
		master.iew = null;
		this.dispose();
	}
	
}

}
