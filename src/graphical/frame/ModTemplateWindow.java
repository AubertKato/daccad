package graphical.frame;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.PseudoTemplateGraph;
import model.OligoGraph;

public class ModTemplateWindow<E> extends TemplateWindow<E> {

	ModTemplateWindow(E template, OligoGraph<?, E> graph) {
		super(template, graph);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void initComponent() {
		super.initComponent();
		//Now, we want to add another panel in
		//second position (after the concentration, which
		//should ALWAYS be there, no matter what.
		
		JPanel panelInputSlowdown = new JPanel();
		panelInputSlowdown.setLayout(new BoxLayout(panelInputSlowdown,BoxLayout.LINE_AXIS));
		
		JLabel label = new JLabel("Input h. slowdown: ");
		label.setToolTipText("<html>Represents the decreased hybridization of<br>speed of the input to the template." +
				
				"</html>");
		
		final JTextField inpslo = new JTextField(""+((PseudoTemplateGraph<?, E>)graph).getInputSlowdown(template));
		inpslo.setPreferredSize(new Dimension(100,18));
		inpslo.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				setInputSlowdown(Double.parseDouble(inpslo.getText()));
			}
			
		});
		inpslo.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {
				
				
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				setInputSlowdown(Double.parseDouble(inpslo.getText()));
				
			}
			
		});
		panelInputSlowdown.add(label);
		panelInputSlowdown.add(inpslo);
		
		Container content = this.getContentPane();
		content.add(panelInputSlowdown, 1);
		this.pack();
	}
	
	protected void setInputSlowdown(double value){
		((PseudoTemplateGraph<?, E>)graph).setInputSlowdown(template,value);
	}
}
