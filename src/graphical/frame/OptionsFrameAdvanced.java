package graphical.frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ToolTipManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

import optimizer.Parameter;

import model.Constants;
import model.OligoGraph;
import model.SlowdownConstants;

/**
 *
 * @author dextra
 */
public class OptionsFrameAdvanced <V,E> extends javax.swing.JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private OligoGraph<V,E> graph;
	// Variables declaration - do not modify
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonRemove;
    private javax.swing.JButton resetToDefault;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBoxAutoplot;
    private javax.swing.JCheckBox jCheckBoxExo;
    private javax.swing.JCheckBox jCheckBoxNick;
    private javax.swing.JCheckBox jCheckBoxPoly;
    private javax.swing.JFormattedTextField jFormattedTextFieldDispl;
    private javax.swing.JFormattedTextField jFormattedTextFieldKduplex;
    private javax.swing.JFormattedTextField jFormattedTextFieldKduplex1;
    private javax.swing.JFormattedTextField jFormattedTextFieldKduplex2;
    private javax.swing.JFormattedTextField jFormattedTextFieldKinhib;
    private javax.swing.JFormattedTextField jFormattedTextFieldKmInhibitorExo;
    private javax.swing.JFormattedTextField jFormattedTextFieldKmNick;
    private javax.swing.JFormattedTextField jFormattedTextFieldKmPoly;
    private javax.swing.JFormattedTextField jFormattedTextFieldKmPoly1;
    private javax.swing.JFormattedTextField jFormattedTextFieldKmSimpleExo;
    private javax.swing.JFormattedTextField jFormattedTextFieldParamMax;
    private javax.swing.JFormattedTextField jFormattedTextFieldParamCurrent;
    private javax.swing.JFormattedTextField jFormattedTextFieldMaxTime;
    private javax.swing.JFormattedTextField jFormattedTextFieldVmExo;
    private javax.swing.JFormattedTextField jFormattedTextFieldParamMin;
    private javax.swing.JFormattedTextField jFormattedTextFieldParamDef;
    private javax.swing.JFormattedTextField jFormattedTextFieldVmNick;
    private javax.swing.JFormattedTextField jFormattedTextFieldVmPoly;
    private javax.swing.JLabel jLabelDispl;
    private javax.swing.JLabel jLabelKduplex;
    private javax.swing.JLabel jLabelKduplex1;
    private javax.swing.JLabel jLabelKduplex2;
    private javax.swing.JLabel jLabelKinhib;
    private javax.swing.JLabel jLabelKmInhibitorExo;
    private javax.swing.JLabel jLabelKmNick;
    private javax.swing.JLabel jLabelKmPoly;
    private javax.swing.JLabel jLabelKmPoly1;
    private javax.swing.JLabel jLabelKmSimpleExo;
    private javax.swing.JLabel jLabelParamMax;
    private javax.swing.JLabel jLabelParamCurrent;
    private javax.swing.JLabel jLabelMaxTime;
    private javax.swing.JLabel jLabelParameterName;
    private javax.swing.JLabel jLabelVmExo;
    private javax.swing.JLabel jLabelParamMin;
    private javax.swing.JLabel jLabelParamDefault;
    private javax.swing.JLabel jLabelVmNick;
    private javax.swing.JLabel jLabelVmPoly;
    private javax.swing.JList<V> jListSequences;
    private javax.swing.JPanel jPanelContextual;
    private javax.swing.JPanel jPanelEnzymes;
    private javax.swing.JPanel jPanelGeneral;
    private javax.swing.JPanel jPanelOptimization;
    private javax.swing.JPanel jPanelPlot;
    private javax.swing.JPanel jPanelSaturable;
    private javax.swing.JScrollPane jScrollPane;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTree jTreeParams;
    private javax.swing.JCheckBox jCheckBoxSatByTemp;
    private javax.swing.JLabel jLabelKmSatByTemp;
    private javax.swing.JFormattedTextField jFormattedTextFieldSatByTemp;
    private javax.swing.Box.Filler filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(50, 0), new java.awt.Dimension(50, 0), new java.awt.Dimension(32767, 0));
    // End of variables declaration

    private JCheckBox jDangle;
    
	private JCheckBox jCheckBoxSelfStart;

	private JLabel jLabelKmSelfStart;

	private JFormattedTextField jFormattedTextFieldSelfStart;
	
	private JCheckBox jCheckBoxCoupling;

	 private JLabel jLabelSigma = new javax.swing.JLabel();
     private JFormattedTextField jFormattedTextFieldSigma = new javax.swing.JFormattedTextField();

	private JLabel jLabelAbsPrec;

	private JFormattedTextField jFormattedTextFieldAbsPrec;

	private JLabel jLabelRelPrec;

	private JFormattedTextField jFormattedTextFieldRelPrec;
	
	private JCheckBox fullExport;
	
	private JLabel jLabelInhibDangle;
	
	private JFormattedTextField jFormattedTextFieldInhibDangle;
	
	/**
     * Creates new form OptionsFrameAdvanced
     */
    public OptionsFrameAdvanced(OligoGraph<V, E> graph) {
    	this.graph = graph;
        initComponents();
    }

    

	/**
     * This method is called from within the constructor to initialize the form.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

    	ToolTipManager.sharedInstance().setDismissDelay(Short.MAX_VALUE);
    	ToolTipManager.sharedInstance().setInitialDelay(0);
    	
    	
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelGeneral = new javax.swing.JPanel();
        jLabelKinhib = new javax.swing.JLabel();
        jFormattedTextFieldKinhib = new javax.swing.JFormattedTextField();
        jLabelKduplex = new javax.swing.JLabel();
        jFormattedTextFieldKduplex = new javax.swing.JFormattedTextField();
        jPanelPlot = new javax.swing.JPanel();
        jScrollPane = new javax.swing.JScrollPane();
        DefaultListModel<V> dlm = new DefaultListModel<V>();
		for (V seq : graph.getPlottedSeqs()){
		dlm.addElement(seq);
		}
        jListSequences = new javax.swing.JList<V>(dlm);
        jButtonRemove = new javax.swing.JButton();
        jButtonAdd = new javax.swing.JButton();
        resetToDefault = new javax.swing.JButton();
        jLabelDispl = new javax.swing.JLabel();
        jFormattedTextFieldDispl = new javax.swing.JFormattedTextField();
        jLabelKduplex1 = new javax.swing.JLabel();
        jLabelKduplex2 = new javax.swing.JLabel();
        jFormattedTextFieldKduplex1 = new javax.swing.JFormattedTextField();
        jFormattedTextFieldKduplex2 = new javax.swing.JFormattedTextField();
        jLabelMaxTime = new javax.swing.JLabel();
        jFormattedTextFieldMaxTime = new javax.swing.JFormattedTextField();
        jCheckBoxAutoplot = new javax.swing.JCheckBox();
        jPanelEnzymes = new javax.swing.JPanel();
        jFormattedTextFieldKmSimpleExo = new javax.swing.JFormattedTextField();
        jLabelVmNick = new javax.swing.JLabel();
        jFormattedTextFieldKmInhibitorExo = new javax.swing.JFormattedTextField();
        jLabelVmExo = new javax.swing.JLabel();
        jLabelVmPoly = new javax.swing.JLabel();
        jLabelKmSimpleExo = new javax.swing.JLabel();
        jLabelKmInhibitorExo = new javax.swing.JLabel();
        jFormattedTextFieldKmPoly = new javax.swing.JFormattedTextField();
        jFormattedTextFieldKmNick = new javax.swing.JFormattedTextField();
        jLabelKmNick = new javax.swing.JLabel();
        jFormattedTextFieldVmPoly = new javax.swing.JFormattedTextField();
        jFormattedTextFieldKmPoly1 = new javax.swing.JFormattedTextField();
        jLabelKmPoly1 = new javax.swing.JLabel();
        jFormattedTextFieldVmNick = new javax.swing.JFormattedTextField();
        jLabelKmPoly = new javax.swing.JLabel();
        jFormattedTextFieldVmExo = new javax.swing.JFormattedTextField();
        jPanelSaturable = new javax.swing.JPanel();
        jCheckBoxExo = new javax.swing.JCheckBox();
        jCheckBoxPoly = new javax.swing.JCheckBox();
        jCheckBoxNick = new javax.swing.JCheckBox();
        jPanelOptimization = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTreeParams = new javax.swing.JTree();
        jPanelContextual = new javax.swing.JPanel();
        jLabelParamMin = new javax.swing.JLabel();
        jFormattedTextFieldParamMin = new javax.swing.JFormattedTextField();
        jLabelParamMax = new javax.swing.JLabel();
        jFormattedTextFieldParamMax = new javax.swing.JFormattedTextField();
        jLabelParamDefault = new javax.swing.JLabel();
        jFormattedTextFieldParamDef = new javax.swing.JFormattedTextField();
        jLabelParamCurrent = new javax.swing.JLabel();
        jFormattedTextFieldParamCurrent = new javax.swing.JFormattedTextField();
        jLabelParameterName = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBoxSatByTemp = new javax.swing.JCheckBox();
        jCheckBoxCoupling = new JCheckBox();
        jLabelKmSatByTemp = new javax.swing.JLabel();
        jFormattedTextFieldSatByTemp = new javax.swing.JFormattedTextField();
        jCheckBoxSelfStart = new javax.swing.JCheckBox();
        jLabelKmSelfStart = new javax.swing.JLabel();
        jFormattedTextFieldSelfStart = new javax.swing.JFormattedTextField();
        jLabelAbsPrec = new javax.swing.JLabel();
        jLabelRelPrec = new javax.swing.JLabel();
        jFormattedTextFieldAbsPrec = new javax.swing.JFormattedTextField();
        jFormattedTextFieldRelPrec = new javax.swing.JFormattedTextField();
        fullExport = new JCheckBox();
        jLabelKinhib.setText("Kinhib factor (no unit)");
        jLabelKinhib.setToolTipText("<html>Factor between the stability of an inhibitor<br>attached to the template which created it<br>and its target template.<br>A factor superior to 1 means that the inhibitor<br>is more stable on its template (default).</html>");
        jDangle = new JCheckBox();
        jDangle.setText("Compute dangles");
        
        jLabelInhibDangle = new JLabel("Inhibition dangle slowdown");
        jLabelInhibDangle.setToolTipText("<html>Denaturation slowdown of the inhibitor <br>attached to its target.<br>If dangles are not computed separatly, this<br>is incorporated in alpha.</html>");
        jLabelInhibDangle.setEnabled(graph.dangle);
        jFormattedTextFieldInhibDangle = new javax.swing.JFormattedTextField();
        
        jFormattedTextFieldInhibDangle.setEnabled(graph.dangle);
        jFormattedTextFieldInhibDangle.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldInhibDangle.setText(Double.toString(SlowdownConstants.inhibDangleSlowdown));
        jFormattedTextFieldInhibDangle.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                SlowdownConstants.inhibDangleSlowdown = value;
                Constants.alpha = Constants.alphaBase*value;
                jFormattedTextFieldKinhib.setText(graph.dangle?Double.toString(Constants.alphaBase):Double.toString(Constants.alpha));
            }
        });
        jFormattedTextFieldInhibDangle.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
				SlowdownConstants.inhibDangleSlowdown = value;
				Constants.alpha = Constants.alphaBase*value;
                jFormattedTextFieldKinhib.setText(graph.dangle?Double.toString(Constants.alphaBase):Double.toString(Constants.alpha));
			}

        	
        });
        
        jFormattedTextFieldKinhib.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldKinhib.setText(graph.dangle?Double.toString(Constants.alphaBase):Double.toString(Constants.alpha));
        jFormattedTextFieldKinhib.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                
                Constants.alpha = value*(graph.dangle?SlowdownConstants.inhibDangleSlowdown:1);
                Constants.alphaBase = value*(graph.dangle?1:1.0/SlowdownConstants.inhibDangleSlowdown);
            }
        });
        jFormattedTextFieldKinhib.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
				Constants.alpha = value*(graph.dangle?SlowdownConstants.inhibDangleSlowdown:1);
                Constants.alphaBase = value*(graph.dangle?1:1.0/SlowdownConstants.inhibDangleSlowdown);
			}

        	
        });

        jLabelKduplex.setText("Association rate (per nM per min)");
        jLabelKduplex.setToolTipText("<html>Rate at which two complementary strands<br>attach to each other.</html>");

        jFormattedTextFieldKduplex.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat(""))));
        jFormattedTextFieldKduplex.setText(Double.toString(Constants.Kduplex));
        jFormattedTextFieldKduplex.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.Kduplex = value;
            }
        });
        jFormattedTextFieldKduplex.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.Kduplex = value;
			}

        	
        });

        jPanelPlot.setBorder(javax.swing.BorderFactory.createTitledBorder("Plotted sequences"));

        jScrollPane.setViewportView(jListSequences);

        jButtonRemove.setText("Remove");
        jButtonRemove.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
        		
                List<V> seqs =  jListSequences.getSelectedValuesList();
                for(int i=0; i<seqs.size(); i++){
                graph.removePlottedSeq(seqs.get(i));
                ((DefaultListModel<V>)jListSequences.getModel()).removeElement(seqs.get(i));
                }
                jListSequences.repaint();
            }
        });

        jButtonAdd.setText("Add");
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
        		V addedSeq = (V) JOptionPane.showInputDialog(null, "Add plotted sequence", "Sequence", JOptionPane.PLAIN_MESSAGE, null, graph.getNotPlottedSeqs().toArray(), (Object) graph.getNotPlottedSeqs().iterator().next());
        		graph.addPlottedSeq(addedSeq);
        		((DefaultListModel<V>)jListSequences.getModel()).addElement(addedSeq);
        		jListSequences.repaint();
        	}
        });

        javax.swing.GroupLayout jPanelPlotLayout = new javax.swing.GroupLayout(jPanelPlot);
        jPanelPlot.setLayout(jPanelPlotLayout);
        jPanelPlotLayout.setHorizontalGroup(
            jPanelPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane)
            .addGroup(jPanelPlotLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonRemove))
        );
        jPanelPlotLayout.setVerticalGroup(
            jPanelPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPlotLayout.createSequentialGroup()
                .addComponent(jScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelPlotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonRemove)
                    .addComponent(jButtonAdd))
                .addGap(4, 4, 4))
        );

        jLabelDispl.setText("Displacement (no unit)");
        jLabelDispl.setToolTipText("<html>Slow down factor of the polymerase when using<br>its displacement ability.<br>Only applies to templates with an inhibitor as<br>output.</html>");

        jFormattedTextFieldDispl.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldDispl.setText(Double.toString(Constants.displ));
        jFormattedTextFieldDispl.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.displ = value;
            }
        });
        jFormattedTextFieldDispl.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.displ = value;
			}

        	
        });

        jLabelKduplex1.setText("Toehold input (no unit)");
        jLabelKduplex1.setToolTipText("<html>Association slowdown for an input species to<br>displace an inhibitor from its inhibited template.<br>May be set to 0 to forbid displacement of<br>inhibitors.<br>Inhibitor always invade at full speed.</html>");

        jFormattedTextFieldKduplex1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat(""))));
        jFormattedTextFieldKduplex1.setText(Double.toString(Constants.ratioToeholdLeft));
        jFormattedTextFieldKduplex1.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.ratioToeholdLeft = value;
            }
        });
        jFormattedTextFieldKduplex1.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.ratioToeholdLeft = value;
			}

        	
        });
        
        jLabelKduplex2.setText("Toehold output (no unit)");
        jLabelKduplex2.setToolTipText("<html>Association slowdown for an output species to<br>displace an inhibitor from its inhibited template.<br>May be set to 0 to forbid displacement of<br>inhibitors.<br>Inhibitor always invade at full speed.</html>");

        jFormattedTextFieldKduplex2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat(""))));
        jFormattedTextFieldKduplex2.setText(Double.toString(Constants.ratioToeholdRight));
        jFormattedTextFieldKduplex2.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.ratioToeholdRight = value;
            }
        });
        jFormattedTextFieldKduplex2.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.ratioToeholdRight = value;
			}

        	
        });

        jLabelMaxTime.setText("Simulation time (min)");
        
        jFormattedTextFieldMaxTime.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldMaxTime.setText(Integer.toString(Constants.numberOfPoints));
        jFormattedTextFieldMaxTime.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                int value = Integer.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.numberOfPoints = value;
            }
        });
        jFormattedTextFieldMaxTime.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				int value = Integer.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.numberOfPoints = value;
			}

        	
        });

        jCheckBoxAutoplot.setText("Autoplot");
        jCheckBoxAutoplot.setSelected(utils.PlotExpData.autoplot);
        jCheckBoxAutoplot.addActionListener(new java.awt.event.ActionListener(){
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
        		utils.PlotExpData.autoplot = jCheckBoxAutoplot.isSelected();
        	}
        });
        
        fullExport.setText("full plot export");
        fullExport.setSelected(Constants.fullExport);
        fullExport.addActionListener(new java.awt.event.ActionListener(){
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
        		Constants.fullExport = fullExport.isSelected();
        	}
        });
        
        fullExport.setToolTipText("<html>When exporting the time series, include the templates</html>");
        
        jDangle.setSelected(graph.dangle);
        
        jDangle.addActionListener(new java.awt.event.ActionListener(){
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
        		graph.dangle = jDangle.isSelected();
        		jLabelInhibDangle.setEnabled(graph.dangle);
                jFormattedTextFieldInhibDangle.setEnabled(graph.dangle);
                jFormattedTextFieldKinhib.setText(graph.dangle?Double.toString(Constants.alphaBase):Double.toString(Constants.alpha));
        	}
        });
        
        jLabelAbsPrec.setText("Absolute prec.");
        jLabelAbsPrec.setToolTipText("<html>Absolute precision of the integration algorithm.<br>The lower, the more precise.<br>May slow down the integration significantly if below 1e-10.</html>");

        jFormattedTextFieldAbsPrec.setText(Constants.absprec+"");
        jFormattedTextFieldAbsPrec.addActionListener(new java.awt.event.ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) arg0.getSource()).getText());
                Constants.absprec = value;
				
			}
        	
        });
        jFormattedTextFieldAbsPrec.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.absprec = value;
			}

        	
        });

        jLabelRelPrec.setText("Relative prec.");
        jLabelRelPrec.setToolTipText("<html>Relative precision of the integration algorithm.<br>The lower, the more precise.<br>May slow down the integration significantly if below 1e-10.</html>");

       
        jFormattedTextFieldRelPrec.setText(Constants.relprec+"");
        jFormattedTextFieldRelPrec.addActionListener(new java.awt.event.ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) arg0.getSource()).getText());
                Constants.relprec = value;
				
			}
        	
        });
        jFormattedTextFieldRelPrec.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.relprec = value;
			}

        	
        });

        javax.swing.GroupLayout jPanelGeneralLayout = new javax.swing.GroupLayout(jPanelGeneral);
        jPanelGeneral.setLayout(jPanelGeneralLayout);
        jPanelGeneralLayout.setHorizontalGroup(
            jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanelGeneralLayout.createSequentialGroup()
                            .addComponent(jLabelKduplex)
                            .addGap(49, 49, 49)
                            .addComponent(jFormattedTextFieldKduplex, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jFormattedTextFieldKinhib, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelGeneralLayout.createSequentialGroup()
                        .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelKinhib)
                            .addComponent(jLabelDispl))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jFormattedTextFieldDispl, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(37, 37, 37)
                .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelGeneralLayout.createSequentialGroup()
                        .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelKduplex1)
                            .addComponent(jLabelKduplex2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelGeneralLayout.createSequentialGroup()
                                .addComponent(jFormattedTextFieldKduplex1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(37, 37, 37)
                                .addComponent(jLabelAbsPrec)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jFormattedTextFieldAbsPrec, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelGeneralLayout.createSequentialGroup()
                                .addComponent(jFormattedTextFieldKduplex2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(37, 37, 37)
                                .addComponent(jLabelRelPrec)
                                .addGap(18, 18, 18)
                                .addComponent(jFormattedTextFieldRelPrec, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelGeneralLayout.createSequentialGroup()
                                .addComponent(fullExport)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelMaxTime)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jFormattedTextFieldMaxTime, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                
                                        ))
                            
                    .addComponent(jCheckBoxAutoplot))
                    //.addComponent(fullExport)
                .addContainerGap())
                .addGroup(jPanelGeneralLayout.createSequentialGroup()
                           .addComponent(jDangle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(100,100,100)
                           .addComponent(jLabelInhibDangle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(18,18,18)
                           
                           .addComponent(jFormattedTextFieldInhibDangle, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                           .addGap(450,450,450))
             .addComponent(jPanelPlot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanelGeneralLayout.setVerticalGroup(
            jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelKduplex)
                    .addComponent(jFormattedTextFieldKduplex, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelKduplex1)
                    .addComponent(jFormattedTextFieldKduplex1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelAbsPrec)
                        .addComponent(jFormattedTextFieldAbsPrec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelKinhib)
                    .addComponent(jFormattedTextFieldKinhib, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelKduplex2)
                    .addComponent(jFormattedTextFieldKduplex2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelRelPrec)
                        .addComponent(jFormattedTextFieldRelPrec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jFormattedTextFieldDispl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelDispl)
                    .addComponent(jCheckBoxAutoplot)
                    .addComponent(fullExport)
                    .addComponent(jLabelMaxTime)
                    .addComponent(jFormattedTextFieldMaxTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                           .addComponent(jDangle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                           
                           .addComponent(jLabelInhibDangle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                           
                           .addComponent(jFormattedTextFieldInhibDangle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                           
                )
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelPlot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("General", jPanelGeneral);

        jFormattedTextFieldKmSimpleExo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldKmSimpleExo.setText(Double.toString(Constants.exoKmSimple));
        jFormattedTextFieldKmSimpleExo.setEnabled(graph.saturableExo);
        jFormattedTextFieldKmSimpleExo.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.exoKmSimple = value;
                Constants.exoVm = Double.valueOf(jFormattedTextFieldVmExo.getText())*value;
            }
        });
        jFormattedTextFieldKmSimpleExo.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.exoKmSimple = value;
                Constants.exoVm = Double.valueOf(jFormattedTextFieldVmExo.getText())*value;
			}

        	
        });

        jLabelVmNick.setText("Activity (per min)");
        jLabelVmNick.setToolTipText("<html>Activity as defined by the Michaelis-Menten constants Vm/Km</html>");

        jFormattedTextFieldKmInhibitorExo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldKmInhibitorExo.setText(Double.toString(Constants.exoKmInhib));
        jFormattedTextFieldKmInhibitorExo.setEnabled(graph.saturableExo);
        jFormattedTextFieldKmInhibitorExo.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.exoKmInhib = value;
            }
        });
        jFormattedTextFieldKmInhibitorExo.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.exoKmInhib = value;
			}

        	
        });

        jLabelVmExo.setText("Activity (per min)");
        jLabelVmExo.setToolTipText("<html>Activity as defined by the Michaelis-Menten constants Vm/Km</html>");

        jLabelVmPoly.setText("Activity (per min)");
        jLabelVmPoly.setToolTipText("<html>Activity as defined by the Michaelis-Menten constants Vm/Km</html>");

        jLabelKmSimpleExo.setText("KmSimple (nM)");
        jLabelKmSimpleExo.setToolTipText("<html>Michaelis constant of signal species<br>with respect to the exonuclease.</html>");
        jLabelKmSimpleExo.setEnabled(graph.saturableExo);

        jLabelKmInhibitorExo.setText("KmInhibitor (nM)");
        jLabelKmInhibitorExo.setToolTipText("<html>Michaelis constant of inhibiting species<br>with respect to the exonuclease.</html>");
        jLabelKmInhibitorExo.setEnabled(graph.saturableExo);

        jFormattedTextFieldKmPoly.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldKmPoly.setText(Double.toString(Constants.polKm));
        jFormattedTextFieldKmPoly.setEnabled(graph.saturablePoly);
        jFormattedTextFieldKmPoly.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.polVm = Double.valueOf(jFormattedTextFieldVmPoly.getText())*value;
                Constants.polKm = value;
            }
        });
        jFormattedTextFieldKmPoly.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.polKm = value;
                Constants.polVm = Double.valueOf(jFormattedTextFieldVmPoly.getText())*value;
			}

        	
        });

        jFormattedTextFieldKmNick.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldKmNick.setText(Double.toString(Constants.nickKm));
        jFormattedTextFieldKmNick.setEnabled(graph.saturableNick);
        jFormattedTextFieldKmNick.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.nickKm = value;
                Constants.nickVm = Double.valueOf(jFormattedTextFieldVmNick.getText())*value;
			}

        	
        });
        jFormattedTextFieldKmNick.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.nickKm = value;
                Constants.nickVm = Double.valueOf(jFormattedTextFieldVmNick.getText())*value;
            }
        });
        
        jLabelKmNick.setText("Km (nM)");
        jLabelKmNick.setEnabled(graph.saturableNick);

        jFormattedTextFieldVmPoly.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldVmPoly.setText(Double.toString(Math.round(Constants.polVm/Constants.polKm * 1000.0)/1000.0));
        jFormattedTextFieldVmPoly.setEnabled(true);
        jFormattedTextFieldVmPoly.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.polVm = value*Constants.polKm;
            }
        });
        jFormattedTextFieldVmPoly.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.polVm = value*Constants.polKm;
			}

        	
        });

        jFormattedTextFieldKmPoly1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldKmPoly1.setText(Double.toString(Constants.polKmBoth));
        jFormattedTextFieldKmPoly1.setEnabled(graph.saturablePoly);
        jFormattedTextFieldKmPoly1.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.polKmBoth = value;
            }
        });
        jFormattedTextFieldKmPoly1.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.polKmBoth = value;
			}

        	
        });

        jLabelKmPoly1.setText("KmDouble (nM)");
        jLabelKmPoly1.setToolTipText("<html>Michaelis constant of fully double-stranded inhibitor-generating<br>templates with respect to the polymerase.</html>");
        jLabelKmPoly1.setEnabled(graph.saturablePoly);

        jFormattedTextFieldVmNick.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldVmNick.setText(Double.toString(Math.round(Constants.nickVm/Constants.nickKm*1000.0)/1000.0));
        jFormattedTextFieldVmNick.setEnabled(true);
        jFormattedTextFieldVmNick.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.nickVm = value*Constants.nickKm;
            }
        });
        jFormattedTextFieldVmNick.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.nickVm = value*Constants.nickKm;
			}

        	
        });

        jLabelKmPoly.setText("Km (nM)");
        jLabelKmPoly.setEnabled(graph.saturablePoly);

        jFormattedTextFieldVmExo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldVmExo.setText(Double.toString(Math.round(Constants.exoVm/Constants.exoKmSimple *1000.0)/1000.0));
        jFormattedTextFieldVmExo.setEnabled(true); 
        jFormattedTextFieldVmExo.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.exoVm = value*Constants.exoKmSimple;
            }
        });
        jFormattedTextFieldVmExo.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.exoVm = value*Constants.exoKmSimple;
			}

        	
        });
        
        jPanelSaturable.setBorder(javax.swing.BorderFactory.createTitledBorder("Saturable"));

        jCheckBoxExo.setText("Exonuclease");
        jCheckBoxExo.setSelected(graph.saturableExo);
        jCheckBoxExo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxExoActionPerformed(evt);
                jCheckBoxCoupling.setEnabled(jCheckBoxNick.isSelected()||jCheckBoxPoly.isSelected()||jCheckBoxExo.isSelected());
            }
        });

        jCheckBoxPoly.setText("Polymerase");
        jCheckBoxPoly.setSelected(graph.saturablePoly);
        jCheckBoxPoly.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPolyActionPerformed(evt);
                jCheckBoxCoupling.setEnabled(jCheckBoxNick.isSelected()||jCheckBoxPoly.isSelected()||jCheckBoxExo.isSelected());
            }
        });

        jCheckBoxNick.setText("Nickase");
        jCheckBoxNick.setSelected(graph.saturableNick);
        jCheckBoxNick.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxNickActionPerformed(evt);
                jCheckBoxCoupling.setEnabled(jCheckBoxNick.isSelected()||jCheckBoxPoly.isSelected()||jCheckBoxExo.isSelected());
            }
        });
        
        jCheckBoxCoupling.setText("Enzymatic coupling");
        jCheckBoxCoupling.setEnabled(jCheckBoxNick.isSelected()||jCheckBoxPoly.isSelected()||jCheckBoxExo.isSelected());
        jCheckBoxCoupling.setSelected(graph.coupling);
        jCheckBoxCoupling.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                graph.coupling = jCheckBoxCoupling.isSelected();
            }
        });
        jCheckBoxSatByTemp.setText("Exonuclease inhibition by free templates");
        jCheckBoxSatByTemp.setToolTipText("<html>Inhibitory saturation of exonuclease by free templates.</html>");
        jCheckBoxSatByTemp.setSelected(graph.isExoSatByTemp());
        jCheckBoxSatByTemp.setEnabled(graph.saturableExo);
        jCheckBoxSatByTemp.addActionListener(new java.awt.event.ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				graph.setSatExoByTemplate(jCheckBoxSatByTemp.isSelected()?Constants.exoKmTemplate:0.0);
				jLabelKmSatByTemp.setEnabled(graph.isExoSatByTemp());
				jFormattedTextFieldSatByTemp.setEnabled(graph.isExoSatByTemp());
			}
        	
        });
        
        

        jLabelKmSatByTemp.setText("Ki (nM)");
        jLabelKmSatByTemp.setEnabled(graph.isExoSatByTemp() && graph.saturableExo);

        jFormattedTextFieldSatByTemp.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldSatByTemp.setText(Double.toString(Constants.exoKmTemplate));
        jFormattedTextFieldSatByTemp.setEnabled(graph.isExoSatByTemp() && graph.saturableExo);
        jFormattedTextFieldSatByTemp.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.exoKmTemplate = value;
                
        	}
        });
        jFormattedTextFieldSatByTemp.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.exoKmTemplate = value;
                
			}

        	
        });

        jCheckBoxSelfStart.setText("Polymerase zeroth order leak");
        jCheckBoxSelfStart.setToolTipText("<html>The polymerase can create some output even without primers, at a fraction of its activity.</html>");
        jCheckBoxSelfStart.setSelected(graph.selfStart);
        
        jCheckBoxSelfStart.addActionListener(new java.awt.event.ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				graph.selfStart = jCheckBoxSelfStart.isSelected();
				jLabelKmSelfStart.setEnabled(graph.selfStart);
				jFormattedTextFieldSelfStart.setEnabled(graph.selfStart);
			}
        	
        });
        
        
        jLabelKmSelfStart.setText("Activity ratio (no unit)");
        jLabelKmSelfStart.setEnabled(graph.selfStart);

        jFormattedTextFieldSelfStart.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldSelfStart.setText(Double.toString(Constants.ratioSelfStart));
        jFormattedTextFieldSelfStart.setEnabled(graph.selfStart);
        jFormattedTextFieldSelfStart.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.ratioSelfStart = value;
            }
        });
        jFormattedTextFieldSelfStart.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                Constants.ratioSelfStart = value;
			}

        	
        });
        resetToDefault.setText("Default");
        resetToDefault.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int answer = JOptionPane.showConfirmDialog(null, "This will reset the enzymatic parameters to default. Confirm?", "Reset parameters",JOptionPane.OK_CANCEL_OPTION);
				if (answer == JOptionPane.OK_OPTION){
					resetValues();
				}
			}
        	
        });
        
        javax.swing.GroupLayout jPanelSaturableLayout = new javax.swing.GroupLayout(jPanelSaturable);
        jPanelSaturable.setLayout(jPanelSaturableLayout);
        jPanelSaturableLayout.setHorizontalGroup(
            jPanelSaturableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSaturableLayout.createSequentialGroup()
                .addGroup(jPanelSaturableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBoxExo)
                    .addComponent(jCheckBoxNick)
                    .addComponent(jCheckBoxPoly))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelSaturableLayout.setVerticalGroup(
            jPanelSaturableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSaturableLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jCheckBoxExo)
                .addGap(0, 0, 0)
                .addComponent(jCheckBoxPoly)
                .addGap(0, 0, 0)
                .addComponent(jCheckBoxNick)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelEnzymesLayout = new javax.swing.GroupLayout(jPanelEnzymes);
        jPanelEnzymes.setLayout(jPanelEnzymesLayout);
        jPanelEnzymesLayout.setHorizontalGroup(
            jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                        .addComponent(jPanelSaturable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabelVmNick)
                            .addComponent(jLabelVmPoly, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelVmExo, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jFormattedTextFieldVmNick, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jFormattedTextFieldVmPoly, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jFormattedTextFieldVmExo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))    
                        .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                        	.addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)	
                        .addComponent(jCheckBoxCoupling)
                        .addComponent(jCheckBoxSatByTemp)
                        .addComponent(jCheckBoxSelfStart))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))    
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelKmSimpleExo)
                    .addComponent(jLabelKmPoly)
                    .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelKmSatByTemp)
                        .addComponent(jLabelKmNick)
                        .addComponent(jLabelKmSelfStart)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                        .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jFormattedTextFieldKmNick, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jFormattedTextFieldKmPoly, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jFormattedTextFieldKmSimpleExo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                                .addComponent(jLabelKmInhibitorExo)
                                .addGap(29, 29, 29)
                                .addComponent(jFormattedTextFieldKmInhibitorExo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(resetToDefault, javax.swing.GroupLayout.Alignment.LEADING)    
                            .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                                .addComponent(jLabelKmPoly1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jFormattedTextFieldKmPoly1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jFormattedTextFieldSatByTemp, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jFormattedTextFieldSelfStart, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanelEnzymesLayout.setVerticalGroup(
            jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                                .addComponent(jFormattedTextFieldKmSimpleExo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jFormattedTextFieldKmPoly, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jFormattedTextFieldKmNick, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                                .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelVmExo)
                                        .addComponent(jLabelKmSimpleExo)
                                        .addComponent(jLabelKmInhibitorExo)
                                        .addComponent(jFormattedTextFieldVmExo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jFormattedTextFieldKmInhibitorExo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                                        .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabelVmPoly)
                                            .addComponent(jLabelKmPoly)
                                            .addComponent(jFormattedTextFieldVmPoly, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, 0)
                                        .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabelVmNick)
                                            .addComponent(jLabelKmNick)
                                            .addComponent(jFormattedTextFieldVmNick, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jFormattedTextFieldKmPoly1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelKmPoly1))))))
                    .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanelSaturable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jCheckBoxCoupling)
                .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxSatByTemp)
                    .addComponent(jLabelKmSatByTemp)
                    .addComponent(jFormattedTextFieldSatByTemp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanelEnzymesLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanelEnzymesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxSelfStart)
                    .addComponent(jLabelKmSelfStart)
                    .addComponent(jFormattedTextFieldSelfStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))

                      .addComponent(resetToDefault)  
                     .addContainerGap(239, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Enzymes", jPanelEnzymes);

        
        jTreeParams.setModel(graph.getOptimizable());
        jTreeParams.setRootVisible(false);
        jTreeParams.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                jTreeParamsValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jTreeParams);
        jTreeParams.getAccessibleContext().setAccessibleName("Parameters");

        jLabelParamMin.setText("Min");
        jLabelParamMin.setVisible(!jTreeParams.isSelectionEmpty());

        jFormattedTextFieldParamMin.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldParamMin.setText(""+optimizer.Constants.valueMin);
        jFormattedTextFieldParamMin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTextFieldParamMinActionPerformed(evt);
            }
        });
        jFormattedTextFieldParamMin.setVisible(!jTreeParams.isSelectionEmpty());
        jFormattedTextFieldParamMin.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				jFormattedTextFieldParamMinActionPerformed(evt);
			}

        	
        });

        jLabelParamMax.setText("Max");
        jLabelParamMax.setVisible(!jTreeParams.isSelectionEmpty());

        jFormattedTextFieldParamMax.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldParamMax.setText(""+optimizer.Constants.valueMax);
        jFormattedTextFieldParamMax.setVisible(!jTreeParams.isSelectionEmpty());
        jFormattedTextFieldParamMax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTextFieldParamMaxActionPerformed(evt);
            }
        });
        jFormattedTextFieldParamMax.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				jFormattedTextFieldParamMaxActionPerformed(evt);
			}

        	
        });

        jLabelParamDefault.setText("Threshold ratio");
        jLabelParamDefault.setToolTipText("<html>Describes how strict the optimisation is.<br>The lower the value, the closer the curve has to be.");

        jFormattedTextFieldParamDef.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldParamDef.setText(""+(1.0/optimizer.Constants.thresholdRatio));
        jFormattedTextFieldParamDef.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTextFieldParamDefActionPerformed(evt);
            }

			
        });
        jFormattedTextFieldParamDef.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				jFormattedTextFieldParamDefActionPerformed(evt);
			}

        	
        });
        
        

        jLabelParamCurrent.setText("Current");
        jLabelParamCurrent.setVisible(!jTreeParams.isSelectionEmpty());

        jFormattedTextFieldParamCurrent.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.###"))));
        jFormattedTextFieldParamCurrent.setText("");
        jFormattedTextFieldParamCurrent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTextFieldKmSimpleExo2ActionPerformed(evt);
            }
        });
        jFormattedTextFieldParamCurrent.setVisible(!jTreeParams.isSelectionEmpty());

        jLabelParameterName.setText("ParameterName");
        jLabelParameterName.setVisible(!jTreeParams.isSelectionEmpty());
        
        jCheckBox1.setText("Optimize");
        jCheckBox1.setSelected(true);
        jCheckBox1.addActionListener(new ActionListener(){
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
        		optimizeCheckBoxClicked();
        	}

			
        });

        jLabelSigma.setText("Sigma");
        jFormattedTextFieldSigma.setText(""+optimizer.Constants.sigma);
        jFormattedTextFieldSigma.addActionListener(new ActionListener(){
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
                double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                optimizer.Constants.sigma = value;
            }
        });
        jFormattedTextFieldSigma.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {}

			@Override
			public void focusLost(FocusEvent evt) {
				double value = Double.valueOf(((javax.swing.JFormattedTextField) evt.getSource()).getText());
                optimizer.Constants.sigma = value;
			}

        	
        });
        
        
        javax.swing.GroupLayout jPanelContextualLayout = new javax.swing.GroupLayout(jPanelContextual);
        jPanelContextual.setLayout(jPanelContextualLayout);
        jPanelContextualLayout.setHorizontalGroup(
            jPanelContextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelContextualLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jPanelContextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelParameterName)
                    .addGroup(jPanelContextualLayout.createSequentialGroup()
                        .addGroup(jPanelContextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            
                            .addComponent(jLabelParamMin)
                            .addComponent(jLabelSigma))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelContextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            
                            .addGroup(jPanelContextualLayout.createSequentialGroup()
                                .addComponent(jFormattedTextFieldParamMin, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelParamMax)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                               
                                
                                )
                                .addGroup(jPanelContextualLayout.createSequentialGroup()
                                .addComponent(jFormattedTextFieldSigma, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelParamDefault)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                
                                )
                                
                        )
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelContextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        		 .addComponent(jFormattedTextFieldParamMax, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        		 .addComponent(jFormattedTextFieldParamDef, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        		 )
                       
                            //.addComponent(jFormattedTextFieldParamCurrent, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    )
                            .addGroup(jPanelContextualLayout.createSequentialGroup()
                        .addGap(300, 300, 300)
                        .addComponent(jCheckBox1)))
                .addContainerGap())
        );
        jPanelContextualLayout.setVerticalGroup(
            jPanelContextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContextualLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelContextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelParameterName)
                    .addComponent(jCheckBox1))
                .addGap(10, 10, 10)
                .addGroup(jPanelContextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    
                    .addGroup(jPanelContextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    	.addComponent(jFormattedTextFieldParamMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)	
                        .addComponent(jLabelParamMin)
                        .addComponent(jLabelParamMax)
                        .addComponent(jFormattedTextFieldParamMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10,10,Short.MAX_VALUE)
           
                    .addGroup(jPanelContextualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelSigma)
                    .addComponent(jFormattedTextFieldSigma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelParamDefault)
                    .addComponent(jFormattedTextFieldParamDef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelOptimizationLayout = new javax.swing.GroupLayout(jPanelOptimization);
        jPanelOptimization.setLayout(jPanelOptimizationLayout);
        jPanelOptimizationLayout.setHorizontalGroup(
            jPanelOptimizationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelOptimizationLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelContextual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelOptimizationLayout.setVerticalGroup(
            jPanelOptimizationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
            .addComponent(jPanelContextual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Optimization", jPanelOptimization);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>                                                            

    private void jCheckBoxExoActionPerformed(java.awt.event.ActionEvent evt) {
		graph.saturableExo = jCheckBoxExo.isSelected();
        
        jLabelKmSimpleExo.setEnabled(jCheckBoxExo.isSelected());
        jLabelKmInhibitorExo.setEnabled(jCheckBoxExo.isSelected());
        
        jFormattedTextFieldKmSimpleExo.setEnabled(jCheckBoxExo.isSelected());
        jFormattedTextFieldKmInhibitorExo.setEnabled(jCheckBoxExo.isSelected());
        jLabelKmSatByTemp.setEnabled(graph.saturableExo);
        jCheckBoxSatByTemp.setEnabled(graph.saturableExo);
        jFormattedTextFieldSatByTemp.setEnabled(graph.saturableExo);
    
}

private void jCheckBoxPolyActionPerformed(java.awt.event.ActionEvent evt) {
	graph.saturablePoly = jCheckBoxPoly.isSelected();
   
    jLabelKmPoly.setEnabled(jCheckBoxPoly.isSelected());
    jLabelKmPoly1.setEnabled(jCheckBoxPoly.isSelected());
    jFormattedTextFieldKmPoly.setEnabled(jCheckBoxPoly.isSelected());
    jFormattedTextFieldKmPoly1.setEnabled(jCheckBoxPoly.isSelected());
}

private void jCheckBoxNickActionPerformed(java.awt.event.ActionEvent evt) {
	graph.saturableNick = jCheckBoxNick.isSelected();
   
    jLabelKmNick.setEnabled(jCheckBoxNick.isSelected());
    
    jFormattedTextFieldKmNick.setEnabled(jCheckBoxNick.isSelected());
}

    @SuppressWarnings("unchecked")
	private void jFormattedTextFieldParamMaxActionPerformed(java.awt.AWTEvent evt) {
    	DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTreeParams.getSelectionModel().getLeadSelectionPath().getLastPathComponent();

		if(node.getLevel() >1){
		((Parameter<V,E>) node.getUserObject()).maxValue = Double.parseDouble(this.jFormattedTextFieldParamMax.getText());
		
		} else {
			Enumeration<DefaultMutableTreeNode> it = (Enumeration<DefaultMutableTreeNode>) ((MutableTreeNode) node).children();
			while(it.hasMoreElements()){
				DefaultMutableTreeNode next = it.nextElement();
				((Parameter<V,E>) next.getUserObject()).maxValue = Double.parseDouble(this.jFormattedTextFieldParamMax.getText());
			}
		}
    }

    private void jFormattedTextFieldKmSimpleExo2ActionPerformed(java.awt.event.ActionEvent evt) {
        // Nothing, since only one km
    }
    
    private void jFormattedTextFieldParamDefActionPerformed(java.awt.AWTEvent evt) {
    	optimizer.Constants.thresholdRatio = 1.0/Double.parseDouble(this.jFormattedTextFieldParamDef.getText());
	}

    @SuppressWarnings("unchecked")
	private void jFormattedTextFieldParamMinActionPerformed(java.awt.AWTEvent evt) {
    	DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTreeParams.getSelectionModel().getLeadSelectionPath().getLastPathComponent();

		if(node.getLevel() >1){
		((Parameter<V,E>) node.getUserObject()).minValue = Double.parseDouble(this.jFormattedTextFieldParamMin.getText());
		
		} else {
			Enumeration<DefaultMutableTreeNode> it = (Enumeration<DefaultMutableTreeNode>) ((MutableTreeNode) node).children();
			while(it.hasMoreElements()){
				DefaultMutableTreeNode next = it.nextElement();
				((Parameter<V,E>) next.getUserObject()).minValue = Double.parseDouble(this.jFormattedTextFieldParamMin.getText());
			}
		}
    }

    @SuppressWarnings("unchecked")
	private void jTreeParamsValueChanged(javax.swing.event.TreeSelectionEvent evt) {
        TreePath path = evt.getNewLeadSelectionPath();
        if(path == null){
            return;
        }
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent(); 
         if (node == null){
        //Nothing is selected.     
            return;
         }
         jLabelParameterName.setText(node.toString());
         jLabelParameterName.setVisible(true);
         jLabelParamMin.setVisible(true);
         jLabelParamMax.setVisible(true);
         jLabelParamCurrent.setVisible(true);
         jFormattedTextFieldParamCurrent.setVisible(true);
         jFormattedTextFieldParamMin.setVisible(true);
         jFormattedTextFieldParamMax.setVisible(true);
         if(node.getLevel()>1){
//        	 jFormattedTextFieldParamCurrent.setVisible(true);
//        	 jLabelParamCurrent.setVisible(true);
//        	 jLabelParamDefault.setVisible(true);
//        	 jFormattedTextFieldParamDef.setVisible(true);
        	 jFormattedTextFieldParamCurrent.setValue(((Parameter<V,E>) node.getUserObject()).currentValue);
        	 jFormattedTextFieldParamMin.setValue(((Parameter<V,E>) node.getUserObject()).minValue);
        	 jFormattedTextFieldParamMax.setValue(((Parameter<V,E>) node.getUserObject()).maxValue);
        	 jCheckBox1.setSelected(((Parameter<V,E>) node.getUserObject()).optimizable);
         } else if (node.getLevel() == 1){
        	 jCheckBox1.setSelected(true);
        	 double min = -1;
        	 double max = -1;
        	 Enumeration<DefaultMutableTreeNode> it = (Enumeration<DefaultMutableTreeNode>) ((MutableTreeNode) node).children();
 			 while(it.hasMoreElements()){
 				DefaultMutableTreeNode next = it.nextElement();
 				if(min == -1 || max == -1){
 					min = ((Parameter<V,E>) next.getUserObject()).minValue;
 					max = ((Parameter<V,E>) next.getUserObject()).maxValue;
 					jFormattedTextFieldParamMin.setText(""+min);
 					jFormattedTextFieldParamMax.setText(""+max);
 				} else {
 					if(min != ((Parameter<V,E>) next.getUserObject()).minValue){
 						jFormattedTextFieldParamMin.setText("");
 					}
 					if(max != ((Parameter<V,E>) next.getUserObject()).maxValue){
 						jFormattedTextFieldParamMax.setText("");
 					}
 				}
 				if(!((Parameter<V,E>) next.getUserObject()).optimizable){
 					jCheckBox1.setSelected(false);
 				}
 			}
         } 
    }
    
    @SuppressWarnings("unchecked")
	private void optimizeCheckBoxClicked() {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTreeParams.getSelectionModel().getLeadSelectionPath().getLastPathComponent();

		if(node.getLevel() >1){
		((Parameter<V,E>) node.getUserObject()).optimizable = jCheckBox1.isSelected();
		} else {
			Enumeration<DefaultMutableTreeNode> it = (Enumeration<DefaultMutableTreeNode>) ((MutableTreeNode) node).children();
			while(it.hasMoreElements()){
				DefaultMutableTreeNode next = it.nextElement();
				((Parameter<V,E>) next.getUserObject()).optimizable = jCheckBox1.isSelected();
			}
		}
	}
    
    private void resetValues(){
		//Constants.exoVm = 300;
	    //Constants.exoKmSimple = 440;
	    //Constants.exoKmInhib = 150;
		//Constants.exoKmTemplate = 40;
	    
	    //Constants.polVm = 2100;
	    //Constants.polKm = 80;
	    //Constants.polKmBoth = 5.5;
	    
	    //Constants.nickVm = 80;
	    //Constants.nickKm = 30;
	    //Constants.ratioSelfStart = 1e-10;
    	Constants.reset();
	    graph.saturableExo = true;
	    graph.saturableNick = true;
	    graph.saturablePoly = true;
	    
	    graph.exoConc = 1;
	    graph.polConc = 1;
	    graph.nickConc = 1;
	    
	    jCheckBoxExo.setSelected(true);
	    jCheckBoxPoly.setSelected(true);
	    jCheckBoxNick.setSelected(true);
	    
	    jFormattedTextFieldKmInhibitorExo.setText(""+Constants.exoKmInhib);
	    jFormattedTextFieldKmInhibitorExo.setEnabled(true);
	    jFormattedTextFieldKmNick.setText(""+Constants.nickKm);
	    jFormattedTextFieldKmNick.setEnabled(true);
	    jFormattedTextFieldKmPoly.setText(""+Constants.polKm);
	    jFormattedTextFieldKmPoly.setEnabled(true);
	    jFormattedTextFieldKmPoly1.setText(""+Constants.polKmBoth);
	    jFormattedTextFieldKmPoly1.setEnabled(true);
	    jFormattedTextFieldKmSimpleExo.setText(""+Constants.exoKmSimple);
	    jFormattedTextFieldKmSimpleExo.setEnabled(true);
	    jFormattedTextFieldVmExo.setText(""+Math.round(Constants.exoVm/Constants.exoKmSimple * 1000.0)/1000.0);
	    jFormattedTextFieldVmNick.setText(""+Math.round(Constants.nickVm/Constants.nickKm * 1000.0)/1000.0);
	    jFormattedTextFieldVmPoly.setText(""+Math.round(Constants.polVm/Constants.polKm * 1000.0)/1000.0);
	    
	    jLabelKmInhibitorExo.setEnabled(true);
	    jLabelKmNick.setEnabled(true);
	    jLabelKmPoly.setEnabled(true);
	    jLabelKmPoly1.setEnabled(true);
	    jLabelKmSimpleExo.setEnabled(true);
	    
	    
	    jFormattedTextFieldSatByTemp.setText(Double.toString(Constants.exoKmTemplate));
	    
	    jCheckBoxSatByTemp.setSelected(true);
	    jCheckBoxSatByTemp.setEnabled(true);
		jCheckBoxSelfStart.setSelected(false);
		jFormattedTextFieldSelfStart.setText(""+Constants.ratioSelfStart);
		jCheckBoxCoupling.setSelected(true);
		jCheckBoxCoupling.setEnabled(true);
		jLabelKmSatByTemp.setEnabled(true);
        jFormattedTextFieldSatByTemp.setEnabled(true);
        jLabelKmSelfStart.setEnabled(false);
        jFormattedTextFieldSelfStart.setEnabled(false);
		
		graph.coupling = true;
		graph.dangle = true;
		graph.setSatExoByTemplate(Constants.exoKmTemplate);
		graph.selfStart = false;
    }
    
}
