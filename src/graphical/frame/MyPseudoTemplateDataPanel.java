package graphical.frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import model.OligoGraph;
import model.chemicals.SequenceVertex;

public class MyPseudoTemplateDataPanel<E> extends DataPanel<E> {

	public MyPseudoTemplateDataPanel(OligoGraph<SequenceVertex, E> graph) {
		super(graph);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ActionListener generateTemplateActionListener(final E edge){
		ActionListener ret = new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ModTemplateWindow<E> win = new ModTemplateWindow<E>(edge,graph);
				win.addWindowListener(new WindowAdapter(){

					@Override
					public void windowClosing(WindowEvent arg0) {
						update();
						getParent().repaint();
						
					}
					
					@Override
					public void windowClosed(WindowEvent arg0) {
						update();
						getParent().repaint();
						
					}
					
				});
				win.setVisible(true);
//				graph.removeEdge(edge);
//				update();
//				getParent().repaint();
			}};
			return ret;
	}
	
}
