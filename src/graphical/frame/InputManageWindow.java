package graphical.frame;


import model.OligoGraph;
import model.chemicals.SequenceVertex;
import model.input.AbstractInput;
import model.input.PulseInput;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class InputManageWindow<V> extends JFrame implements ActionListener, ListSelectionListener {
 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	SequenceVertex vert;
	DefaultListModel<AbstractInput> dlm;
	InputEditWindow iew = null;
	final OligoGraph<V,?> graph;
	
	InputManageWindow(V vert, OligoGraph<V,?> graph){
		super("Input manager");
		this.vert = (SequenceVertex) vert;
		this.graph = graph;
		JPanel panel = new JPanel();
		JPanel buttonpanel = new JPanel();
		JLabel label = new JLabel("Inputs for "+vert);
		this.dlm = new DefaultListModel<AbstractInput>();
		for (AbstractInput input : this.vert.inputs){
		dlm.addElement(input);
		}
		JList<AbstractInput> jl = new JList<AbstractInput>(dlm);
		jl.addListSelectionListener(this);
		JScrollPane listScroller = new JScrollPane(jl);
		JButton addbutton = new JButton("add");
		addbutton.setActionCommand("add");
		addbutton.addActionListener(this);
		
		//JButton save = new JButton("save");
		//save.setActionCommand("save");
		//save.addActionListener(this);
		listScroller.setPreferredSize(new Dimension(250, 80));
		listScroller.setAlignmentX(LEFT_ALIGNMENT);
		
		panel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

		panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
		buttonpanel.setLayout(new BoxLayout(buttonpanel,BoxLayout.X_AXIS));

		panel.add(label);
		panel.add(Box.createRigidArea(new Dimension(0,5)));
		panel.add(listScroller);
		buttonpanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
		buttonpanel.add(Box.createHorizontalGlue());
		buttonpanel.add(addbutton);
		buttonpanel.add(Box.createRigidArea(new Dimension(10,0)));
		//buttonpanel.add(save);
		
		Container content = this.getContentPane();
		content.setLayout(new BoxLayout(content,BoxLayout.Y_AXIS));
		
		content.add(panel);
		content.add(buttonpanel);

		this.pack();
		//System.out.println("Buttonpanel size:"+buttonpanel.getWidth()+" Panel size:"+panel.getWidth());
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getActionCommand() == "add" ){
			PulseInput newinput = new PulseInput(100,50);
			vert.inputs.add(newinput);
			dlm.addElement(newinput);
			graph.replot();
			this.repaint();
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		if (((JList<AbstractInput>)arg0.getSource()).getSelectedValue() != null && iew == null){
		iew = new InputEditWindow(this,(AbstractInput)((JList<AbstractInput>)arg0.getSource()).getSelectedValue());
		((JList<AbstractInput>)arg0.getSource()).clearSelection();
		iew.setVisible(true);
		iew.setAlwaysOnTop(true);
		iew.addWindowListener(new WindowAdapter(){

			@Override
			public void windowClosing(WindowEvent arg0) {
				iew = null;
				
			}
			
		});
		}
	}

	public void deleteInput(AbstractInput input) {
		vert.inputs.remove(input);
		this.dlm.removeElement(input);
		this.repaint();
		graph.replot();
	}
}
