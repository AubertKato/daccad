package graphical.frame;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.JFileChooser;
import javax.swing.ProgressMonitor;

import model.Constants;
import model.OligoGraph;
import model.OligoSystemAllSats;
import model.PseudoTemplateGraph;
import model.PseudoTemplateOligoSystem;
import model.chemicals.SequenceVertex;
import utils.MyWorker;
import edu.uci.ics.jung.algorithms.layout.Layout;
import graphical.frame.DataPanel;
import graphical.frame.MyActionlistener;

public class MyPseudoTemplateActionListener extends
		MyActionlistener<SequenceVertex, String> {

	public MyPseudoTemplateActionListener(Layout<SequenceVertex, String> layout, DataPanel<String> data) {
		super(layout, data);
		
	}
	
	@Override
	protected void plot() {
		
		if(PseudoTemplateGraph.class.isAssignableFrom(graph.getClass())){
			model = new PseudoTemplateOligoSystem((PseudoTemplateGraph<SequenceVertex,String>)this.graph);
		} else {
			super.plot(); //Somehow, we are using a different interface
			return;
		}
		
		final MyWorker myWorker = new MyWorker(model,graph);
		
		ProgressMonitor progressMonitor = new ProgressMonitor(null,
	            "Simulating",
	            "", 0, 100);
		progressMonitor.setProgress(0);

		final ProgressMonitor cop = progressMonitor;
		
		myWorker.setProg(cop);
		myWorker.setCustomProgress(0);
		myWorker.addPropertyChangeListener(new PropertyChangeListener(){

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				//System.out.println(evt.getPropertyName());
				if ("progress".equals(evt.getPropertyName()) ) {
					//System.out.println("Balsdkbgn");
		            int progress = (Integer) evt.getNewValue();
		            cop.setProgress(progress);
				}
				
			}
			
		});
		
		myWorker.execute();
	}
	
	@Override
	protected void exportTimeSerie() {
		if(PseudoTemplateGraph.class.isAssignableFrom(graph.getClass())){
			model = new  PseudoTemplateOligoSystem((PseudoTemplateGraph<SequenceVertex, String>)graph);
		} else {
			model = new OligoSystemAllSats<String>((OligoGraph<SequenceVertex, String>) this.graph);
		}
		double[][] res = model.calculateTimeSeries(null);
		StringBuilder export = new StringBuilder("#");
		for(SequenceVertex s : graph.getVertices()){
			export.append(s+"\t");
		}
		if(Constants.fullExport){
		Iterator<String> it = model.templates.keySet().iterator();
		while(it.hasNext()){
			String e = it.next();
			export.append(""+e+"alone"+"\t"+e+"in"+"\t"+e+"out"+"\t"+e+"both"+"\t"+e+"ext"+"\t"+e+"inhibited"+"\t");
		}
		}
		export.append("\n");
		
		for(int t=0;t<res[0].length;t++){
			for(int i=0;i<(Constants.fullExport?res.length:graph.getVertexCount());i++){
				export.append(""+ res[i][t] + "\t");
			}
			export.append("\n");
		}
		JFileChooser exp = new JFileChooser();
		int result = exp.showSaveDialog(null);
		if (result == JFileChooser.APPROVE_OPTION){
			File selectedFile = exp.getSelectedFile();
			try {
				if (!selectedFile.exists()){

					selectedFile.createNewFile();
				
				}
				
				FileWriter fw = new FileWriter(selectedFile);
				fw.write(export.toString());
				fw.flush();
				fw.close();
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	}

}
