package graphical.frame;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;

import com.google.common.base.Function;

import edu.uci.ics.jung.algorithms.layout.Layout;


public class GraphicInterfaceMouseAdapter implements MouseListener,MouseMotionListener{
	
	boolean edit = false;
	boolean rightclic = false;
	boolean draganddrop = false;
	boolean multiselect = false;
	Point start = null;
	Point now = null;
	Object dragged = null;
	
	public void mouseDragged(MouseEvent arg0){
			now = new Point(arg0.getX(),arg0.getY());
			arg0.getComponent().repaint();
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		if (arg0.getButton() == MouseEvent.BUTTON1){
		rightclic = false;
		edit = ((arg0.getModifiersEx() & MouseEvent.SHIFT_DOWN_MASK) == MouseEvent.SHIFT_DOWN_MASK);
		now = arg0.getPoint();
		this.multiselect = false; //Just to be safe;
		this.draganddrop = false;
		arg0.getComponent().repaint();
		} else if (arg0.getButton() == MouseEvent.BUTTON3){
//		rightclic = true;
//		now = arg0.getPoint();
//		this.multiselect = false; //Just to be safe;
//		this.draganddrop = false;
//		arg0.getComponent().repaint();
			if(MyVisualizationServer.class.isAssignableFrom(arg0.getComponent().getClass())){
				((MyVisualizationServer<?, ?>) arg0.getComponent()).rightclic(arg0.getPoint());
				arg0.getComponent().repaint();
			}
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void mousePressed(MouseEvent arg0) {
		
		start = arg0.getPoint();
		Layout<Object, ?> layout = (Layout<Object, ?>) ((MyVisualizationServer<Object, ?>) arg0.getSource()).getJUNGLayout();
		Function<Object, Shape> shapeTransformer = ((MyVisualizationServer<Object, ?>) arg0.getSource()).getRenderContext().getVertexShapeTransformer();
		for(Object o :layout.getGraph().getVertices()){
			AffineTransform xform = AffineTransform.getTranslateInstance(layout.apply(o).getX(), layout.apply(o).getY());
			Shape shape = shapeTransformer.apply(o);
			shape = xform.createTransformedShape(shape);
			
		if(shape.contains(start)){
			this.dragged = o;
			this.draganddrop = true;
			break;
		}
			
		}
		if (!this.draganddrop){
			this.multiselect = true;
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		this.multiselect = false;
		this.draganddrop = false;
		this.edit = false;
		start = null;
		now = null;
		arg0.getComponent().repaint();
	}

}
