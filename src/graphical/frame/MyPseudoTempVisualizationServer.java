package graphical.frame;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.Layer;
import graphical.frame.DataPanel;
import graphical.frame.GraphicInterfaceMouseAdapter;
import graphical.frame.InputManageWindow;
import graphical.frame.MyVisualizationServer;
import model.OligoGraph;
import model.PseudoTemplateGraph;
import model.chemicals.PseudoExtendedSequence;
import model.chemicals.SequenceVertex;

public class MyPseudoTempVisualizationServer<E> extends
		MyVisualizationServer<SequenceVertex, E> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyPseudoTempVisualizationServer(Layout<SequenceVertex, E> layout,
			GraphicInterfaceMouseAdapter mouse, DataPanel<E> data) {
		super(layout, mouse, data);
		
	}

	@Override
	public void rightclic(Point point) {
		Point2D p;
		AffineTransform affine;
		JPopupMenu pm = new JPopupMenu();
		
		for (SequenceVertex v: layout.getGraph().getVertices()){
			p = layout.apply(v);
			p = this.getRenderContext().getMultiLayerTransformer().transform(Layer.LAYOUT, p);
			affine = AffineTransform.getTranslateInstance(p.getX(), p.getY());
			if (affine.createTransformedShape(this.getRenderContext().getVertexShapeTransformer().apply(v)).contains(point))
			{
				
				JLabel title = new JLabel(""+v);
				pm.add(title);
				pm.add(new JSeparator());
				JMenuItem menuitem = new JMenuItem("Delete");
				menuitem.setMnemonic(KeyEvent.VK_D);
				menuitem.getAccessibleContext().setAccessibleDescription(
			        "Delete Sequence");
				menuitem.setActionCommand("delete");
				final SequenceVertex selected = v;
				menuitem.addActionListener(new ActionListener(){
		        	public void actionPerformed(ActionEvent e){
		        		OligoGraph<SequenceVertex,E> graph = ((OligoGraph<SequenceVertex,E>) layout.getGraph());
		        		graph.getSelected().add(selected);
		        		ArrayList<SequenceVertex> selec = new ArrayList<SequenceVertex>(graph.getSelected());
		        		for (SequenceVertex i: selec){
		        			if(graph.getVertices().contains(i))
		        				graph.removeVertex(i);
		        			 
		        		 }
		        		graph.getSelected().clear();
		        		 data.update();
		        		 data.getParent().repaint();
		        		 graph.replot(); //reploted once
		        	}
				});
				pm.add(menuitem);
				menuitem = new JMenuItem("Add input");
				menuitem.setActionCommand("addinput");
				menuitem.addActionListener(new ActionListener(){
		        	public void actionPerformed(ActionEvent e){
		        		InputManageWindow<SequenceVertex> imw = new InputManageWindow<SequenceVertex>(selected,(OligoGraph<SequenceVertex,E>)layout.getGraph());
		        		imw.addWindowListener(new WindowAdapter(){
		        			@Override
		        			public void windowClosing(WindowEvent arg0) {
		        				data.getParent().repaint();
		        			}
		        		});
		        		imw.setVisible(true);
		        		imw.setAlwaysOnTop(true);
		        	}
				});
				pm.add(menuitem);
				if(!v.isInhib()){
					final SequenceVertex trueV = v;
					final PseudoTemplateGraph<SequenceVertex,E> graph = ((PseudoTemplateGraph<SequenceVertex,E>) layout.getGraph());
					if(graph.getExtendedSpecies(v)==null){
					menuitem = new JMenuItem("Add pseudo-template");
					menuitem.setActionCommand("addpseudo");
					menuitem.addActionListener(new ActionListener(){

						@Override
						public void actionPerformed(ActionEvent arg0) {
							
							
							
								if(graph.getVertices().contains(trueV)){
									PseudoExtendedSequence newi = new PseudoExtendedSequence(trueV,0.0);
									graph.setExtendedSpecies(trueV, newi);
								}
		        			 
							
							data.getParent().repaint();
						}
					
					});
					pm.add(menuitem);
					} else {
						// We are in the case where there is already a pseudo-template
						menuitem = new JMenuItem("Edit pseudo-template");
						menuitem.setActionCommand("editpseudo");
						menuitem.addActionListener(new ActionListener(){

							@Override
							public void actionPerformed(ActionEvent arg0) {
								
								
									new PseudoTemplateWindow<SequenceVertex,E>(graph.getEdgeFactory().createEdge(trueV, graph.getExtendedSpecies(trueV)),trueV,graph).setVisible(true);
			        			 
								
							}
						
						});
						pm.add(menuitem);
					}
				}
				pm.show(this,(int) Math.round(point.getX()),(int) Math.round(point.getY()));
				break;
			}
		}
		
	}

}
