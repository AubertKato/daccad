package graphical.frame;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.picking.MultiPickedState;
import graphical.graphRendering.MyVertexFillPaintTransformer;
import graphical.graphRendering.OligoRenderer;

import static java.awt.event.InputEvent.CTRL_MASK;
import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;


import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.border.LineBorder;

import java.util.Iterator;

import utils.GraphUtils;
import model.OligoGraph;
import model.chemicals.SequenceVertex;

public class MainFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected OligoGraph<SequenceVertex, String> graph;
	
	
	
	public MainFrame(){
		super("DACCAD");
		initGraph();
		initComponents();
		this.addWindowListener(new java.awt.event.WindowAdapter(){


			@Override
			public void windowClosing(WindowEvent arg0) {
				if(!graph.saved){
					int ret = JOptionPane.showConfirmDialog(null, "Graph is not saved. Close?", "Warning", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
					if(ret == JOptionPane.CANCEL_OPTION){
						return;
					}
				}
				System.exit(0);
			}
			
		});
	}
	
	//For future extensions
	protected MainFrame(String customTitle){
		super(customTitle);
		initGraph();
		initComponents();
		this.addWindowListener(new java.awt.event.WindowAdapter(){


			@Override
			public void windowClosing(WindowEvent arg0) {
				if(!graph.saved){
					int ret = JOptionPane.showConfirmDialog(null, "Graph is not saved. Close?", "Warning", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
					if(ret == JOptionPane.CANCEL_OPTION){
						return;
					}
				}
				System.exit(0);
			}
			
		});
	}
	
	protected void initGraph(){
		final OligoGraph<SequenceVertex, String> g = GraphUtils.initGraph();
	    this.graph = g;
	}
	
	protected void initComponents(){
		boolean isMac = (System.getProperty("os.name").toLowerCase().indexOf("mac")>=0 );
		
		final DataPanel<String> dataPanel = new DataPanel<String>(graph);
		JMenuBar jMenuBar = new javax.swing.JMenuBar();
        JMenu jMenuFile = new javax.swing.JMenu();
        JMenuItem jMenuItemNew = new javax.swing.JMenuItem();
        JMenuItem jMenuItemOpen = new javax.swing.JMenuItem();
        JMenuItem jMenuItemSave = new javax.swing.JMenuItem();
        JMenuItem jMenuItemSaveAs = new javax.swing.JMenuItem();
        JMenuItem jMenuItemUndo = new javax.swing.JMenuItem();
        JSeparator jSeparator = new javax.swing.JPopupMenu.Separator();
        JMenuItem jMenuItemExport = new javax.swing.JMenuItem();
        JMenu jMenuSequences = new javax.swing.JMenu();
        JMenuItem jMenuItemNewSeq = new javax.swing.JMenuItem();
        JMenuItem jMenuItemFusion = new javax.swing.JMenuItem();
        JMenuItem jMenuItemDuplicate = new javax.swing.JMenuItem();
        JMenuItem jMenuItemImport = new javax.swing.JMenuItem();
        JMenuItem jMenuOptions = new javax.swing.JMenuItem();
        //JMenu jMenuPlotMenu = new javax.swing.JMenu();
        JMenuItem jMenuPlot = new javax.swing.JMenuItem();
        //JMenuItem jMenuPlotRange = new javax.swing.JMenuItem();
        JMenuItem jMenuImportInput = new javax.swing.JMenuItem();
        
        
         // The BasicVisualizationServer<V,E> is parameterized by the edge types
         MyVisualizationServer<SequenceVertex,String> vv = initVisualizationServer(dataPanel);
                  
         
         
         
         MyActionlistener<SequenceVertex,String> listen = initActionListener(vv, dataPanel);
         //Add all the action listening here
         jMenuItemNew.setActionCommand("new");
         jMenuItemNew.addActionListener(listen);
         
         jMenuItemOpen.setActionCommand("open");
         jMenuItemOpen.addActionListener(listen);
         
         jMenuItemSave.setActionCommand("save");
         jMenuItemSave.addActionListener(listen);
         
         jMenuItemSaveAs.setActionCommand("saveas");
         jMenuItemSaveAs.addActionListener(listen);
         
         jMenuItemUndo.setActionCommand("undo");
         jMenuItemUndo.addActionListener(listen);
         
         jMenuItemExport.setActionCommand("export");
         jMenuItemExport.addActionListener(listen);
         
         jMenuItemNewSeq.setActionCommand("newseq");
         jMenuItemNewSeq.addActionListener(listen);
         
         jMenuItemFusion.setActionCommand("fusion");
         jMenuItemFusion.addActionListener(listen);
         
         jMenuItemDuplicate.setActionCommand("duplicate");
         jMenuItemDuplicate.addActionListener(listen);
         
         jMenuItemImport.setActionCommand("import");
         jMenuItemImport.addActionListener(listen);
         
         jMenuImportInput.setActionCommand("impInput");
         jMenuImportInput.addActionListener(listen);
         
         jMenuOptions.setActionCommand("options");
         jMenuOptions.addActionListener(listen);
         
         jMenuPlot.setActionCommand("plot");
         jMenuPlot.addActionListener(listen);
         
         //jMenuPlotRange.setActionCommand("plotrange");
         //jMenuPlotRange.addActionListener(listen);
         
         setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
         
         jMenuBar.setBackground(new java.awt.Color(204, 199, 196));
         jMenuOptions.setBackground(new java.awt.Color(204, 199, 196));
         jMenuPlot.setBackground(new java.awt.Color(204, 199, 196));

         jMenuFile.setText("File");

         jMenuItemNew.setAccelerator(getKeyStroke(java.awt.event.KeyEvent.VK_N, CTRL_MASK));
         jMenuItemNew.setText("New");
         
        
         jMenuFile.add(jMenuItemNew);

         jMenuItemOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, CTRL_MASK));
         jMenuItemOpen.setText("Open");
         jMenuFile.add(jMenuItemOpen);

         jMenuItemSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, CTRL_MASK));
         jMenuItemSave.setText("Save");
         jMenuFile.add(jMenuItemSave);
         
         jMenuItemSaveAs.setText("Save as");
         jMenuFile.add(jMenuItemSaveAs);

         jMenuItemUndo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, CTRL_MASK));
         jMenuItemUndo.setText("Undo");
         jMenuFile.add(jMenuItemUndo);
         
         jSeparator.setBackground(new java.awt.Color(236, 151, 66));
         jSeparator.setForeground(new java.awt.Color(186, 94, 94));
         jMenuFile.add(jSeparator);

         jMenuItemExport.setText("Export");
         jMenuFile.add(jMenuItemExport);

         jMenuBar.add(jMenuFile);

         jMenuSequences.setText("Species");

         jMenuItemNewSeq.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, CTRL_MASK));
         jMenuItemNewSeq.setText("New species");
         jMenuSequences.add(jMenuItemNewSeq);

         jMenuItemFusion.setText("Join selected");
         jMenuSequences.add(jMenuItemFusion);

         jMenuItemDuplicate.setText("Duplicate selected");
         jMenuSequences.add(jMenuItemDuplicate);

         jMenuItemImport.setText("Import module");
         jMenuSequences.add(jMenuItemImport);

         jMenuImportInput.setText("Import input");
         jMenuSequences.add(jMenuImportInput);
         
         jMenuBar.add(jMenuSequences);

         jMenuOptions.setText("Options");
         

         //jMenuPlotMenu.setText("Plot");
         jMenuPlot.setText("Plot");
         jMenuPlot.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, CTRL_MASK));
         //jMenuPlotRange.setText("Range");
         //jMenuPlotMenu.add(jMenuPlot);
         //jMenuPlotMenu.add(jMenuPlotRange);
         jMenuBar.add(jMenuOptions);
         jMenuBar.add(jMenuPlot);   
         
         
         setJMenuBar(jMenuBar);

         javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
         getContentPane().setLayout(layout);
         layout.setHorizontalGroup(
             layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
             .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                 .addContainerGap()
                 .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                     .addComponent(dataPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                     .addComponent(vv))
                 .addContainerGap())
         );
         layout.setVerticalGroup(
             layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
             .addGroup(layout.createSequentialGroup()
                 .addContainerGap()
                 .addComponent(vv, 0,300,Short.MAX_VALUE)
                 .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                 .addComponent(dataPanel, isMac?180:150, isMac?180:150, 180)
                 .addContainerGap())
         );
         
         //Other actions
         final OligoGraph<SequenceVertex,String> g = this.graph;
         this.rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0),
         "escaped");
         this.rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE,0),
         "delete");
         
         this.rootPane.getActionMap().put("escaped",
                 new AbstractAction(){

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent arg0) {

						g.getSelected().clear();
						dataPanel.update();
						repaint();
					}
        	 
         });
         this.rootPane.getActionMap().put("delete",
                 new AbstractAction(){

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent arg0) {
						Iterator<SequenceVertex> it = g.getSelected().iterator();
						while(it.hasNext()){
							SequenceVertex s = it.next();
							if(g.getVertices().contains(s))
								g.removeVertex(s);
						}
						g.getSelected().clear();
						dataPanel.update();
						repaint();
					}
        	 
         });
         
         pack();
	}
	
	protected MyVisualizationServer<SequenceVertex,String> initVisualizationServer(DataPanel<String> dataPanel){
		GraphicInterfaceMouseAdapter myadapter = new GraphicInterfaceMouseAdapter();
        
        Layout<SequenceVertex, String> frlayout = new FRLayout<SequenceVertex, String>(graph);
        frlayout.setSize(new Dimension(300,300));
		MyVisualizationServer<SequenceVertex,String> vv = new MyVisualizationServer<SequenceVertex,String>(frlayout, myadapter,dataPanel);
        vv.setRenderer(new OligoRenderer<SequenceVertex,String>());
        vv.setBorder(LineBorder.createBlackLineBorder());
        vv.addMouseListener(myadapter);
        vv.addMouseMotionListener(myadapter);
        vv.getRenderContext().setPickedVertexState(new MultiPickedState<SequenceVertex>());
        vv.getRenderContext().setVertexFillPaintTransformer(new MyVertexFillPaintTransformer<SequenceVertex>(graph));
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        
        return vv;
	}
	
	protected MyActionlistener<SequenceVertex,String> initActionListener(MyVisualizationServer<SequenceVertex,String> vv, DataPanel<String> dataPanel){
		return new MyActionlistener<SequenceVertex,String>(vv.getGraphLayout(),dataPanel);
	}
	
	public static void main(String[] args){
		
		MainFrame myframe = new MainFrame();
		myframe.setVisible(true);
		
	}
}
