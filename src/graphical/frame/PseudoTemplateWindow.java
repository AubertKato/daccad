package graphical.frame;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ToolTipManager;

import graphical.frame.TemplateWindow;
import model.OligoGraph;
import model.PseudoTemplateGraph;
/**
 * This class is supposed to give a nice viewer for pseudo temp
 * parameters
 * @author naubertkato
 *
 * @param <E>
 */

public class PseudoTemplateWindow<V,E> extends TemplateWindow<E> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final V seq;

	PseudoTemplateWindow(E template, V seq, OligoGraph<V, E> graph) {
		super(template, graph);
		this.seq = seq;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected JButton initRemoveButton(){
		JButton button =new JButton("Remove template");
		button.addActionListener(new ActionListener(){

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				((PseudoTemplateGraph<V,E>) graph).removeExtendedSpecies(seq);;
				//graph.removeEdge(template);
				graph.replot();
				dispose();
			}
			
		});
		return button;
	}
	
protected void initComponent() {
		
		ToolTipManager.sharedInstance().setDismissDelay(Short.MAX_VALUE);
    	ToolTipManager.sharedInstance().setInitialDelay(0);
		
		JPanel panelup = new JPanel();
		JPanel panelmiddle = new JPanel();
		JPanel paneldown = new JPanel();
		panelup.setLayout(new BoxLayout(panelup,BoxLayout.LINE_AXIS));
		panelmiddle.setLayout(new BoxLayout(panelmiddle,BoxLayout.PAGE_AXIS));
		paneldown.setLayout(new BoxLayout(paneldown, BoxLayout.LINE_AXIS));
		
		Container content = this.getContentPane();
		content.setLayout(new BoxLayout(content,BoxLayout.PAGE_AXIS));
		
		JLabel concLab = new JLabel("Template concentration: ");
		final JTextField conc = new JTextField(""+graph.getTemplateConcentration(template));
		conc.setPreferredSize(new Dimension(100,18));
		conc.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				setTemplateValue(Double.parseDouble(conc.getText()));
			}
			
		});
		conc.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {
				
				
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				setTemplateValue(Double.parseDouble(conc.getText()));
				
			}
			
		});
		panelup.add(concLab);
		panelup.add(conc);
		content.add(panelup);
		
		final JTextField dangleL = new JTextField(""+graph.getDangleL(template));
		
		if(graph.dangle){
		JLabel dangleLLab = new JLabel("Left dangle slowdown: ");
		dangleLLab.setToolTipText("<html>Represents the increased stability of<br>a structure where the input is<br>present.<br>Based on Santa Lucia et al.:" +
						
						"</html>");
		
		
		dangleLLab.setLabelFor(dangleL);
		
		JPanel p3 = new JPanel();
		
		p3.setLayout(new BoxLayout(p3,BoxLayout.LINE_AXIS));
		
		
		p3.add(dangleLLab);
		p3.add(dangleL);
		panelmiddle.add(p3);
		dangleL.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				setDangleLValue(Double.parseDouble(dangleL.getText()));
			}
			
		});
		dangleL.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent arg0) {
				
				
			}

			@Override
			public void focusLost(FocusEvent arg0) {
				setDangleLValue(Double.parseDouble(dangleL.getText()));
				
			}
			
		});
		}
		
		if(PseudoTemplateGraph.class.isAssignableFrom(graph.getClass())){
			PseudoTemplateGraph<?,E> pGraph = (PseudoTemplateGraph<?,E>) graph;
			JPanel p5 = new JPanel();
		
			p5.setLayout(new BoxLayout(p5,BoxLayout.LINE_AXIS));
			final JTextField missingBases = new JTextField(""+pGraph.getInputSlowdown(template));
		

			JLabel missingLab = new JLabel("Missing base input slowdown: ");
			missingLab.setToolTipText("<html>Represents the lowered stability of<br>a structure where the input domain is<br>missing a few bases." +	
						"</html>");
			
		
			missingLab.setLabelFor(missingBases);
			p5.add(missingLab);
			p5.add(missingBases);
			panelmiddle.add(p5);
			missingBases.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					setDangleRValue(Double.parseDouble(missingBases.getText()));
				}
				
			});
			missingBases.addFocusListener(new FocusListener(){

				@Override
				public void focusGained(FocusEvent arg0) {}

				@Override
				public void focusLost(FocusEvent arg0) {
					setDangleRValue(Double.parseDouble(missingBases.getText()));	
				}
				
			});
			
		}
		
		content.add(panelmiddle);
		
		
		
		
		paneldown.add(Box.createHorizontalGlue());
		JButton remove = initRemoveButton();
		paneldown.add(remove);
		content.add(paneldown);
		this.pack();
	}

}
