package graphical.frame;

import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ProgressMonitor;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

import optimizer.Parameter;
import utils.CodeGenerator;
import utils.MyWorker;
import utils.PlotExpData;
import model.Constants;
import model.OligoGraph;
import model.OligoSystem;
import model.OligoSystemAllSats;
import model.chemicals.SequenceVertex;
import model.input.ExternalInput;
import edu.uci.ics.jung.algorithms.layout.Layout;


public class MyActionlistener <V extends SequenceVertex,E> implements ActionListener {

	protected OligoGraph<V,E> graph;
	protected Layout<V,E> layout;
	protected DataPanel<E> data;
	protected OligoSystem<E> model;
	protected OptionsFrameAdvanced<V,E> optionFrame = null;
	
	JFileChooser fc = new JFileChooser();
	FileDialog fd = null;
	FileNameExtensionFilter filter = new FileNameExtensionFilter(
		        "Graph files", "graph");
	File savingFile;
	//FileNameExtensionFilter exportsbmlfilter = new FileNameExtensionFilter( "SBML files", "sbml");
	//FileNameExtensionFilter exportMathematicafilter = new FileNameExtensionFilter( "Mathematica files", "");
	@SuppressWarnings("unchecked")
	public MyActionlistener(Layout<V,E> layout,DataPanel<E> data){
		this.layout = layout;
		this.graph= (OligoGraph<V,E>) layout.getGraph();
		this.data = data;
		this.model = new OligoSystem<E>((OligoGraph<SequenceVertex, E>) this.graph);
		if(System.getProperty("os.name").toLowerCase().indexOf("mac")>=0){
			fc = null;
			fd = new FileDialog((JFrame) null);
			fd.setFilenameFilter(new FilenameFilter(){

				@Override
				public boolean accept(File arg0, String arg1) {
					return arg1.endsWith(".graph");
				}
				
			});
		} else {
		fc.setFileFilter(filter);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getActionCommand() == "new"){
			if(!graph.saved){
				int ret = JOptionPane.showConfirmDialog(null, "Graph is not saved. Continue?", "Warning", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
				if(ret == JOptionPane.CANCEL_OPTION){
					return;
				}
			}
			savingFile = null;
			graph.totalReset();
			Constants.reset();
			data.update();
			data.getParent().repaint();
		} else if (arg0.getActionCommand() == "newseq"){
			newseq();
			
		} else if (arg0.getActionCommand() == "remove"){
			removevertex();
			
		} else if (arg0.getActionCommand() == "duplicate"){
			
			boolean rep = PlotExpData.autoplot;
			PlotExpData.autoplot = false;
			duplicate();
			PlotExpData.autoplot = rep;
			if(rep)
				graph.replot();
			
		} else if (arg0.getActionCommand() == "fusion"){
			boolean rep = PlotExpData.autoplot;
			
			PlotExpData.autoplot = false;
			
			fusion();
			PlotExpData.autoplot = rep;
			if(rep)
				graph.replot();
			
		} else if (arg0.getActionCommand() == "save"){
			if (savingFile == null){
				if(fc != null){
					int result = fc.showSaveDialog(null);
					if (result == JFileChooser.APPROVE_OPTION){
						this.savingFile = fc.getSelectedFile();
						performSave();
					}
				} else {
					// we are on mac
					fd.setMode(FileDialog.SAVE);
					fd.setVisible(true);
					if(fd.getFile() != null){
						String filterPath = fd.getDirectory();
						this.savingFile = new File(filterPath, fd.getFile());
						performSave();
					}
				}
			} else {
				performSave();
			}
		} else if (arg0.getActionCommand() == "saveas"){
			if(fc != null){
				int result = fc.showSaveDialog(null);
				if (result == JFileChooser.APPROVE_OPTION){
					this.savingFile = fc.getSelectedFile();
					performSave();
				}
			} else {
				fd.setMode(FileDialog.SAVE);
				fd.setVisible(true);
				if(fd.getFile() != null){
					String filterPath = fd.getDirectory();
					this.savingFile = new File(filterPath, fd.getFile());
					performSave();
				}
			}
		} else if (arg0.getActionCommand() == "open"){
			boolean rep = PlotExpData.autoplot;
			PlotExpData.autoplot = false;
			open();
			PlotExpData.autoplot = rep;
			if(rep)
				graph.replot();
		} else if (arg0.getActionCommand() == "import"){
			boolean rep = PlotExpData.autoplot;
			PlotExpData.autoplot = false;
			doimport();
			if(rep)
				graph.replot();
		} else if (arg0.getActionCommand() == "impInput"){
			doinput();
		} else if (arg0.getActionCommand() == "export"){
			Object[] options = {"SBML",
	                "Mathematica",
	                "Plot only"};
			int request = JOptionPane.showOptionDialog(null, "Export system","Format", JOptionPane.YES_NO_CANCEL_OPTION,
				    JOptionPane.QUESTION_MESSAGE,
				    null,
				    options,
				    options[0]);
			if (request == 0) {
				export();
			} else if (request == 1){
				exportMathematica();
			} else if (request == 2){
				exportTimeSerie();
				
			}
		} else if (arg0.getActionCommand() == "plot"){
				plot();
				
		} else if (arg0.getActionCommand() == "options"){
			if(this.optionFrame==null){
			 this.optionFrame = new OptionsFrameAdvanced<V,E>(graph);
			 this.optionFrame.addWindowListener(new WindowAdapter(){

				@Override
				public void windowClosing(WindowEvent arg0) {
					if(PlotExpData.autoplot)
						graph.replot();
					optionFrameClosed();
				}
				 
			 });
			this.optionFrame.setVisible(true);
			}
		} else if (arg0.getActionCommand() == "undo"){
			undo();
		}
	}

	private void optionFrameClosed(){
		this.optionFrame = null;
	}
	
	private void undo() {
		graph.undo();
		data.update();
		data.getParent().repaint();
	}

	@SuppressWarnings("unchecked")
	private void exportMathematica() {
		JFileChooser exp = new JFileChooser();
		int result = exp.showSaveDialog(null);
		if (result == JFileChooser.APPROVE_OPTION){
			File selectedFile = exp.getSelectedFile();
			try {
				if (!selectedFile.exists()){

					selectedFile.createNewFile();
				
				}
				
				FileWriter fw = new FileWriter(selectedFile);
				fw.write(CodeGenerator.exportToMathematicaCode((OligoGraph<SequenceVertex,E>) graph));
				fw.flush();
				fw.close();
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	protected void exportTimeSerie() {
		model = new OligoSystemAllSats<E>((OligoGraph<SequenceVertex, E>) this.graph);
		double[][] res = model.calculateTimeSeries(null);
		StringBuilder export = new StringBuilder("#");
		for(int i=0;i<this.graph.getVertexCount();i++){
			export.append("s"+i+"\t");
		}
		if(Constants.fullExport){
		Iterator<E> it = model.templates.keySet().iterator();
		while(it.hasNext()){
			E e = it.next();
			export.append(""+e+"alone"+"\t"+e+"in"+"\t"+e+"out"+"\t"+e+"both"+"\t"+e+"ext"+"\t"+e+"inhibited"+"\t");
		}
		}
		export.append("\n");
		
		for(int t=0;t<res[0].length;t++){
			for(int i=0;i<(Constants.fullExport?res.length:graph.getVertexCount());i++){
				export.append(""+ res[i][t] + "\t");
			}
			export.append("\n");
		}
		JFileChooser exp = new JFileChooser();
		int result = exp.showSaveDialog(null);
		if (result == JFileChooser.APPROVE_OPTION){
			File selectedFile = exp.getSelectedFile();
			try {
				if (!selectedFile.exists()){

					selectedFile.createNewFile();
				
				}
				
				FileWriter fw = new FileWriter(selectedFile);
				fw.write(export.toString());
				fw.flush();
				fw.close();
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void newseq(){
		
		Object[] options = {"Activation species",
                "Inhibiting species",
                "Cancel"};
		int request = JOptionPane.showOptionDialog(null, "Create a species?","New species", JOptionPane.YES_NO_CANCEL_OPTION,
			    JOptionPane.QUESTION_MESSAGE,
			    null,
			    options,
			    options[0]);
		V newvertex = null;
		if (request == 0){
			newvertex = graph.getVertexFactory().create();
			graph.addVertex(newvertex);
			graph.K.put(newvertex, 1/Constants.PadiracKSimpleDiv);
			data.update();
			data.getParent().repaint();
		} else if (request == 1){
			if (graph.getEdges().isEmpty()){
				JOptionPane.showMessageDialog(null, "No template yet, create the inhibited template first");
				return;
			} else {
				if(!graph.getInhibitableTemplates().isEmpty()){
					E inhibtemp = (E) JOptionPane.showInputDialog(null, "Inhibited template?", "New inhibiting species", JOptionPane.PLAIN_MESSAGE, null, graph.getInhibitableTemplates().toArray(), (Object) graph.getEdges().iterator().next());
					if (inhibtemp != null){
						newvertex = graph.getVertexFactory().create();
						((SequenceVertex)newvertex).setInhib(true);
						graph.addVertex(newvertex);
						graph.K.put(newvertex, 1/Constants.PadiracKInhibDiv);
						graph.addInhibition(inhibtemp, newvertex);
						data.update();
						data.getParent().repaint();
		    		}
				} else {
					JOptionPane.showMessageDialog(null, "No available template");
				}
			}
		}
		if(newvertex != null){
			Parameter<V,E> tempSeqK = null;
			//seqK
			Enumeration<DefaultMutableTreeNode> it = (Enumeration<DefaultMutableTreeNode>) ((MutableTreeNode) graph.optimizable.getChild(graph.optimizable.getRoot(), 0)).children();
			while(it.hasMoreElements()){
				DefaultMutableTreeNode next = it.nextElement();
				if(((Parameter<V,E>) next.getUserObject()).target.equals(newvertex)){
					tempSeqK = (Parameter<V,E>) next.getUserObject();
					break;
				}
			}
			if(tempSeqK != null){
				tempSeqK.currentValue = graph.getK(newvertex);
				tempSeqK.minValue = graph.isInhibitor(newvertex)? Constants.inhibKmin : Constants.simpleKmin;
				tempSeqK.maxValue = graph.isInhibitor(newvertex)? Constants.inhibKmax : Constants.simpleKmax;
			}
			//seqC
			it = (Enumeration<DefaultMutableTreeNode>) ((MutableTreeNode) graph.optimizable.getChild(graph.optimizable.getRoot(), 1)).children();
			while(it.hasMoreElements()){
				DefaultMutableTreeNode next = it.nextElement();
				if(((Parameter<V,E>) next.getUserObject()).target.equals(newvertex)){
					tempSeqK = (Parameter<V,E>) next.getUserObject();
					break;
				}
			}
			if(tempSeqK != null){
				tempSeqK.currentValue = ((SequenceVertex) newvertex).initialConcentration;
				tempSeqK.minValue = 0;
				tempSeqK.maxValue = 100;
			}
		}
	}

	private void removevertex(){
		Set<V> set = new HashSet<V>(graph.getSelected());
		for (V i: set){
			if(graph.getVertices().contains(i))
				graph.removeVertex(i);
			 
		 }
		graph.getSelected().clear();
		 data.update();
		 data.getParent().repaint();
	}
	
	private void duplicate(){
		ArrayList<V> notrealselected = getNotRealSelected();
		HashMap<V,V> translation = getNewNamesForVertices(notrealselected);
		
		for(V i: graph.getSelected()){
			
			addEdgesForVertex(i,translation);
			updateInhibitions(translation);
		
		}
		
		if(graph.getSelected().size()!=0){
			positionNewVertex(translation);
			data.update();
			data.getParent().repaint();
		}
	}
	
	private void positionNewVertex(HashMap<V, V> translation) {
		Point2D vector = new Point2D.Double();
		vector.setLocation(Math.random()*20, Math.random()*20);
		for(V i : graph.getSelected()){
			V target = translation.get(i);
			Point2D temp = new Point2D.Double();
			temp.setLocation(layout.apply(i).getX()+vector.getX(), layout.apply(i).getY()+vector.getY());
			this.layout.setLocation(target, temp);
		}
	}

	private ArrayList<V> getNotRealSelected(){
		ArrayList<V> notrealselected = new ArrayList<V>();
		V from;
		for (E e: graph.getInhibitions()){
			from = graph.getInhibition(e).getLeft();
			if (graph.getSelected().contains(from) && !(graph.getSelected().containsAll(graph.getEndpoints(e)))){
				notrealselected.add(from);
			}
		}
		return notrealselected;
	}
	
	private HashMap<V,V> getNewNamesForVertices(ArrayList<V> notrealselected){
		HashMap<V,V> translation = new HashMap<V,V>();
		V newvertex;
		for (V i : graph.getSelected()){
			if (notrealselected.contains(i)){
				translation.put(i, i);
			} else {
				newvertex = graph.getVertexFactory().create();
			translation.put(i, newvertex)  ;
			graph.addVertex(newvertex);
			graph.K.put(newvertex, graph.getK(i));
			((SequenceVertex) newvertex).initialConcentration = ((SequenceVertex) i).initialConcentration;
			}
		}
		return translation;
	}
	
	private void addEdgesForVertex(V i, HashMap<V,V> translation){
		V from,to;
		for (E e: graph.getOutEdges(i)){
			from = graph.getEndpoints(e).getFirst();
			to = graph.getEndpoints(e).getSecond();
			if(graph.getSelected().contains(from)&&graph.getSelected().contains(to)){
				E newE = graph.getEdgeFactory().createEdge(translation.get(from), translation.get(to));
				graph.addActivation(newE, translation.get(from), translation.get(to));
				graph.setTemplateConcentration(newE, graph.getTemplateConcentration(e));
			}
		}
	}
	
	private void updateInhibitions(HashMap<V,V> translation){
		V from, to;
		ArrayList<E> inhibs = new ArrayList<E>(graph.getInhibitions());
		
		for (E e: inhibs){
			if(graph.getSelected().contains(graph.getInhibition(e).getLeft())){
			from = graph.getEndpoints(e).getFirst();
			to = graph.getEndpoints(e).getSecond();
			
			if(graph.getSelected().contains(from)&&graph.getSelected().contains(to)){
				graph.addInhibition(graph.getEdgeFactory().createEdge(translation.get(from), translation.get(to)), translation.get(graph.getInhibition(e).getLeft()));
			}
			}
		}
	}
	
	// WARNING: bug if fusion with inhibitors
	private void fusion(){
		if(graph.getSelected().size()>1){
		V newnode = graph.getVertexFactory().create();
		V v2;
		graph.addVertex(newnode);
		graph.K.put(newnode, 1/Constants.PadiracKSimpleDiv);
		
		//graph.getSelected().clear();
		//graph.getSelected().add(newnode);
		ArrayList<E> loop = checkForLoops();
		for (V v: graph.getSelected()){
			for(E e: graph.getInEdges(v)){
				v2 = graph.getEndpoints(e).getFirst();
				E newedge = graph.getEdgeFactory().createEdge(v2, newnode);
				if (!graph.getSelected().contains(v2) && !graph.containsEdge(newedge)){
					graph.addActivation(newedge, v2, newnode);
					V inh = graph.getInhibition(e)==null?null:graph.getInhibition(e).getLeft();
					if(inh != null){
						//we have to change the inhibition target.
						V newInh = graph.getVertexFactory().create();
						graph.addVertex(newInh);
						((SequenceVertex) newInh).setInhib(true);
						((SequenceVertex) newInh).setInitialConcentration(((SequenceVertex) inh).initialConcentration);
						((SequenceVertex) newInh).inputs = ((SequenceVertex) inh).inputs;
						graph.K.put(newInh, graph.getK(inh));
						graph.addInhibition(newedge, newInh);
						//And we have to go yet another level down
						for(E e2 : graph.getInEdges(inh)){
							V v3 = graph.getEndpoints(e2).getFirst();
							E newedge2 = graph.getEdgeFactory().createEdge(v3, newInh);
							if (!graph.getSelected().contains(v3) && !graph.containsEdge(newedge2)){
								graph.addActivation(newedge2, v3, newInh);
							} else if (!graph.containsEdge(newedge2)) {
								// we are inhibiting back
								graph.addActivation(newedge2, newnode, newInh);
							}
						}
					}
				}
			}
			for(E e: graph.getOutEdges(v)){
				v2 = graph.getEndpoints(e).getSecond();
				E newedge = graph.getEdgeFactory().createEdge(newnode, v2);
				if (!graph.getSelected().contains(v2) && ! graph.containsEdge(newedge)){
					graph.addActivation(newedge, newnode, v2);
					V inh = graph.getInhibition(e)==null?null:graph.getInhibition(e).getLeft();
					if(inh != null){
						//we have to change the inhibition target.
						V newInh = graph.getVertexFactory().create();
						graph.addVertex(newInh);
						((SequenceVertex) newInh).setInhib(true);
						((SequenceVertex) newInh).setInitialConcentration(((SequenceVertex) inh).initialConcentration);
						((SequenceVertex) newInh).inputs = ((SequenceVertex) inh).inputs;
						graph.K.put(newInh, graph.getK(inh));
						graph.addInhibition(newedge, newInh);
						//And we have to go yet another level down
						for(E e2 : graph.getInEdges(inh)){
							V v3 = graph.getEndpoints(e2).getFirst();
							E newedge2 = graph.getEdgeFactory().createEdge(v3, newInh);
							if (!graph.getSelected().contains(v3) && !graph.containsEdge(newedge2)){
								graph.addActivation(newedge2, v3, newInh);
							} else if (!graph.containsEdge(newedge2)) {
								// we are inhibiting back
								graph.addActivation(newedge2, newnode, newInh);
							}
						}
					}
				}
			}
		}
			
		if(!loop.isEmpty()){
			//we have a loop, meaning an autocatalysis.
			//we have to had it to the new node, then fusion the possible inhibitors
			E autocat = graph.getEdgeFactory().createEdge(newnode, newnode);
			graph.addActivation(autocat, newnode, newnode);
			ArrayList<V> inhibitors = new ArrayList<V>();
			V newInhib = graph.getVertexFactory().create();
			for(E possibleEdge : loop){
				if (graph.getInhibition(possibleEdge) != null){
					V oldv = graph.getInhibition(possibleEdge).getLeft();
					inhibitors.add(oldv);
				}
			}
			if(!inhibitors.isEmpty()){
				graph.addVertex(newInhib);
				graph.addInhibition(autocat, newInhib);
				graph.K.put(newInhib, 1/Constants.PadiracKInhibDiv);
				Set<V> done = new HashSet<V>();
				for(V inh : inhibitors){
					for(E act : graph.getInEdges(inh)){
						V origin = graph.getEndpoints(act).getFirst();
						if(!done.contains(origin)){
							done.add(origin);
							E newInhEdge = graph.getEdgeFactory().createEdge(origin, newInhib);
							if(!graph.getEdges().contains(newInhEdge)){
								graph.addActivation(newInhEdge, origin, newInhib);
							}
						}
					}
				}
			}
		}
			removevertex();
			graph.getSelected().add(newnode);
			data.update();
			data.getParent().repaint();
		}
	}
	
	private ArrayList<E> checkForLoops() {
		ArrayList<E> loopEdges = new ArrayList<E>();
		Set<V> looping = eliminateOutsiders(graph.getSelected()); //any remaining should be in a loop		
		for(V v : looping){
			ArrayList<E> outs = new ArrayList<E>(graph.getOutEdges(v));
			for(E edge: outs){
				if(looping.contains(graph.getEndpoints(edge).getSecond())){
					loopEdges.add(edge);
				}
			}
		}
		return loopEdges;
	}
	
	//This is not pretty
	private Set<V> eliminateOutsiders(Set<V> initial) {
		Set<V> returns = new HashSet<V>(initial);
		Set<V> toBechecked = Collections.synchronizedSet(new HashSet<V>(initial));
		while(!toBechecked.isEmpty()){
		V vstart = toBechecked.iterator().next(); // pick one at random
		toBechecked.remove(vstart);
		ArrayList<E> ins = new ArrayList<E>(graph.getInEdges(vstart));
		ArrayList<E> outs = new ArrayList<E>(graph.getOutEdges(vstart));
		Iterator<E> ite = ins.iterator();
		while(ite.hasNext()){
			E ein = ite.next();
			if(!returns.contains(graph.getEndpoints(ein).getFirst())){
				ite.remove();
			}
		}
		ite = outs.iterator();
		while(ite.hasNext()){
			E eout = ite.next();
			if(!returns.contains(graph.getEndpoints(eout).getSecond())){
				ite.remove();
			}
		}
		if(ins.isEmpty() || outs.isEmpty()){
			//outliner
			returns.remove(vstart);
			//Now I need to check if anybody connected to it became an ourliner.
			if(!ins.isEmpty()){
				for(E ein: ins){
					if(!toBechecked.contains(graph.getEndpoints(ein).getFirst())){
						toBechecked.add(graph.getEndpoints(ein).getFirst());
					}
				}
			} else if (!outs.isEmpty()){
				for(E ein: outs){
					if(!toBechecked.contains(graph.getEndpoints(ein).getSecond())){
						toBechecked.add(graph.getEndpoints(ein).getSecond());
					}
				}
			}
		}
		}
		return returns;
	}

	@SuppressWarnings("unchecked")
	private void export(){
		File selectedFile = null;
		if(fc != null){
			JFileChooser exp = new JFileChooser();
			int result = exp.showSaveDialog(null);
			if (result == JFileChooser.APPROVE_OPTION){
				selectedFile = exp.getSelectedFile();
			}
		} else {
			fd.setMode(FileDialog.SAVE);
			fd.setVisible(true);
			if(fd.getFile() != null){
				String filterPath = fd.getDirectory();
				selectedFile = new File(filterPath, fd.getFile());
			}
		}
		if(selectedFile != null){
			try {
				if (!selectedFile.exists()){

					selectedFile.createNewFile();
				
				}
				
				FileWriter fw = new FileWriter(selectedFile);
				fw.write(CodeGenerator.exportToSBMLcode((OligoGraph<SequenceVertex,E>) graph));
				fw.flush();
				fw.close();
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void performSave(){
		try {
			//if(!savingFile.getName().endsWith(".graph")){
			//	File tmpFile = new File(savingFile.getAbsolutePath()+".graph");
			//	boolean rename = savingFile.renameTo(tmpFile);
				//System.out.println("renamed "+rename);
			//}
			
			if (!savingFile.exists()){
				
				savingFile.createNewFile();
			
			}

			graph.saved = true;
			FileWriter fw = new FileWriter(savingFile);
			fw.write(CodeGenerator.saveGraph((OligoGraph<SequenceVertex,E>)graph, (Layout<SequenceVertex, E>)layout));
			fw.flush();
			fw.close();

		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void open(){
		if (fc != null){
			int result = fc.showOpenDialog(null);
			if (result == JFileChooser.APPROVE_OPTION){
				File open = fc.getSelectedFile();
				CodeGenerator.openGraph((OligoGraph<SequenceVertex,E>)graph, (Layout<SequenceVertex,E>) layout,open);
				this.savingFile = open;
			}
		} else {
			fd.setMode(FileDialog.LOAD);
			fd.setVisible(true);
			if(fd.getFile() != null){
				String filterPath = fd.getDirectory();
				File open = new File(filterPath, fd.getFile());
				CodeGenerator.openGraph((OligoGraph<SequenceVertex,E>)graph, (Layout<SequenceVertex,E>) layout,open);
				this.savingFile = open;
			}
		}
		data.update();
		data.getParent().repaint();
	}
	
@SuppressWarnings("unchecked")
private void doimport(){
		if(fc != null){
			int result = fc.showOpenDialog(null);
			if (result == JFileChooser.APPROVE_OPTION){
				File open = fc.getSelectedFile();
				CodeGenerator.importGraph((OligoGraph<SequenceVertex,E>)graph,(Layout<SequenceVertex,E>) layout, open);
			}
		} else {
			fd.setMode(FileDialog.LOAD);
			fd.setVisible(true);
			if(fd.getFile() != null){
				String filterPath = fd.getDirectory();
				File open = new File(filterPath, fd.getFile());
				CodeGenerator.openGraph((OligoGraph<SequenceVertex,E>)graph, (Layout<SequenceVertex,E>) layout,open);
			}
		}
		data.update();
		data.getParent().repaint();
	}

private void doinput(){
	if(graph.getVertexCount()>0){
		SequenceVertex seq = (SequenceVertex) JOptionPane.showInputDialog(null, "Sequence?", "Add input to sequence", JOptionPane.PLAIN_MESSAGE, null, graph.getVertices().toArray(), (SequenceVertex) graph.getVertices().iterator().next());
		if(seq != null){
			
			File open = null;
			if (fc != null){
				JFileChooser ffc = new JFileChooser();
				int result = ffc.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION){
					open = ffc.getSelectedFile();
				}
			} else {
				FileDialog ffd = new FileDialog((JFrame) null);
			
				ffd.setMode(FileDialog.LOAD);
				ffd.setVisible(true);
				if(ffd.getFile() != null){
					String filterPath = ffd.getDirectory();
					
					open = new File(filterPath, ffd.getFile());
				}
			}
			if(open != null){
				try {
					FileReader in = new FileReader(open);
					BufferedReader reader = new BufferedReader(in);
					String line = null;
					String[] split;
					ArrayList<Double> values = new ArrayList<Double>();
					while ((line = reader.readLine()) != null){
						split= line.split("\t");
						for(int i=0; i<split.length;i++){
							values.add(Double.parseDouble(split[i]));
						}
					}
					Double[] inp = new Double[values.size()];
					inp = values.toArray(inp);
					seq.inputs.add(new ExternalInput(inp,open));
					graph.replot();
					data.getParent().repaint();
					reader.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}


@SuppressWarnings("unchecked")
protected void plot() {
	boolean newestVersion = true;
	if(newestVersion){
	model = new OligoSystemAllSats<E>((OligoGraph<SequenceVertex, E>) this.graph);
	} else {
	//DEBUG
	model = new OligoSystem<E>((OligoGraph<SequenceVertex, E>) this.graph);
	}
	
	//END DEBUG
	final MyWorker myWorker = new MyWorker(model,graph);
	
	ProgressMonitor progressMonitor = new ProgressMonitor(null,
            "Simulating",
            "", 0, 100);
	progressMonitor.setProgress(0);

	final ProgressMonitor cop = progressMonitor;
	
	myWorker.setProg(cop);
	myWorker.setCustomProgress(0);
	myWorker.addPropertyChangeListener(new PropertyChangeListener(){

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			//System.out.println(evt.getPropertyName());
			if ("progress".equals(evt.getPropertyName()) ) {
				
	            int progress = (Integer) evt.getNewValue();
	            cop.setProgress(progress);
			}
			
		}
		
	});
	
	myWorker.execute();
}
}
