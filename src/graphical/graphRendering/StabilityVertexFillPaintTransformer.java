package graphical.graphRendering;

import java.awt.Color;
import java.awt.Paint;

import model.OligoGraph;

import com.google.common.base.Function;

public class StabilityVertexFillPaintTransformer<V> implements Function<V, Paint> {

	private OligoGraph<V,?> graph;
	
	public StabilityVertexFillPaintTransformer(OligoGraph<V,?> graph){
		this.graph = graph;
	}
	
	@Override
	public Paint apply(V arg0) {
		float vect = (float) Math.min(0.33, (float) (Math.log(graph.getK(arg0)+5))/(float)17.0);
		return (Paint) Color.getHSBColor((float) (0.66+vect), 1, 1);
	}

}
