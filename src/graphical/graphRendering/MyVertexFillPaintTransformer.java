package graphical.graphRendering;
import java.awt.Color;
import java.awt.Paint;
import java.util.Set;

import com.google.common.base.Function;

import model.OligoGraph;


public class MyVertexFillPaintTransformer<V> implements Function<V, Paint> {
	OligoGraph<V,?> graph;
	Set<V> selected;
	
	public MyVertexFillPaintTransformer(OligoGraph<V, ?> g){
		this.graph = g;
		this.selected = g.getSelected();
	}
	
	@Override
	public Paint apply(V arg0) {
		if (graph.getSelected()!=null && graph.getSelected().contains(arg0)){
			return (Paint) Color.BLUE;
		}
		return (Paint) Color.RED;
	}

}
