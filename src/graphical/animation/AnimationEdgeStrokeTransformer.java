package graphical.animation;

import java.awt.BasicStroke;
import java.awt.Stroke;

import org.apache.commons.collections4.Transformer;

import com.google.common.base.Function;

import model.OligoGraph;

public class AnimationEdgeStrokeTransformer<E> implements Function<E, Stroke> {

	private OligoGraph<?,E> graph;
	
	public AnimationEdgeStrokeTransformer(OligoGraph<?,E> graph){
		this.graph = graph;
	}
	
	@Override
	public Stroke apply(E arg0) {
		
		return new BasicStroke((float) Math.min(10.0, graph.getTemplateConcentration(arg0)/10.0));
	}

	
	
}
