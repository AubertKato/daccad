package graphical.animation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JPanel;

public class SaturationPanel extends JPanel {

	protected int[][] saturations;
	private int columnwidth = 10;
	private int columnheight = 50;
	private int baseX = 30;
	private int baseY = 40;
	private int textX = -10;
	private int textY = 5;
	private int displ = 30;
	public int time = 0;
	public static int sizeY = 100;
	
	/**
	 * Display the saturations in an AnimatedSequences panel
	 * saturation[0][n] represents the saturation of exonuclease at time n
	 * saturation[1][n] // polymerase //
	 * saturation[2][n] // nicking enzyme //
	 * @param saturations between 0 (no saturation) and 1 (full saturation)
	 */
	public SaturationPanel(double[][] saturations){
		this.saturations = new int[3][saturations[0].length];
		for(int i=0;i<3;i++){
			for(int j=0;j<this.saturations[0].length;j++){
				this.saturations[i][j] = (int) (columnheight*(1-saturations[i][j]));
			}
		}
		//this.add(new JButton("butterscotch"));
		this.setPreferredSize(new Dimension(110,sizeY));
	}

	public void paint(Graphics g){
		super.paint(g);
		g.drawString("Activity", 0, 10);
		g.drawString("1", baseX+textX, baseY+textY);
		g.drawString("0", baseX+textX, baseY+columnheight+textY);
		g.drawString("exo",baseX-5,baseY-5);
		g.drawRect(baseX, baseY, columnwidth, columnheight);
		Color c = g.getColor();
		g.setColor(java.awt.Color.green);
		g.fillRect(baseX+1, baseY+saturations[0][time], columnwidth-1, columnheight-saturations[0][time]);
		g.setColor(c);
		g.drawString("poly",baseX+displ-5,baseY-5);
		g.drawRect(baseX+displ, baseY, columnwidth, columnheight);
		g.setColor(java.awt.Color.blue);
		g.fillRect(baseX+1+displ, baseY+saturations[1][time], columnwidth-1, columnheight-saturations[1][time]);
		g.setColor(c);
		g.drawString("nick",baseX+2*displ-5,baseY-5);
		g.drawRect(baseX+2*displ, baseY, columnwidth, columnheight);
		g.setColor(java.awt.Color.red);
		g.fillRect(baseX+1+2*displ, baseY+saturations[2][time], columnwidth-1, columnheight-saturations[2][time]);
		g.setColor(c);
	}
	
	public void setTime(int t){
		this.time = t;
	}
	
}
