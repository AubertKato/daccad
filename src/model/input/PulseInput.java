package model.input;

import model.chemicals.SequenceVertex;

public class PulseInput extends AbstractInput{

	public double pulseTime;
	public double ampli;
	public double period;
	public boolean periodic;
	
	public PulseInput(double pulseTime, double ampli){
		this.pulseTime = pulseTime;
		this.ampli = ampli;
		this.period = 100;
		this.periodic = false;
	}
	
	@Override
	public boolean hasSingularity() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public double getNextSingularityTime() {
		// TODO Auto-generated method stub
		return pulseTime;
	}

	@Override
	public void consummeSingularity() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double f(double t) {
		double value = t-this.pulseTime;
		if(value >0 && periodic){
			value = value - period*((int) (value/period)); 
			if(value > period/2){
				value = period - value;
			}
			//System.out.println(value);
		}
		return this.ampli*Math.exp(-(value)*(value));
	}

	@Override
	public String toMathematica(SequenceVertex seq) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTime(double valueOf) {
		// TODO Auto-generated method stub
		this.pulseTime = valueOf;
	}

	public boolean isPeriodic() {
		// TODO Auto-generated method stub
		return periodic;
	}

	public double getPeriod() {
		// TODO Auto-generated method stub
		return period;
	}

	public void setPeriodic(boolean selected) {
		this.periodic = selected;
		
	}

	public void setPeriod(double valueOf) {
		this.period = valueOf;
	}

	public String toString(){
		return "Pulse "+(periodic?"starting at time "+this.pulseTime+", period "+period:" at time "+pulseTime);
	}
	
}
