package model.input;

import model.chemicals.SequenceVertex;

public abstract class AbstractInput {

	public int targetSequence;
	
	/**
	 * Function that should be called by the eventHandler to know if there is a singularity coming
	 * @return
	 */
	public abstract boolean hasSingularity();
	
	/** 
	 * 
	 * @return the next singularity time, if it exists, or -1 else
	 */
	public abstract double getNextSingularityTime(); 
	
	/**
	 * The event handler should call this to remove a singularity point that has already been used
	 */
	public abstract void consummeSingularity();
	
	/**
	 * Returns the value of the input function at time t
	 * @param t
	 * @return
	 */
	public abstract double f(double t);
	
	public abstract String toMathematica(SequenceVertex seq);

	public abstract void reset();

	public abstract void setTime(double valueOf);
}
