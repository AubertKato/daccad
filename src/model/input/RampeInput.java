package model.input;

import model.Constants;
import model.chemicals.SequenceVertex;

public class RampeInput extends AbstractInput {

	private double privatetime=1;
	public double rampe = 100;
	private double startingtime=1;

	@Override
	public boolean hasSingularity() {
		// TODO Auto-generated method stub
		return privatetime<Constants.numberOfPoints;
	}

	@Override
	public double getNextSingularityTime() {
		// TODO Auto-generated method stub
		return privatetime;
	}

	@Override
	public void consummeSingularity() {
		// TODO Auto-generated method stub
		privatetime++;
	}

	@Override
	public double f(double t) {
		// TODO Auto-generated method stub
		return t/this.rampe ;
	}

	@Override
	public String toMathematica(SequenceVertex seq) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void reset() {
		privatetime = startingtime;
		
	}
	
	@Override
	public void setTime(double valueOf) {
		this.privatetime = valueOf;
		this.startingtime = valueOf;
		
	}

}
