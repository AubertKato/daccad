package model;

import java.util.Collection;
import java.util.HashMap;

import utils.Undoable;

public class PseudoTemplateGraph<V, E> extends OligoGraph<V, E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected HashMap<V,V> extendedSpecies = new HashMap<V,V>();
	protected HashMap<E,V> pseudoTemplates = new HashMap<E,V>(); //point to the species it modifies
	protected HashMap<E,Double> inputSlowdowns = new HashMap<E,Double>();
	
	public V getExtendedSpecies(V v){
		return extendedSpecies.get(v);
	}
	
	public boolean isPseudoTemplate(E e){
		return pseudoTemplates.containsKey(e);
	}
	
	public E getPseudoTemplate(V v){
		if(extendedSpecies.containsKey(v)){
			for(E e : pseudoTemplates.keySet()){
				if (pseudoTemplates.get(e).equals(v)){
					return e;
				}
			}
		}
		return null;
	}
	
	public V getPseudoTemplateInput(E e){
		return pseudoTemplates.get(e);
	}
	
	public void setExtendedSpecies(final V v, V ext){
		extendedSpecies.put(v, ext);
		//This is kind of strange, as this edge doesn't really exists...
		final E fakeEdge = this.edgeFactory.createEdge(v, ext);
		vertices.get(v).getSecond().add(fakeEdge);
		dangleLSlowdown.put(fakeEdge, 1.0);
		inputSlowdowns.put(fakeEdge, 1.0);
		templateConcentrations.put(fakeEdge,10.0);
		pseudoTemplates.put(fakeEdge, v);
		edits.add(new Undoable(){
			
			public void undo(){
				vertices.get(v).getSecond().remove(fakeEdge);
				inputSlowdowns.remove(fakeEdge);
				templateConcentrations.remove(fakeEdge);
				extendedSpecies.remove(v);
				pseudoTemplates.remove(fakeEdge);
				dangleLSlowdown.remove(fakeEdge);
			}
		});
	}		
	
	
	public void removeExtendedSpecies(V v){
		final V origin = v;
		final V ext = extendedSpecies.get(v);
		final E fakeEdge = this.edgeFactory.createEdge(v, ext);
		final double conc = templateConcentrations.get(fakeEdge);
		final double slowdown = inputSlowdowns.get(fakeEdge);
		
		vertices.get(v).getSecond().remove(fakeEdge);
		inputSlowdowns.remove(fakeEdge);
		templateConcentrations.remove(fakeEdge);
		extendedSpecies.remove(v);
		pseudoTemplates.remove(fakeEdge);
		dangleLSlowdown.remove(fakeEdge);
		
		if(ext!=null){
			edits.add(new Undoable(){
				
				public void undo(){
					extendedSpecies.put(origin,ext);
					vertices.get(origin).getSecond().add(fakeEdge);
					templateConcentrations.put(fakeEdge,conc);
					pseudoTemplates.put(fakeEdge, v);
					inputSlowdowns.put(fakeEdge,slowdown);
					dangleLSlowdown.put(fakeEdge, 1.0);
				}
			});
		}
	}

	public Collection<V> getAllExtendedSpecies(){
		return extendedSpecies.values();
	}
	
	public Collection<V> getAllSpeciesWithPseudoTemplate(){
		return extendedSpecies.keySet();
	}
	
	@Override
	public boolean removeVertex(V v){
		V ext = extendedSpecies.get(v);
		if(ext != null){
			vertices.get(v).getSecond().remove(edgeFactory.createEdge(v, ext));
			extendedSpecies.remove(v);
		}
		return super.removeVertex(v);
	}
	
	@Override
	public boolean addActivation(E e, V v1, V v2, double conc) {
		inputSlowdowns.put(e,1.0);
		return super.addActivation(e, v1, v2, conc);
	}
	
	@Override
	public boolean addActivation(E e, V v1, V v2) {
		inputSlowdowns.put(e,1.0);
		return super.addActivation(e, v1, v2);
	}
	
	@Override
	public boolean removeEdge(E edge) {
		inputSlowdowns.remove(edge);
		return super.removeEdge(edge);
	}
	
	public double getInputSlowdown(E edge){
		return inputSlowdowns.get(edge);
	}
	
	public void setInputSlowdown(E edge, double value){
		inputSlowdowns.put(edge, value);
	}
	
	public boolean hasPseudoTemplate(V v){
		return (extendedSpecies.get(v) != null);
	}
	
}
