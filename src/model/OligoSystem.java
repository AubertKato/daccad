package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.io.Serializable;

import utils.DefaultTemplateFactory;
import utils.MyStepHandler;
import utils.PluggableWorker;
import utils.SequenceVertexComparator;
import utils.TemplateFactory;
import model.chemicals.InvalidConcentrationException;
import model.chemicals.SequenceVertex;
import model.chemicals.Template;
import model.input.AbstractInput;

import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.nonstiff.GraggBulirschStoerIntegrator;


/**
 * 
 * @author naubertkato
 *
 * Basic oligo system with hardcoded enzymatic saturation.
 *
 * @param <E> representation for templates in Graph, usually String
 */
public class OligoSystem<E> implements Serializable,
		Cloneable, FirstOrderDifferentialEquations {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3626332588448633729L;

	//private double observedExoKm = 0;

	public int total = 0;
	public int inhTotal = 0;
	
	public Map<E,Template<E>> templates;
	
	protected OligoGraph<SequenceVertex,E> graph;
	
	protected TemplateFactory<E> templateFactory;

	protected ArrayList<SequenceVertex> sequences;
	
	protected int profiling = 0;
	protected double[][] savedActivity = new double[3][Constants.numberOfPoints+1];
	protected int time = 0;

	/**
	 * Created every time we try to integrate the system over time
	 * @param graph
	 */
	public OligoSystem(OligoGraph<SequenceVertex,E> graph) {
		this.graph = graph;
		this.templateFactory = new DefaultTemplateFactory<E>(graph);
		this.inhTotal = graph.inhibitors.size();
		this.total = graph.getVertexCount() - this.inhTotal;

		this.templates = new LinkedHashMap<E,Template<E>>();
		for (E e :graph.getEdges()){
			this.templates.put(e, templateFactory.create(e));
		}
		this.sequences = new ArrayList<SequenceVertex>(graph.getVertices());
		Collections.sort(this.sequences, new SequenceVertexComparator());
	}
	
	/**
	 * Application extending the meaning of templates can use this constructor
	 * @param graph
	 * @param tf
	 */
	public OligoSystem(OligoGraph<SequenceVertex,E> graph, TemplateFactory<E> tf) {
		this.graph = graph;
		this.templateFactory = tf;
		this.inhTotal = graph.inhibitors.size();
		this.total = graph.getVertexCount() - this.inhTotal;

		this.templates = new LinkedHashMap<E,Template<E>>();
		for (E e :graph.getEdges()){
			this.templates.put(e, templateFactory.create(e));
		}
		this.sequences = new ArrayList<SequenceVertex>(graph.getVertices());
		Collections.sort(this.sequences, new SequenceVertexComparator());
	}

	public void setTotals(int total, int inhTotal) {
		this.total = total;
		this.inhTotal = inhTotal;
	}
	
	public double getTotalCurrentFlux(SequenceVertex s) {
		
		if (!graph.isInhibitor(s)) {
			return getTotalCurrentFluxSimple(s);
		} else {
			return getTotalCurrentFluxInhibitor(s);
		}
	}
	
	protected double getTotalCurrentFluxSimple(SequenceVertex s) {
		// As Input
		double flux = 0;
		Template<E> temp;

		for(E e: graph.getOutEdges(s)){
			temp = this.templates.get(e);
			flux += temp.inputSequenceFlux();
			}
		// As Output
		for (E e: graph.getInEdges(s)) {
			temp = this.templates.get(e);
			flux += temp.outputSequenceFlux();
		}
		if (!graph.saturableExo) {

			flux -= s.getConcentration() * graph.exoConc*(Constants.exoVm)/Constants.exoKmSimple;
		} else {
			double km = Constants.exoKmSimple;
			//flux -= s.getConcentration() * Constants.exoConc*Constants.exoVm
			//		/ (Constants.exoKmSimple * this.observedExoKm);
			if(graph.coupling){
			
			flux -= s.getConcentration() * graph.exoConc*Constants.exoVm/ this.computeExoKm(km);
			} else {
			flux -= s.getConcentration() * graph.exoConc*Constants.exoVm/(km + s.getConcentration());
			}
			
		}
		return flux;
	}

	protected double getTotalCurrentFluxInhibitor(SequenceVertex s) {
		double flux = 0;
		Template<E> temp;
		// As Inhib
		temp = this.templates.get(graph.inhibitors.get(s));
		flux += temp.inhibSequenceFlux();
		
		// As Output
		for (E e: graph.getInEdges(s)) {
			temp = this.templates.get(e);
			flux += temp.outputSequenceFlux();
		}
		if (!graph.saturableExo) {

			flux -= s.getConcentration() * graph.exoConc*Constants.exoVm/Constants.exoKmInhib;

		} else {
			double km = Constants.exoKmInhib;
			//flux -= s.getConcentration() * Constants.exoConc*Constants.exoVm
			/// (Constants.exoKmInhib * this.observedExoKm);
			if(graph.coupling){
			flux -= s.getConcentration() * graph.exoConc*Constants.exoVm/ this.computeExoKm(km);//(Constants.exoKmInhib);
			} else {
				flux -= s.getConcentration() * graph.exoConc*Constants.exoVm/(km + s.getConcentration());
			}

		}

		return flux;
	}

	protected double computeExoKm(double myKm){
		double value = myKm;
		
		for (SequenceVertex s: this.sequences) {
			double tempKm = (graph.inhibitors.containsKey(s)?Constants.exoKmInhib:Constants.exoKmSimple);
			
			value += s.getConcentration()*(tempKm==myKm?1.0:myKm/tempKm);
		}
		if(graph.isExoSatByTemp()){
			for(Template<E> t: this.templates.values()){
				value += (myKm*t.concentrationAlone)/t.exoKm;
			}
		}
		return value;
	}
	
//	private void setObservedExoKm() {
//		double value = 0;
//		for (SequenceVertex s: graph.getVertices()) {
//			value += s.getConcentration()
//					/ (graph.inhibitors.containsKey(s)?Constants.exoKmInhib:Constants.exoKmSimple);
//		}
//		if(graph.exoSaturationByFreeTemplates){
//			for(Template<E> t: this.templates.values()){
//				value += t.concentrationAlone/Constants.exoKmTemplate;
//			}
//		}
//		this.observedExoKm = 1 + value;
//	}
	
	protected void setObservedPolyKm() {
		double value = 1;
		if(graph.coupling){
		for (Template<E> temp: this.templates.values()){
			value += temp.getPolyUsage();
		}
		for (Template<E> temp: this.templates.values()){
			//System.out.println("New pol activity: "+Constants.polVm/(Constants.polKm*value));
			//System.out.println("New polboth activity: "+Constants.polVm/(Constants.polKmBoth*value));
			temp.setAllocatedPoly(value);
		}
		} else {
			for (Template<E> temp: this.templates.values()) {
				value =1+ temp.getPolyUsage();
				//perceivedPoly = new Enzyme("poly", this.poly.activity/value, this.poly.basicKm, this.poly.optionalValue);
				temp.setAllocatedPoly(value);
		}
	}
	}
	
	protected void setObservedNickKm() {
		double value = 1;
		if(graph.coupling){
		for (Template<E> temp: this.templates.values()){
			value += temp.getNickUsage();
		}
		for (Template<E> temp: this.templates.values()){
			temp.setAllocatedNick(value);
		}
		} else {
			for (Template<E> temp: this.templates.values()) {
				value =1+ temp.getNickUsage();
				//perceivedPoly = new Enzyme("poly", this.poly.activity/value, this.poly.basicKm, this.poly.optionalValue);
				temp.setAllocatedNick(value);
		}
	}
	}

	protected double[] getCurrentConcentration() {
		double concentration[] = new double[total + inhTotal];
		Iterator<SequenceVertex> it = this.sequences.iterator();
		int i=0;
		while (it.hasNext()) {
					concentration[i] = it.next().getConcentration();
					i++;
				}
		return (concentration);
	}



	public void reinitializeOiligoSystem() {
		for(SequenceVertex v: graph.getVertices()){
			v.reset();
		}
		for(Template<E> temp: templates.values()){
			temp.reset();
		}
		this.savedActivity = new double[3][Constants.numberOfPoints+1];
	}

	@Override
	public void computeDerivatives(double t, double[] y, double[] ydot) {
		// First all the activation sequences, then inibiting, then templates.
		// ydot is a placeholder, should be updated with the derivative of y at
		// time t.
		
		profiling++;
		
		int where = 0;
		boolean saveActivity = (t >= this.time +1);
		Iterator<SequenceVertex> it = this.sequences.iterator();
		while(it.hasNext()){
			
						it.next().setConcentration(y[where]);
			
						where++;
			}

		double[] internal;
		Iterator<Template<E>> it2 = this.templates.values().iterator();
		while(it2.hasNext()) {
			Template<E> templ = it2.next();
			int length = templ.getStates().length;
			internal = new double[length];
			for(int i = 0; i< length; i++){
			internal[i] = y[where]; // there must be a better way. There is with arraycopy...
			where++;
			}
			try {
				templ.setStates(internal);
			} catch (InvalidConcentrationException e) {
				
				e.printStackTrace();
				System.exit(-1);
			}
		}
		
//		if (graph.saturableExo){
//			this.setObservedExoKm();
//		}
		if (graph.saturablePoly){
			this.setObservedPolyKm();
		}
		if (graph.saturableNick){
			this.setObservedNickKm();
		}
		
		if (saveActivity){
			//System.out.println("Saving stuff "+this.time);
			this.time++;
			//System.out.println("New pol activity: "+time+" "+this.templates.values().iterator().next().poly+" values:"+y[0]+" "+y[1]);
			this.savedActivity[0][time] = graph.saturableExo?Constants.exoKmSimple/ this.computeExoKm(Constants.exoKmSimple):1;
			//ret[where] = graph.saturablePoly?this.templates.values().iterator().next().poly:1;
			this.savedActivity[1][time] = graph.saturablePoly?this.templates.values().iterator().next().getCurrentPoly()/(Constants.polVm/Constants.polKm):1;
			//ret[where] = graph.saturablePoly?this.templates.values().iterator().next().nick:1;
			this.savedActivity[2][time] = graph.saturablePoly?this.templates.values().iterator().next().getCurrentNick()/(Constants.nickVm/Constants.nickKm):1;
		}
		
		where = 0;
		it = this.sequences.iterator();
		SequenceVertex seq;
		while(it.hasNext()){
					seq = it.next();
					ydot[where] = this.getTotalCurrentFlux(seq);
					for(AbstractInput inp : seq.inputs){
						ydot[where]+=inp.f(t);
					}
					where++;
				}

		it2 = this.templates.values().iterator();
		while(it2.hasNext()) {
			internal = it2.next().flux();
			for(int i=0; i<internal.length;i++){
			ydot[where] = internal[i];
			where++;
			}
			
		}
		//if(t>=10)
		//System.out.println("Alternative test: time "+t+" [y] "+y[0]+" d "+ydot[0]);
		where++;
	}

	/**
	 * dimension: signal sequences + inhibiting sequences + 6 states per template + 3 enzymes
	 * enzymes are not calculated by the integrator: always get a derivative of 0.
	 */
	@Override
	public int getDimension() {
		
		//Templates may have different sizes
		int totaltemplatesize = 0;
		for(Template<E> templ : this.templates.values()){
			totaltemplatesize += templ.getStates().length;
		}
		
		return this.total + this.inhTotal + totaltemplatesize;
	}

	public double[] initialConditions() {
//		if (graph.saturableExo){
//			this.setObservedExoKm();
//		}
		if (graph.saturablePoly){
			this.setObservedPolyKm();
		}
		if (graph.saturableNick){
			this.setObservedNickKm();
		}
		double[] ret = new double[this.getDimension()];
		int where = 0;
		for (int i = 0; i < this.total + this.inhTotal; i++) {
					ret[where] = this.getCurrentConcentration()[i];
					
					where++;
				}
		
		double[] internal;
		Iterator<Template<E>> it2 = this.templates.values().iterator();
		while(it2.hasNext()){
			internal = it2.next().getStates();
			for(int i=0; i<internal.length;i++){
				ret[where] = internal[i];
				where++;
				}
		}
		
		//ret[where] = graph.saturableExo?Constants.exoVm/ this.computeExoKm(Constants.exoKmSimple):1;
		this.savedActivity[0][0] = graph.saturableExo?Constants.exoKmSimple/ this.computeExoKm(Constants.exoKmSimple):1;
		//ret[where] = graph.saturablePoly?this.templates.values().iterator().next().poly:1;
		this.savedActivity[1][0] = (graph.saturablePoly && !this.templates.isEmpty())?this.templates.values().iterator().next().getCurrentPoly()/(Constants.polVm/Constants.polKm):1;
		//ret[where] = graph.saturablePoly?this.templates.values().iterator().next().nick:1;
		this.savedActivity[2][0] = (graph.saturableNick && !this.templates.isEmpty())?this.templates.values().iterator().next().getCurrentNick()/(Constants.nickVm/Constants.nickKm):1;
		return ret;
	}

	public double[][] calculateTimeSeries(PluggableWorker myWorker) {
		final GraggBulirschStoerIntegrator myIntegrator = new GraggBulirschStoerIntegrator(
				1e-14, 1, Constants.absprec, Constants.relprec);
		// DormandPrince853Integrator myIntegrator = new
		// DormandPrince853Integrator(1e-18, 10000, Constants.absprec, Constants.relprec) ;
		
		//int index = 0;
		//ArrayList<SequenceVertex> allseqs = new ArrayList<SequenceVertex>(sequences);
		//allseqs.get(0).inputs.add(new RampeInput());
		
		this.reinitializeOiligoSystem();
		//System.out.println("reinitialized");
		final double[] placeholder = this.initialConditions();
		final OligoSystem<E> syst = this;
		final MyStepHandler handler = new MyStepHandler();
		
		handler.registerWorker(myWorker);
		
		
		
//		HashMap<SequenceVertex,Integer> inp = new HashMap<SequenceVertex,Integer>();
//		for (SequenceVertex seq: allseqs){
//			if(!seq.inputs.isEmpty()){
//				inp.put(seq, allseqs.indexOf(seq));
//			}
//		}
//		MyEventHandler ehandler = new MyEventHandler(inp);
		myIntegrator.addStepHandler(handler);
		//myIntegrator.addEventHandler(ehandler, 100, 1e-10, 100);
		//System.out.println("Ready");
		try{
		myIntegrator.integrate(syst, 0, placeholder, Constants.numberOfPoints,
				placeholder);
		} catch ( org.apache.commons.math3.exception.NumberIsTooSmallException e){
			System.out.println("Integration error: min possible value is "+e.getMin());
		}
		syst.displayProfiling();
		
		//progressMonitor.close();
		return handler.getTimeSerie();
		
	}
	
	public double[][] getActivity(){
		return this.savedActivity;
	}
	
	public void displayProfiling(){
		if(Constants.debug);
		System.out.println("profiling: "+profiling+" steps");
	}
	
	public String[] giveNames() {
		String[] names = new String[graph.getPlottedSeqs().size()];
		int index = 0;
		Iterator<SequenceVertex> it = graph.getPlottedSeqs().iterator();
		
		while(it.hasNext()){
			
			names[index] = it.next().toString();
			index++;
			
		}
		return names;
	}
	
	public ArrayList<SequenceVertex> getSequences(){
		return this.sequences;
	}
	
	public Collection<Template<E>> getTemplates(){
		return this.templates.values();
	}
	
	public OligoGraph<SequenceVertex,E> getGraph(){
		return this.graph;
	}
}
