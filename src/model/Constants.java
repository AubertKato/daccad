package model;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;


public class Constants {
	
	public static double Kduplex = 0.2; // unit is "per nMolar per min"
	public static double displ = 0.2;
	public static double baseStack = 0.2;// dimensionless
	public static double baseDangleL = 1.0;
	public static double baseDangleR = 1.0;
    
	// With saturation
	public static double exoVm = 300;
    public static double exoKmSimple = 440;
    public static double exoKmInhib = 150;
	public static double exoKmTemplate = 10;
    
    public static double polVm = 1050;
    public static double polKm = 80;
    public static double polKmBoth = 5.5;
    
    public static double nickVm = 80;
    public static double nickKm = 30;
    
	public static final double PadiracKSimpleDiv = 0.013453938452081501;
	public static final double PadiracKInhibDiv = 4.618812442295603;
	public static double alpha =3;
	public static double alphaBase = 6;
	
	public static int numberOfPoints = 1000;

	public static double ratioToeholdLeft = 0.002;
	public static double ratioToeholdRight = 0.01;
	public static double absprec = 1e-6;
	public static double relprec = 1e-6;
	
	public static boolean fullExport = false;
	
	// Only used for optimization, right now
	public static final double simpleKmin = 3e-2/Kduplex;
	public static final double simpleKmax = 2e1/Kduplex;
	public static final double inhibKmin = 1e-7/Kduplex;
	public static final double inhibKmax = 1e-1/Kduplex;
	
	public static double ratioSelfStart = 1e-10;
	public static double nickKmBoth = nickKm;
	
	public static boolean debug = false;
	
	public static void reset(){
		Kduplex = 0.2; // unit is "per nMolar per min"
		displ = 0.2;
	    
		// With saturation
		exoVm = 300;
	    exoKmSimple = 440;
	    exoKmInhib = 150;
		exoKmTemplate = 10;
	    
	    polVm = 1050;
	    polKm = 80;
	    polKmBoth = 5.5;
	    
	    nickVm = 80;
	    nickKm = 30;
	    nickKmBoth = nickKm;
	    
		
		alpha =3;
		
		numberOfPoints = 1000;

		ratioToeholdLeft = 0.002;
		ratioToeholdRight = 0.01;
		absprec = 1e-6;
		relprec = 1e-6;
		
		ratioSelfStart = 1e-10;
	}
	
	//Write the configurations of this class
	public static String configsToString(){
		return configsToString(Constants.class);
	}
	
	public static String configsToString(Class<?> subConfigClass){
		StringBuilder sb = new StringBuilder();
		sb.append(new Date(System.currentTimeMillis()).toString()+"\n");
		
		Field[] f = subConfigClass.getFields();
		
		
		for(int i=0; i<f.length; i++){
			try {
				if(f[i].getType().isArray()){
					sb.append(f[i].getName()+"="+Arrays.deepToString((Object[]) f[i].get(null))+"\n");
				} else {
					sb.append(f[i].getName()+"="+f[i].get(null)+"\n"); //Only valid for static methods
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return sb.toString();
	}
	
	/**
	 * A parameter setting is formated as parameterName = parameterValue
	 * @param subConfigClass the class to update
	 * @param parameterName
	 * @param parameterValue
	 */
	public static void readConfigFromString(Class<?> subConfigClass, String parameterName, String parameterValue){
		Field[] fields = subConfigClass.getFields();
			
			
			for(int j = 0; j<fields.length; j++){
				if(fields[j].getName().trim().equals(parameterName.trim())){
					//We found the field, update the parameter
					//Now we need to parse correctly the parameter's value
					try {
						switch(fields[j].getType().toString()){
							case "Integer":
							case "int":
						
								fields[j].setInt(null, Integer.parseInt(parameterValue.trim()));
								break;
							case "Float":
							case "float":
						
								fields[j].setFloat(null, Float.parseFloat(parameterValue.trim()));
								break;
							case "Double":
							case "double":
						
								fields[j].setDouble(null, Double.parseDouble(parameterValue.trim()));
								break;
							case "Boolean":
							case "boolean":
						
								fields[j].setBoolean(null, Boolean.parseBoolean(parameterValue.trim()));
								break;
							case "String":
							case "class java.lang.String":
								fields[j].set(null, parameterValue.trim());
								break;
							case "String[]":
							case "class [Ljava.lang.String;":
								String base = parameterValue.trim(); // format: [ , ... , ]
								base = base.substring(1, base.length()-1); //removed surrounding brackets
								String[] vals = base.split("\\s*,\\s*");
								fields[j].set(null, vals);
								break;
							default:
								System.out.println(fields[j].getType().toString());
						
						}
					} catch (IllegalArgumentException | IllegalAccessException e) {
						
						e.printStackTrace();
					}
					
				}
			}
		
	}
}
