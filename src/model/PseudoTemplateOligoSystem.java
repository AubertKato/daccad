package model;

import model.chemicals.PseudoExtendedSequence;
import model.chemicals.SequenceVertex;
import model.chemicals.Template;
import model.chemicals.TemplateWithModSpecies;
import utils.PseudoTemplateCapableTemplateFactory;
import utils.TemplateFactory;

public class PseudoTemplateOligoSystem extends OligoSystemAllSats<String> {

	public PseudoTemplateOligoSystem(PseudoTemplateGraph<SequenceVertex, String> graph) {
		super(graph, new PseudoTemplateCapableTemplateFactory<String>(graph));
		
		init();
	}
	
	public PseudoTemplateOligoSystem(PseudoTemplateGraph<SequenceVertex, String> graph,SaturationEvaluator<String> se) {
		super(graph,new PseudoTemplateCapableTemplateFactory<String>(graph),se);
		
		init();
	}
	
	public PseudoTemplateOligoSystem(PseudoTemplateGraph<SequenceVertex, String> graph,TemplateFactory<String> tf, SaturationEvaluator<String> se) {
		super(graph,tf,se);
		
		init();
	}
	
	public PseudoTemplateOligoSystem(PseudoTemplateGraph<SequenceVertex, String> graph,TemplateFactory<String> tf) {
		super(graph,tf);
		
		init();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void init(){
		PseudoTemplateGraph<SequenceVertex, String> gr = (PseudoTemplateGraph<SequenceVertex, String>) graph;
		//Add the PseudoExtendedSequences and their pseudotemplates
				
		for(SequenceVertex org : gr.getAllSpeciesWithPseudoTemplate()){
			SequenceVertex pes = gr.getExtendedSpecies(org);
			this.sequences.add(pes);
			this.total++;
			String edge = graph.edgeFactory.createEdge(org, pes);
			Template<String> pt = templateFactory.create(edge);
			templates.put(pt.getName(), pt);		
			}
	}

	//Treat separately extended strands
	@Override
	public double getTotalCurrentFlux(SequenceVertex s) {
		
		if(PseudoExtendedSequence.class.isAssignableFrom(s.getClass())){
			return getTotalCurrentFluxExtended((PseudoExtendedSequence)s);
		} else {
			return super.getTotalCurrentFlux(s);
		}
	}

	protected double getTotalCurrentFluxExtended(PseudoExtendedSequence s) {
		// As Input
		double flux = 0;
		Template<String> temp;
		SequenceVertex original = s.originalSequence;

		//from the pseudotemplate
		temp = templates.get(graph.edgeFactory.createEdge(original, s));
		flux += temp.outputSequenceFlux();
		//System.out.println("debug: base creation"+flux);
		
		for(String e: graph.getOutEdges(original)){
			temp = this.templates.get(e);
			if(TemplateWithModSpecies.class.isAssignableFrom(temp.getClass())){
				flux += ((TemplateWithModSpecies<String>)temp).extinpSequenceFlux();
				//System.out.println("debug: from temp input"+e+" "+flux);
			}
			}
		// As Output
		for (String e: graph.getInEdges(original)) {
			temp = this.templates.get(e);
			if(TemplateWithModSpecies.class.isAssignableFrom(temp.getClass())){
				flux += ((TemplateWithModSpecies<String>)temp).extoutpSequenceFlux();
				//System.out.println("debug: from temp output"+e+" "+flux);
			}
		}
		if (!graph.saturableExo) {

			flux -= s.getConcentration() * graph.exoConc*(Constants.exoVm)/PseudoExtendedSequence.exoKmExtendedSeq;
		} else {
			//flux -= s.getConcentration() * Constants.exoConc*Constants.exoVm
			//		/ (Constants.exoKmSimple * this.observedExoKm);
			if(graph.coupling){
			flux -= s.getConcentration() * graph.exoConc*Constants.exoVm/ computeExoKm(PseudoExtendedSequence.exoKmExtendedSeq);
			} else {
			flux -= s.getConcentration() * graph.exoConc*Constants.exoVm/(PseudoExtendedSequence.exoKmExtendedSeq + s.getConcentration());
			}
		}
		//System.out.println("debug: full flux "+flux);
		return flux;
	}
	
	@Override
	protected double computeExoKm(double myKm){
		double value = myKm;
		
		for (SequenceVertex s: graph.getVertices()) {
			double tempKm = ((graph.inhibitors.containsKey(s)||PseudoExtendedSequence.class.isAssignableFrom(s.getClass()))?Constants.exoKmInhib:Constants.exoKmSimple);
			value += s.getConcentration()*(tempKm==myKm?1.0:myKm/tempKm);
		}
		if(graph.se.enzymeKms[SaturationEvaluator.ENZYME.EXO.value][SaturationEvaluator.TALONE]>0.0){
			for(Template<String> t: this.templates.values()){
				value += (myKm*t.concentrationAlone)/t.exoKm;
			}
		}
		
		return value;
	}
		
	
}
