package model;

import java.util.Arrays;

import model.OligoSystem;
import model.chemicals.SequenceVertex;
import model.chemicals.Template;

/**
 * This is a wrapper for the basic OligoSystem implementation.
 * It gives convenient access to the saturation in the system.
 * It will probably be moved to DACCAD in the future.
 */
public class SaturationEvaluator<E> {
	
	
	/**
	 * Saturation parameter indexes
	 */
	public static final int TALONE = 0;
	public static final int TIN = 1;
	public static final int TOUT= 2;
	public static final int TBOTH = 3;
	public static final int TEXT = 4;
	public static final int TINHIB = 5;
	public static final int SIGNAL = 6; 
	public static final int INHIB = 7;
	
	public enum ENZYME {
		POL(0), NICK(1), EXO(2);
		public final int value;
		
		ENZYME(int value){
			this.value = value;
		}
	};
	
	private double[] polKms;
	private double[] nickKms;
	private double[] exoKms;
	
	public double[][] enzymeKms = new double[3][8];
	
	public SaturationEvaluator( double[] polKms, double[] nickKms, double[] exoKms){
		this.polKms = polKms; // I trust it has the right size (8 for now)
		this.nickKms = nickKms;
		this.exoKms = exoKms;
		this.enzymeKms[0] = this.polKms;
		this.enzymeKms[1] = this.nickKms;
		this.enzymeKms[2] = this.exoKms;
	}
	
	public double getCurrentEnzymeSaturation(ENZYME type, OligoSystem<E> os){
		double value = 1.0; //no sat
		double[] kms = enzymeKms[type.value];
		if(kms[TALONE]> 0.0 ||kms[TIN]> 0.0 ||kms[TOUT]> 0.0 ||kms[TBOTH]> 0.0 ||kms[TEXT]> 0.0 ||kms[TINHIB]> 0.0 ){
			for(Template<E> t : os.templates.values()){ //we take all the current templates
				value += t.getEnzymeUsage(type);
			}
		}
		if(kms[SIGNAL] > 0.0 || kms[INHIB] > 0.0){
			for(SequenceVertex s : os.getSequences()){
				if(!s.isInhib() && kms[SIGNAL] >0.0){
					value+= s.getConcentration()/kms[SIGNAL];
				} else if (s.isInhib() && kms[INHIB] >0.0){
					value+= s.getConcentration()/kms[INHIB];
				}
			}
		}
		return value;
	}
	
	public static final void main(String[] args){
		SaturationEvaluator<String> se = new SaturationEvaluator<String>( new double[]{0.0},new double[]{1.0},new double[]{2.0});
		System.out.println(""+se.enzymeKms[1][0]);
	}

	public int totalNbOfParams() {
		int total = 0;
		for(int i = 0; i<=INHIB; i++){
			if(enzymeKms[0][i]>0.0)
				total++;
			if(enzymeKms[1][i]>0.0)
				total++;
			if(enzymeKms[2][i]>0.0)
				total++;
		}
		return total;
	}
	
	public double getRealParam(int j) {
		int counter = 0;
		
		for(int i = 0; i<3; i++){
			for(int k = 0; k<enzymeKms[i].length; k++){
				if(enzymeKms[i][k] > 0.0){
					if(counter == j){
						return enzymeKms[i][k];
					}
					counter++;
				}
			}
		}
		System.err.println("WARNING: no such parameter");
		return -1;
	}

	public void setRealParam(int j, double exp) {
int counter = 0;
		
		for(int i = 0; i<3; i++){
			for(int k = 0; k<enzymeKms[i].length; k++){
				if(enzymeKms[i][k] > 0.0){
					if(counter == j){
						enzymeKms[i][k] = exp;
						return;
					}
					counter++;
				}
			}
		}
		System.err.println("WARNING: no such parameter");
		
	}
	
	public Object clone(){
		SaturationEvaluator<E> ret = new SaturationEvaluator<E>(Arrays.copyOf(polKms, polKms.length),Arrays.copyOf(nickKms, nickKms.length),Arrays.copyOf(exoKms, exoKms.length));
		return ret;
	}
	
}
