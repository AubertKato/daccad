package model.chemicals;

import model.Constants;
import model.chemicals.SequenceVertex;

public class PseudoExtendedSequence extends SequenceVertex {

	public SequenceVertex originalSequence;
	
	public static double exoKmExtendedSeq = Constants.exoKmInhib;

	public PseudoExtendedSequence(SequenceVertex s, double init) {
		super(s.ID, init);
		originalSequence = s;
		this.inhib = true; //Used as a cheat to get the correct saturation
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
