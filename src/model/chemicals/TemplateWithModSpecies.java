package model.chemicals;

import model.Constants;
import model.OligoGraph;
import model.SaturationEvaluator;
import model.SaturationEvaluator.ENZYME;
import model.chemicals.SequenceVertex;
import model.chemicals.Template;

public class TemplateWithModSpecies <E>  extends Template<E>{	

  

private static final long serialVersionUID = 4394186475468043658L;
  //public int numberOfStates = 11;
  
  protected PseudoExtendedSequence outp;
  protected PseudoExtendedSequence inp;
  protected double concwithextinp;
  protected double concwithbothextinp;
  protected double concwithextoutp;
  protected double concwithbothextoutp;
  protected double concwithbothextinpoutp;
  
  protected double inputSlowdown;
  
  public TemplateWithModSpecies(OligoGraph<SequenceVertex, E> parent,
			double totalConcentration, double stackSlowdown, double dangleL,
			double dangleR, SequenceVertex from, SequenceVertex to,
			SequenceVertex inhib, PseudoExtendedSequence inp, PseudoExtendedSequence outp,double inputSlowdown) {
		super(parent, totalConcentration, stackSlowdown, dangleL, dangleR, from, to,
				inhib);
		// TODO Auto-generated constructor stub
		this.inp = inp;
		this.outp = outp;
		this.inputSlowdown = inputSlowdown;
		this.numberOfStates = 11;
	}

  

  public double[] flux() {
	  //System.out.println(""+inp+" "+outp);
    double[] ans = { aloneFlux(), inputFlux(), outputFlux(), bothFlux(), 
      extendedFlux(), inhibitedFlux(), extinputFlux(), extoutputFlux(), bothWithExtinputFlux(),
      bothWithExtoutputFlux(), bothWithExtbothFlux()};
    return ans;
  }

  //Done
  private double bothWithExtoutputFlux() {
	  double poldispl = this.parent.isInhibitor(this.to) ? this.polyboth * Constants.displ : 
	      this.poly;
	if(outp==null){
		return 0;
	}
	double koutp = ((Double)this.parent.K.get(to)).doubleValue(); //Because they have the same sequence
	return model.Constants.Kduplex*(outp.getConcentration()*this.concentrationWithInput
			+from.getConcentration()*inputSlowdown*this.concwithextoutp)-
			this.concwithbothextoutp * (((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * stackSlowdown/dangleRSlowdown+ stackSlowdown/dangleLSlowdown* koutp * Constants.Kduplex + poldispl)
			;
}


//Done
private double bothWithExtbothFlux() {
	
	
	if(outp==null||inp==null){
		return 0;
	}
	double kinp = ((Double)this.parent.K.get(from)).doubleValue(); //Because they have the same sequence
	double koutp = ((Double)this.parent.K.get(to)).doubleValue(); //Because they have the same sequence
	return Constants.Kduplex*(outp.getConcentration()*this.concwithextinp+inp.getConcentration()*inputSlowdown*this.concwithextoutp)-
	this.concwithbothextinpoutp*(kinp * Constants.Kduplex * stackSlowdown/dangleRSlowdown+ stackSlowdown/dangleLSlowdown* koutp * Constants.Kduplex);
}


//Done
private double extinputFlux() {

	if(inp==null){
		return 0;
	}
	double concOutp = 0;
	double KOutp = 0;
	double concInhib = 0;
	if(outp!=null){
		concOutp = outp.getConcentration();
		KOutp =((Double)this.parent.K.get(this.to)).doubleValue();
	}
	
	double kinp = ((Double)this.parent.K.get(from)).doubleValue(); //Because they have the same sequence
	
	if(inhib!=null){
		concInhib = inhib.getConcentration();
	}
	return Constants.Kduplex*(inp.getConcentration()*inputSlowdown*this.concentrationAlone+KOutp *stackSlowdown/dangleLSlowdown*this.concwithbothextinpoutp
			+((Double)this.parent.K.get(this.to)).doubleValue() *stackSlowdown/dangleLSlowdown*this.concwithbothextinp+Constants.ratioToeholdLeft*inp.getConcentration()*inputSlowdown*this.concentrationInhibited)-
	this.concwithextinp*Constants.Kduplex*(kinp*this.dangleLSlowdown+concOutp+to.getConcentration()+concInhib);
}


//Done
private double extoutputFlux() {
	if(outp==null){
		return 0;
	}
	
	double concInp = 0;
	double KInp = 0;
	double concInhib = 0;
	if(inp!=null){
		concInp = inp.getConcentration()*inputSlowdown;
		KInp =((Double)this.parent.K.get(this.from)).doubleValue();
	}
	
	double koutp = ((Double)this.parent.K.get(to)).doubleValue(); //Because they have the same sequence
	
	if(inhib!=null){
		concInhib = inhib.getConcentration();
	}
	return Constants.Kduplex*(outp.getConcentration()*this.concentrationAlone+KInp *stackSlowdown/dangleRSlowdown*this.concwithbothextinpoutp
			+((Double)this.parent.K.get(this.from)).doubleValue() *stackSlowdown/dangleRSlowdown*this.concwithbothextoutp+Constants.ratioToeholdRight*outp.getConcentration()*this.concentrationInhibited)-
	this.concwithextoutp*Constants.Kduplex*(koutp*this.dangleRSlowdown+concInp+from.getConcentration()*inputSlowdown+concInhib);
}


//Done
private double bothWithExtinputFlux() {
	if(inp==null){
		return 0;
	}
	
	double kinp = ((Double)this.parent.K.get(from)).doubleValue(); //Because they have the same sequence
	
	
	return model.Constants.Kduplex*(inp.getConcentration()*inputSlowdown*this.concentrationWithOutput
			+to.getConcentration()*this.concwithextinp)-
			this.concwithbothextinp * (((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * stackSlowdown/dangleLSlowdown+ stackSlowdown/dangleRSlowdown* kinp * Constants.Kduplex)
			;
}

//Done
public double extinpSequenceFlux(){
	if(inp==null){
		return 0;
	}
	double conc = inp.getConcentration()*inputSlowdown;
	double K = ((Double)this.parent.K.get(from)).doubleValue();
	double concInhib = 0;
	if(inhib!=null){
		concInhib = inhib.getConcentration();
	}
	return Constants.Kduplex*K*(concwithextinp*dangleLSlowdown+concwithbothextinp*stackSlowdown/dangleRSlowdown+concwithbothextinpoutp*stackSlowdown/dangleRSlowdown)
				+Constants.Kduplex*concInhib*concwithextinp-Constants.Kduplex*conc*(concentrationAlone+concentrationWithOutput+concwithextoutp+Constants.ratioToeholdLeft*concentrationInhibited);
}

//Done
public double extoutpSequenceFlux(){
	if(outp==null){
		return 0;
	}
	
	double poldispl = this.parent.isInhibitor(this.to) ? this.polyboth * Constants.displ : 
	      this.poly;
	
	double conc = outp.getConcentration();
	double K = ((Double)this.parent.K.get(to)).doubleValue();
	double concInhib = 0;
	if(inhib!=null){
		concInhib = inhib.getConcentration();
	}
	return Constants.Kduplex*K*(concwithextoutp*dangleRSlowdown+concwithbothextoutp*stackSlowdown/dangleLSlowdown+concwithbothextinpoutp*stackSlowdown/dangleLSlowdown) + poldispl*concwithbothextoutp
				+Constants.Kduplex*concInhib*concwithextoutp-Constants.Kduplex*conc*(concentrationAlone+concentrationWithInput+concwithextinp+Constants.ratioToeholdRight*concentrationInhibited);
}

//DONE
public double inhibSequenceFlux() {
    double conc = this.inhib.getConcentration();
    double concinp = 0.0;
   
    double concoutp = 0.0;
    
    
    if(inp != null){
    	concinp = inp.getConcentration()*inputSlowdown;
    	
    }
    
    if(outp != null){
    	concoutp = outp.getConcentration();
    	
    }
    
    return ((Double)this.parent.K.get(this.inhib)).doubleValue() * truealpha * Constants.Kduplex * 
      this.concentrationInhibited - 
      Constants.Kduplex * 
      conc * (
      this.concentrationAlone + this.concentrationWithInput + 
      this.concentrationWithOutput+this.concwithextinp+this.concwithextoutp)
      + Constants.Kduplex*this.concentrationInhibited*(Constants.ratioToeholdLeft*(this.from.getConcentration()*inputSlowdown+concinp)+Constants.ratioToeholdRight*(this.to.getConcentration()+concoutp));
  }

//DONE
  public double inputSequenceFlux() {
    double conc = this.from.getConcentration()*inputSlowdown;
    double kinhib;
    double inhib;
    if (this.inhib != null) {
      inhib = this.inhib.getConcentration();
      kinhib = Constants.Kduplex;
    }
    else {
      inhib = 0.0D;
      kinhib = 0.0D;
    }
    
    
    
    double debug = ((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * (
      dangleLSlowdown*this.concentrationWithInput + stackSlowdown/dangleRSlowdown*this.concentrationWithBoth
      + stackSlowdown/dangleRSlowdown*this.concwithbothextoutp) + 
      kinhib * 
      inhib * 
      this.concentrationWithInput - 
      Constants.Kduplex * 
      conc * (
      this.concentrationAlone + this.concentrationWithOutput + this.concwithextoutp + Constants.ratioToeholdLeft*this.concentrationInhibited);

    return debug;
  }

  //DONE
  public double outputSequenceFlux() {
    double conc = this.to.getConcentration();
    double kinhib;
    double inhib;
    if (this.inhib != null) {
      inhib = this.inhib.getConcentration();
      kinhib = Constants.Kduplex;
    }
    else {
      inhib = 0.0D;
      kinhib = 0.0D;
    }
    double pol;
    if (this.parent.isInhibitor(this.to))
      pol = this.polyboth * Constants.displ;
    else {
      pol = this.poly;
    }
    pol*=parent.polConc;
    double debug = ((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * (
      dangleRSlowdown*this.concentrationWithOutput + stackSlowdown/dangleLSlowdown*this.concentrationWithBoth
      + stackSlowdown/dangleLSlowdown*this.concwithbothextinp) + 
      kinhib * 
      inhib * 
      this.concentrationWithOutput - 
      Constants.Kduplex * 
      conc * (
      this.concentrationAlone + this.concentrationWithInput + this.concwithextinp + Constants.ratioToeholdRight*this.concentrationInhibited) + 
      pol * 
      this.concentrationWithBoth;

    return debug;
  }

  @Override
  protected double aloneFlux() {
    double concIn = this.from.getConcentration()*inputSlowdown;
    double concOut = this.to.getConcentration();
    double kreleaseInhib;
    double concInhib;
    double selfstart = 0;
    if(parent.selfStart){
    	selfstart = Constants.ratioSelfStart;
    }
    
    double concinp = 0.0;
    double Kinp = 0.0;
    double concoutp = 0.0;
    double Koutp = 0.0;
    
    if(inp != null){
    	concinp = inp.getConcentration()*inputSlowdown;
    	Kinp = ((Double)this.parent.K.get(from)).doubleValue();
    }
    
    if(outp != null){
    	concoutp = outp.getConcentration();
    	Koutp = ((Double)this.parent.K.get(to)).doubleValue();
    }
    
    if (this.inhib != null) {
      concInhib = this.inhib.getConcentration();
      kreleaseInhib = ((Double)this.parent.K.get(this.inhib)).doubleValue() * truealpha * Constants.Kduplex;
    } else {
      concInhib = 0.0D;
      kreleaseInhib = 0.0D;
    }
    
    

    double debug = ((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * 
      dangleLSlowdown*this.concentrationWithInput + 
      dangleRSlowdown*((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * 
      this.concentrationWithOutput + 
      Kinp*Constants.Kduplex*dangleLSlowdown*concwithextinp+
      Koutp*Constants.Kduplex*dangleRSlowdown*concwithextoutp+
      kreleaseInhib * 
      this.concentrationInhibited - 
      this.concentrationAlone * 
      Constants.Kduplex * (
      concIn + concOut + concInhib + concinp + concoutp)
      -this.concentrationAlone*selfstart*poly*parent.polConc;

    return debug;
  }

  @Override
  protected double inputFlux() {
    double concIn = this.from.getConcentration()*inputSlowdown;
    double concOut = this.to.getConcentration();
    double concInhib;
    
    
    double concoutp = 0.0;
    double Koutp = 0.0;
    
    
    
    if(outp != null){
    	concoutp = outp.getConcentration();
    	Koutp = ((Double)this.parent.K.get(to)).doubleValue();
    }

    if (this.inhib != null)
      concInhib = this.inhib.getConcentration();
    else {
      concInhib = 0.0D;
    }
    return Constants.Kduplex * 
      concIn * 
      (this.concentrationAlone + Constants.ratioToeholdLeft*this.concentrationInhibited) + 
      ((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * 
      stackSlowdown/dangleLSlowdown* this.concentrationWithBoth +
      Koutp*Constants.Kduplex*stackSlowdown/dangleLSlowdown*this.concwithbothextoutp- 
      this.concentrationWithInput * (
      dangleLSlowdown*((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex + this.poly*parent.polConc + Constants.Kduplex * 
      concOut + Constants.Kduplex*concoutp+Constants.Kduplex * 
      concInhib);
  }

  //DONE
  @Override
  protected double outputFlux()
  {
    double concIn = this.from.getConcentration()*inputSlowdown;
    double concOut = this.to.getConcentration();
    double concInhib;
    double selfstart = 0;
    if(parent.selfStart){
    	selfstart = Constants.ratioSelfStart;
    }
    
    double concinp = 0.0;
    double Kinp = 0.0;
    
    
    if(inp != null){
    	concinp = inp.getConcentration()*inputSlowdown;
    	Kinp = ((Double)this.parent.K.get(from)).doubleValue();
    }
    
    
    
    if (this.inhib != null)
      concInhib = this.inhib.getConcentration();
    else {
      concInhib = 0.0D;
    }
    double debug = concOut * 
      Constants.Kduplex * 
      (this.concentrationAlone + Constants.ratioToeholdRight*this.concentrationInhibited) + 
      ((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * 
      stackSlowdown/dangleRSlowdown* this.concentrationWithBoth +
      Kinp*Constants.Kduplex*stackSlowdown/dangleRSlowdown*this.concwithbothextinp- 
      this.concentrationWithOutput * (
      dangleRSlowdown*((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex + 
      Constants.Kduplex*concinp+ Constants.Kduplex * concIn + Constants.Kduplex * 
      concInhib)
      + this.concentrationAlone*selfstart*poly*parent.polConc;

    return debug;
  }

  @Override
  protected double bothFlux() {
    double concIn = this.from.getConcentration()*inputSlowdown;
    double concOut = this.to.getConcentration();
    double pol = this.parent.isInhibitor(this.to) ? this.polyboth * Constants.displ : 
      this.poly;
    pol*=parent.polConc;
    return Constants.Kduplex * concIn * this.concentrationWithOutput + 
      Constants.Kduplex * concOut * this.concentrationWithInput + 
      this.nick * parent.nickConc * this.concentrationExtended - 
       this.concentrationWithBoth * (((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * stackSlowdown/dangleRSlowdown+ stackSlowdown/dangleLSlowdown* ((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex + pol);
  }

  @Override
  protected double extendedFlux() {
    double poldispl = this.parent.isInhibitor(this.to) ? this.polyboth * Constants.displ : 
      this.poly;

   poldispl*=parent.polConc;
    return this.poly * parent.polConc *
      this.concentrationWithInput + 
      poldispl * 
      (this.concentrationWithBoth + this.concwithbothextoutp) - 
      this.nick * parent.nickConc* this.concentrationExtended;
  }

  @Override
  protected double inhibitedFlux()
  {
    double krelease;
    double conc;
    
    
    double concinp = 0.0;
    
    double concoutp = 0.0;
    
    
    if(inp != null){
    	concinp = inp.getConcentration()*inputSlowdown;
    	
    }
    
    if(outp != null){
    	concoutp = outp.getConcentration();
    	
    }

    if (this.inhib != null) {
      conc = this.inhib.getConcentration();
      krelease = ((Double)this.parent.K.get(this.inhib)).doubleValue() * truealpha * Constants.Kduplex;
    } else {
      conc = 0.0D;
      krelease = 0.0D;
    }

    return -krelease * 
      this.concentrationInhibited 
      - this.concentrationInhibited*Constants.Kduplex*(Constants.ratioToeholdLeft*(this.from.getConcentration()*inputSlowdown+concinp)+Constants.ratioToeholdRight*(this.to.getConcentration()+concoutp))
       + Constants.Kduplex * 
      conc * (
      this.concentrationAlone + this.concentrationWithInput + 
      this.concentrationWithOutput+ this.concwithextinp + this.concwithextoutp);
  }

  @Override
  public double getEnzymeUsage(ENZYME type){
	  double value = 0.0;
	  double[] kms = parent.se.enzymeKms[type.value];
	  if (kms[SaturationEvaluator.TALONE] > 0.0){
			value += concentrationAlone/kms[SaturationEvaluator.TALONE];
		}
		if (kms[SaturationEvaluator.TIN] > 0.0){
			value += concentrationWithInput/kms[SaturationEvaluator.TIN];
			value += concwithextinp/kms[SaturationEvaluator.TIN];
		}
		if (kms[SaturationEvaluator.TOUT] > 0.0){
			value += concentrationWithOutput/kms[SaturationEvaluator.TOUT];
			value += concwithextoutp/kms[SaturationEvaluator.TOUT];
		}
		if (kms[SaturationEvaluator.TBOTH] > 0.0){
			value += concentrationWithBoth/kms[SaturationEvaluator.TBOTH];
			value += concwithbothextinp/kms[SaturationEvaluator.TBOTH];
			value += concwithbothextoutp/kms[SaturationEvaluator.TBOTH];
			value += concwithbothextinpoutp/kms[SaturationEvaluator.TBOTH];
		}	
		if (kms[SaturationEvaluator.TEXT] > 0.0){
			value += concentrationExtended/kms[SaturationEvaluator.TEXT];
		}
		if (kms[SaturationEvaluator.TINHIB] > 0.0){
			value += concentrationInhibited/kms[SaturationEvaluator.TINHIB];
		}
    return value;
  }

  public void setAllocatedPoly(double observedKm) {
		double[] kms = parent.se.enzymeKms[ENZYME.POL.value];
	    this.poly = (Constants.polVm)/(kms[SaturationEvaluator.TIN]*observedKm);
	    this.polyboth = Constants.polVm/(kms[SaturationEvaluator.TBOTH]*observedKm);
	  }

  public void setAllocatedNick(double observedKm) {
	    this.nick = Constants.nickVm/(parent.se.enzymeKms[ENZYME.NICK.value][SaturationEvaluator.TEXT]*observedKm);
	  }
  
 

  public double[] getStates() {
    double[] ans = { this.concentrationAlone, this.concentrationWithInput, 
      this.concentrationWithOutput, this.concentrationWithBoth, 
      this.concentrationExtended, this.concentrationInhibited,
      concwithextinp, concwithbothextinp, concwithextoutp, concwithbothextoutp,
      concwithbothextinpoutp};
    return ans;
  }

  public void setStates(double[] values) throws InvalidConcentrationException {
    if (values.length != 11) {
      System.err
        .println("Error: wrong internal values setting for template " + 
        this);
      return;
    }
    for (int i = 0; i < 6; i++) {
      if ((values[i] < 0.0D) || (values[i] > this.totalConcentration+1))
      {
        values[i] = 0.0D;
      }
    }

    this.concentrationAlone = values[0];
    this.concentrationWithInput = values[1];
    this.concentrationWithOutput = values[2];
    this.concentrationWithBoth = values[3];
    this.concentrationExtended = values[4];
    this.concentrationInhibited = values[5];
    concwithextinp = values[6];
    concwithextoutp= values[7]; 
    concwithbothextinp = values[8];
    concwithbothextoutp = values[9];
    concwithbothextinpoutp = values[10];
  }

  public SequenceVertex getFrom() {
    return this.from;
  }

  public SequenceVertex getTo() {
    return this.to;
  }
  
  public SequenceVertex getInhib(){
	  return this.inhib;
  }
  
  public PseudoExtendedSequence getFromExt(){
	return this.inp;  
  }
  
  public PseudoExtendedSequence getToExt(){
	  return this.outp;
  }
  
  public void setFromExt(PseudoExtendedSequence inp){
	  this.inp = inp;
  }
  
  public void setToExt(PseudoExtendedSequence outp){
	  this.outp = outp;
  }
  
  public double totalConcentration() {
    return this.concentrationAlone + this.concentrationExtended + 
      this.concentrationInhibited + this.concentrationWithBoth + 
      this.concentrationWithInput + this.concentrationWithOutput+
      concwithextinp+ concwithbothextinp+ concwithextoutp+ concwithbothextoutp+
      concwithbothextinpoutp;
  }

  public void reset() {
    this.concentrationAlone = this.totalConcentration;
    this.concentrationExtended = 0.0D;
    this.concentrationInhibited = 0.0D;
    this.concentrationWithBoth = 0.0D;
    this.concentrationWithOutput = 0.0D;
    this.concentrationWithInput = 0.0D;
    this.concwithbothextinp = 0.0;
    this.concwithbothextinpoutp = 0.0;
    this.concwithbothextoutp = 0.0;
    this.concwithextinp = 0.0;
    this.concwithextoutp = 0.0;
    this.poly = (Constants.polVm / parent.se.enzymeKms[ENZYME.POL.value][SaturationEvaluator.TIN]);
    this.nick = (Constants.nickVm / parent.se.enzymeKms[ENZYME.NICK.value][SaturationEvaluator.TEXT]);
  }
  
  public double getLoad(){
	  return this.concentrationExtended+this.concentrationWithBoth+
	  this.concentrationWithInput+this.concwithbothextinp+this.concwithbothextinpoutp
	  +this.concwithbothextoutp+this.concwithextinp;
  }

  public String toString()
  {
    return "S" + this.from.ID + "->" + (this.parent.isInhibitor(this.to) ? "I(" + this.parent.inhibitors.get(this.to).toString() + ")" : new StringBuilder("S").append(this.to.ID).toString());
  }
  
  public String getName()
  {
	  return this.from.ID + "->" + this.to.ID;
  }
}