package model.chemicals;

import model.Constants;
import model.OligoGraph;

/**
 * This class adds an hybridization slowdown to the input
 * by missing a few bases. This requires us to update most
 * flux calculation, but the behavior remains the same.
 * /!\ We use a trick: we multiply the conc of input 
 * (when it is set in the header of the function) by the 
 * slowdown, instead of the hybridization speed. 
 */

public class ModTemplate<E> extends Template<E> {
	
	protected double inputSlowdown;

	public ModTemplate(OligoGraph<SequenceVertex, E> parent,
			double totalConcentration, double stackSlowdown, double dangleL,
			double dangleR, SequenceVertex from, SequenceVertex to,
			SequenceVertex inhib, double inputSlowdown) {
		super(parent, totalConcentration, stackSlowdown, dangleL, dangleR, from, to,
				inhib);
		this.inputSlowdown = inputSlowdown;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public double inhibSequenceFlux() {
	    double conc = this.inhib.getConcentration();
	    return ((Double)this.parent.K.get(this.inhib)).doubleValue() * truealpha * Constants.Kduplex * 
	      this.concentrationInhibited - 
	      Constants.Kduplex * 
	      conc * (
	      this.concentrationAlone + this.concentrationWithInput + 
	      this.concentrationWithOutput)
	      + Constants.Kduplex*this.concentrationInhibited*(Constants.ratioToeholdLeft*this.from.getConcentration()*inputSlowdown
	    		  +Constants.ratioToeholdRight*this.to.getConcentration());
	  }
	
	@Override
	public double inputSequenceFlux() {
	    double conc = this.from.getConcentration()*inputSlowdown; //Trick
	    double kinhib;
	    double inhib;
	    if (this.inhib != null) {
	      inhib = this.inhib.getConcentration();
	      kinhib = Constants.Kduplex;
	    }
	    else {
	      inhib = 0.0D;
	      kinhib = 0.0D;
	    }
	    double debug = ((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * (
	      dangleLSlowdown*this.concentrationWithInput + stackSlowdown/dangleRSlowdown*this.concentrationWithBoth) + 
	      kinhib * 
	      inhib * 
	      this.concentrationWithInput - 
	      Constants.Kduplex * 
	      conc * (
	      this.concentrationAlone + this.concentrationWithOutput + Constants.ratioToeholdLeft*this.concentrationInhibited);

	    return debug;
	  }
	
	@Override
	protected double aloneFlux() {
	    double concIn = this.from.getConcentration()*inputSlowdown; //trick
	    double concOut = this.to.getConcentration();
	    double kreleaseInhib;
	    double concInhib;
	    double selfstart = 0;
	    if(parent.selfStart){
	    	selfstart = Constants.ratioSelfStart;
	    }
	    if (this.inhib != null) {
	      concInhib = this.inhib.getConcentration();
	      kreleaseInhib = ((Double)this.parent.K.get(this.inhib)).doubleValue() * truealpha * Constants.Kduplex;
	    } else {
	      concInhib = 0.0D;
	      kreleaseInhib = 0.0D;
	    }
	    

	    double debug = ((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * 
	      dangleLSlowdown*this.concentrationWithInput + 
	      dangleRSlowdown*((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * 
	      this.concentrationWithOutput + 
	      kreleaseInhib * 
	      this.concentrationInhibited - 
	      this.concentrationAlone * 
	      Constants.Kduplex * (
	      concIn + concOut + concInhib)
	      -this.concentrationAlone*selfstart*poly*parent.polConc;

	    return debug;
	  }
	
	@Override
	protected double inputFlux() {
	    double concIn = this.from.getConcentration()*inputSlowdown;
	    double concOut = this.to.getConcentration();
	    double concInhib;

	    if (this.inhib != null)
	      concInhib = this.inhib.getConcentration();
	    else {
	      concInhib = 0.0D;
	    }
	    return Constants.Kduplex * 
	      concIn * 
	      (this.concentrationAlone + Constants.ratioToeholdLeft*this.concentrationInhibited) + 
	      ((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * 
	      stackSlowdown/dangleLSlowdown* this.concentrationWithBoth - 
	      this.concentrationWithInput * (
	      dangleLSlowdown*((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex + this.poly*parent.polConc + Constants.Kduplex * 
	      concOut + Constants.Kduplex * 
	      concInhib);
	  }

	  @Override
	  protected double outputFlux()
	  {
	    double concIn = this.from.getConcentration()*inputSlowdown;
	    double concOut = this.to.getConcentration();
	    double concInhib;
	    double selfstart = 0;
	    if(parent.selfStart){
	    	selfstart = Constants.ratioSelfStart;
	    }
	    if (this.inhib != null)
	      concInhib = this.inhib.getConcentration();
	    else {
	      concInhib = 0.0D;
	    }
	    double debug = concOut * 
	      Constants.Kduplex * 
	      (this.concentrationAlone + Constants.ratioToeholdRight*this.concentrationInhibited) + 
	      ((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * 
	      stackSlowdown/dangleRSlowdown* this.concentrationWithBoth - 
	      this.concentrationWithOutput * (
	      dangleRSlowdown*((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex + Constants.Kduplex * concIn + Constants.Kduplex * 
	      concInhib)
	      + this.concentrationAlone*selfstart*poly*parent.polConc;

	    return debug;
	  }

	  @Override
	  protected double bothFlux() {
	    double concIn = this.from.getConcentration()*inputSlowdown;
	    double concOut = this.to.getConcentration();
	    double pol = this.parent.isInhibitor(this.to) ? this.polyboth * Constants.displ : 
	      this.poly;
	    pol*=parent.polConc;
	    return Constants.Kduplex * concIn * this.concentrationWithOutput + 
	      Constants.Kduplex * concOut * this.concentrationWithInput + 
	      this.nick * parent.nickConc * this.concentrationExtended - 
	       this.concentrationWithBoth * (((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * stackSlowdown/dangleRSlowdown+ stackSlowdown/dangleLSlowdown* ((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex + pol);
	  }

	  @Override
	  protected double inhibitedFlux()
	  {
	    double krelease;
	    double conc;

	    if (this.inhib != null) {
	      conc = this.inhib.getConcentration();
	      krelease = ((Double)this.parent.K.get(this.inhib)).doubleValue() * truealpha * Constants.Kduplex;
	    } else {
	      conc = 0.0D;
	      krelease = 0.0D;
	    }

	    return -krelease * 
	      this.concentrationInhibited 
	      - this.concentrationInhibited*Constants.Kduplex*(Constants.ratioToeholdLeft*this.from.getConcentration()*inputSlowdown
	    		  +Constants.ratioToeholdRight*this.to.getConcentration())
	       + Constants.Kduplex * 
	      conc * (
	      this.concentrationAlone + this.concentrationWithInput + 
	      this.concentrationWithOutput);
	  }
	  
	  //TODO: Should the size difference affect the template in any way?
}
