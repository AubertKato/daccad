package model.chemicals;

import java.io.Serializable;

import model.Constants;
import model.OligoGraph;
import model.SaturationEvaluator;
import model.SaturationEvaluator.ENZYME;
import model.SlowdownConstants;

public class Template <E> implements Serializable {	

  private static final long serialVersionUID = 4394186475468043658L;
  public int numberOfStates = 6;
  public double totalConcentration;
  public double concentrationAlone;
  public double concentrationWithInput;
  public double concentrationWithOutput;
  public double concentrationWithBoth;
  public double concentrationExtended;
  public double concentrationInhibited;
  public double stackSlowdown;
  public double dangleLSlowdown;
  public double dangleRSlowdown;
  public double exoKm;
  protected OligoGraph<SequenceVertex, E> parent;
  protected SequenceVertex from;
  protected SequenceVertex to;
  protected SequenceVertex inhib;
  protected double poly;
  protected double polyboth;
  protected double nick;
  protected double truealpha;

  public Template(OligoGraph<SequenceVertex, E> parent, double totalConcentration, double stackSlowdown, double dangleL, double dangleR, SequenceVertex from, SequenceVertex to, SequenceVertex inhib)
  {
    this.parent = parent;
    
    this.totalConcentration = totalConcentration;
    this.concentrationAlone = totalConcentration;
    this.concentrationExtended = 0.0D;
    this.concentrationInhibited = 0.0D;
    this.concentrationWithBoth = 0.0D;
    this.concentrationWithOutput = 0.0D;
    this.concentrationWithInput = 0.0D;
    
    this.stackSlowdown = stackSlowdown;
    this.dangleLSlowdown = dangleL;
    this.dangleRSlowdown = dangleR;
    if(inhib != null){
    	truealpha = (parent.dangle && parent.getIncidentEdges(inhib).iterator().hasNext())? SlowdownConstants.inhibDangleSlowdown*Constants.alphaBase*parent.getDangleR(parent.getIncidentEdges(inhib).iterator().next()):Constants.alpha; 
    	//the previous magical formula represents the fact that the regular alpha is based
    	//on the stability of the inhibitor on its template, including the dangle. If dangles
    	//are computed separatly, we have to remove the slowdown of the template from alpha(base).
    } else {
    	numberOfStates = 5;
    	truealpha = 0;
    }

    this.from = from;
    this.to = to;
    this.inhib = inhib;
    
    try{
    this.exoKm = parent.se.enzymeKms[ENZYME.EXO.value][SaturationEvaluator.TALONE];
    } catch(java.lang.NullPointerException e){
    	this.exoKm = 1.0; //In the case where it doesn't matter.
    }
  }

  public double[] flux() {
	  double[] ans;
	  if (inhib!= null) {
		  ans = new double[] { aloneFlux(), inputFlux(), outputFlux(), bothFlux(), 
				  extendedFlux(), inhibitedFlux() };
	  } else {
		  ans = new double[] { aloneFlux(), inputFlux(), outputFlux(), bothFlux(), 
			      extendedFlux() };
	  }
    return ans;
  }

  public double inhibSequenceFlux() {
    double conc = this.inhib.getConcentration();
    return ((Double)this.parent.K.get(this.inhib)).doubleValue() * truealpha * Constants.Kduplex * 
      this.concentrationInhibited - 
      Constants.Kduplex * 
      conc * (
      this.concentrationAlone + this.concentrationWithInput + 
      this.concentrationWithOutput)
      + Constants.Kduplex*this.concentrationInhibited*(Constants.ratioToeholdLeft*this.from.getConcentration()+Constants.ratioToeholdRight*this.to.getConcentration());
  }

  public double inputSequenceFlux() {
    double conc = this.from.getConcentration();
    double kinhib;
    double inhib;
    if (this.inhib != null) {
      inhib = this.inhib.getConcentration();
      kinhib = Constants.Kduplex;
    }
    else {
      inhib = 0.0D;
      kinhib = 0.0D;
    }
    double debug = ((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * (
      dangleLSlowdown*this.concentrationWithInput + stackSlowdown/dangleRSlowdown*this.concentrationWithBoth) + 
      kinhib * 
      inhib * 
      this.concentrationWithInput - 
      Constants.Kduplex * 
      conc * (
      this.concentrationAlone + this.concentrationWithOutput + Constants.ratioToeholdLeft*this.concentrationInhibited);

    return debug;
  }

  public double outputSequenceFlux() {
    double conc = this.to.getConcentration();
    double kinhib;
    double inhib;
    if (this.inhib != null) {
      inhib = this.inhib.getConcentration();
      kinhib = Constants.Kduplex;
    }
    else {
      inhib = 0.0D;
      kinhib = 0.0D;
    }
    double pol;
    if (this.parent.isInhibitor(this.to))
      pol = this.polyboth * Constants.displ;
    else {
      pol = this.poly;
    }
    pol*=parent.polConc;
    double debug = ((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * (
      dangleRSlowdown*this.concentrationWithOutput + stackSlowdown/dangleLSlowdown*this.concentrationWithBoth) + 
      kinhib * 
      inhib * 
      this.concentrationWithOutput - 
      Constants.Kduplex * 
      conc * (
      this.concentrationAlone + this.concentrationWithInput + Constants.ratioToeholdRight*this.concentrationInhibited) + 
      pol * 
      this.concentrationWithBoth;

    return debug;
  }

  protected double aloneFlux() {
    double concIn = this.from.getConcentration();
    double concOut = this.to.getConcentration();
    double kreleaseInhib;
    double concInhib;
    double selfstart = 0;
    if(parent.selfStart){
    	selfstart = Constants.ratioSelfStart;
    }
    if (this.inhib != null) {
      concInhib = this.inhib.getConcentration();
      kreleaseInhib = ((Double)this.parent.K.get(this.inhib)).doubleValue() * truealpha * Constants.Kduplex;
    } else {
      concInhib = 0.0D;
      kreleaseInhib = 0.0D;
    }

    double debug = ((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * 
      dangleLSlowdown*this.concentrationWithInput + 
      dangleRSlowdown*((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * 
      this.concentrationWithOutput + 
      kreleaseInhib * 
      this.concentrationInhibited - 
      this.concentrationAlone * 
      Constants.Kduplex * (
      concIn + concOut + concInhib)
      -this.concentrationAlone*selfstart*poly*parent.polConc;

    return debug;
  }

  protected double inputFlux() {
    double concIn = this.from.getConcentration();
    double concOut = this.to.getConcentration();
    double concInhib;

    if (this.inhib != null)
      concInhib = this.inhib.getConcentration();
    else {
      concInhib = 0.0D;
    }
    return Constants.Kduplex * 
      concIn * 
      (this.concentrationAlone + Constants.ratioToeholdLeft*this.concentrationInhibited) + 
      ((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex * 
      stackSlowdown/dangleLSlowdown* this.concentrationWithBoth - 
      this.concentrationWithInput * (
      dangleLSlowdown*((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex + this.poly*parent.polConc + Constants.Kduplex * 
      concOut + Constants.Kduplex * 
      concInhib);
  }

  protected double outputFlux()
  {
    double concIn = this.from.getConcentration();
    double concOut = this.to.getConcentration();
    double concInhib;
    double selfstart = 0;
    if(parent.selfStart){
    	selfstart = Constants.ratioSelfStart;
    }
    if (this.inhib != null)
      concInhib = this.inhib.getConcentration();
    else {
      concInhib = 0.0D;
    }
    double debug = concOut * 
      Constants.Kduplex * 
      (this.concentrationAlone + Constants.ratioToeholdRight*this.concentrationInhibited) + 
      ((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * 
      stackSlowdown/dangleRSlowdown* this.concentrationWithBoth - 
      this.concentrationWithOutput * (
      dangleRSlowdown*((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex + Constants.Kduplex * concIn + Constants.Kduplex * 
      concInhib)
      + this.concentrationAlone*selfstart*poly*parent.polConc;

    return debug;
  }

  protected double bothFlux() {
    double concIn = this.from.getConcentration();
    double concOut = this.to.getConcentration();
    double pol = this.parent.isInhibitor(this.to) ? this.polyboth * Constants.displ : 
      this.poly;
    pol*=parent.polConc;
    return Constants.Kduplex * concIn * this.concentrationWithOutput + 
      Constants.Kduplex * concOut * this.concentrationWithInput + 
      this.nick * parent.nickConc * this.concentrationExtended - 
       this.concentrationWithBoth * (((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * stackSlowdown/dangleRSlowdown+ stackSlowdown/dangleLSlowdown* ((Double)this.parent.K.get(this.to)).doubleValue() * Constants.Kduplex + pol);
  }

  protected double extendedFlux() {
    double poldispl = this.parent.isInhibitor(this.to) ? this.polyboth * Constants.displ : 
      this.poly;

   poldispl*=parent.polConc;
    return this.poly * parent.polConc *
      this.concentrationWithInput + 
      poldispl * 
      this.concentrationWithBoth - 
      this.nick * parent.nickConc* this.concentrationExtended;
  }

  protected double inhibitedFlux()
  {
    double krelease;
    double conc;

    if (this.inhib != null) {
      conc = this.inhib.getConcentration();
      krelease = ((Double)this.parent.K.get(this.inhib)).doubleValue() * truealpha * Constants.Kduplex;
    } else {
      conc = 0.0D;
      krelease = 0.0D;
    }

    return -krelease * 
      this.concentrationInhibited 
      - this.concentrationInhibited*Constants.Kduplex*(Constants.ratioToeholdLeft*this.from.getConcentration()+Constants.ratioToeholdRight*this.to.getConcentration())
       + Constants.Kduplex * 
      conc * (
      this.concentrationAlone + this.concentrationWithInput + 
      this.concentrationWithOutput);
  }

  public double getEnzymeUsage(ENZYME type){
	  double value = 0.0;
	  double[] kms = parent.se.enzymeKms[type.value];
	  if (kms[SaturationEvaluator.TALONE] > 0.0){
			value += concentrationAlone/kms[SaturationEvaluator.TALONE];
		}
		if (kms[SaturationEvaluator.TIN] > 0.0){
			value += concentrationWithInput/kms[SaturationEvaluator.TIN];
		}
		if (kms[SaturationEvaluator.TOUT] > 0.0){
			value += concentrationWithOutput/kms[SaturationEvaluator.TOUT];
		}
		if (kms[SaturationEvaluator.TBOTH] > 0.0){
			value += concentrationWithBoth/kms[SaturationEvaluator.TBOTH];
		}	
		if (kms[SaturationEvaluator.TEXT] > 0.0){
			value += concentrationExtended/kms[SaturationEvaluator.TEXT];
		}
		if (kms[SaturationEvaluator.TINHIB] > 0.0){
			value += concentrationInhibited/kms[SaturationEvaluator.TINHIB];
		}
    return value;
  }
  
  public double getPolyUsage()
  {
	 return getEnzymeUsage(ENZYME.POL);
  }
  
  public double getCurrentPoly(){
	  return poly;
  }

  public void setAllocatedPoly(double observedKm) {
	double[] kms = parent.se.enzymeKms[ENZYME.POL.value];
    this.poly = (Constants.polVm)/(kms[SaturationEvaluator.TIN]*observedKm);
    this.polyboth = Constants.polVm/(kms[SaturationEvaluator.TBOTH]*observedKm);
  }

  public double getNickUsage() {
	  return getEnzymeUsage(ENZYME.NICK);
  }
  
  public double getCurrentNick(){
	  return nick;
  }

  public void setAllocatedNick(double observedKm) {
    this.nick = Constants.nickVm/(parent.se.enzymeKms[ENZYME.NICK.value][SaturationEvaluator.TEXT]*observedKm);
  }
  
  
  public double[] getStates() {
    double[] ans;
    if (inhib!=null){
    	ans = new double[] { this.concentrationAlone, this.concentrationWithInput, 
    			this.concentrationWithOutput, this.concentrationWithBoth, 
    			this.concentrationExtended, this.concentrationInhibited };
    } else {
    	ans = new double[] { this.concentrationAlone, this.concentrationWithInput, 
    		    this.concentrationWithOutput, this.concentrationWithBoth, 
    		    this.concentrationExtended};
    }
    return ans;
  }

  public void setStates(double[] values) throws InvalidConcentrationException {
    if (values.length != numberOfStates) {
      System.err
        .println("Error: wrong internal values setting for template " + 
        this);
      return;
    }
    for (int i = 0; i < numberOfStates; i++) {
      if ((values[i] < 0.0D) || (values[i] > this.totalConcentration+1))
      {
        values[i] = 0.0D;
      }
    }

    this.concentrationAlone = values[0];
    this.concentrationWithInput = values[1];
    this.concentrationWithOutput = values[2];
    this.concentrationWithBoth = values[3];
    this.concentrationExtended = values[4];
    if(inhib!=null) this.concentrationInhibited = values[5];
  }

  public SequenceVertex getFrom() {
    return this.from;
  }

  public SequenceVertex getTo() {
    return this.to;
  }
  
  public SequenceVertex getInhib(){
	  return this.inhib;
  }

  public double totalConcentration() {
    return this.concentrationAlone + this.concentrationExtended + 
      this.concentrationInhibited + this.concentrationWithBoth + 
      this.concentrationWithInput + this.concentrationWithOutput;
  }

  public void reset() {
    this.concentrationAlone = this.totalConcentration;
    this.concentrationExtended = 0.0D;
    this.concentrationInhibited = 0.0D;
    this.concentrationWithBoth = 0.0D;
    this.concentrationWithOutput = 0.0D;
    this.concentrationWithInput = 0.0D;
    this.poly = (Constants.polVm / Constants.polKm);
    this.nick = (Constants.nickVm / Constants.nickKm);
  }
  
  public double getLoad(){
	  return this.concentrationExtended+this.concentrationWithBoth+
	  this.concentrationWithInput;
  }

  public String toString()
  {
    return "S" + this.from.ID + "->" + (this.parent.isInhibitor(this.to) ? "I(" + this.parent.inhibitors.get(this.to).toString() + ")" : new StringBuilder("S").append(this.to.ID).toString());
  }
  
  public String getName()
  {
	  return this.from.ID + "->" + this.to.ID;
  }
  
}