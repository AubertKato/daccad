package model.chemicals;

import model.Constants;
import model.OligoGraph;
import model.SaturationEvaluator;
import model.SaturationEvaluator.ENZYME;
import model.chemicals.SequenceVertex;



public class Pseudotemplate<E> extends ModTemplate<E>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public static double outputInvasionRate = 1.0;
	/**
	 * Extended species are supposed to have the same stability than the
	 * normal ones due to the mismatch in the extension. However, they
	 * benefit from an extra stability boost when on their template.
	 */
	public static double outputExtraStab = 1.0/Constants.alpha;

	
	public Pseudotemplate(OligoGraph<SequenceVertex,E> parent, double totalConcentration,
			double dangleL,
			SequenceVertex from, PseudoExtendedSequence to, double inputSlowdown) {
		super(parent, totalConcentration, Constants.baseStack, dangleL, Constants.baseDangleR, from, to,
				null, inputSlowdown);
		numberOfStates = 3;
	}
	
	@Override
	public double[] flux() {
	    double[] ans = { aloneFlux(), inputFlux(),
	      extendedFlux()};
	    return ans;
	  }
	
	public double inputSequenceFlux() {
	    double conc = this.from.getConcentration();
	    double concOut = this.to.getConcentration();
	    
	    double debug = (((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * 
	      dangleLSlowdown+Constants.Kduplex * concOut *outputInvasionRate)*this.concentrationWithInput 
	      -inputSlowdown*Constants.Kduplex * conc * this.concentrationAlone;

	    return debug;
	  }

	  public double outputSequenceFlux() {
	    double conc = this.to.getConcentration();
	    double kto = ((Double)this.parent.K.get(((PseudoExtendedSequence)this.to).originalSequence)).doubleValue();
	    double debug = outputExtraStab*kto * Constants.Kduplex * 
	      this.concentrationExtended 
	      - Constants.Kduplex * conc * (this.concentrationAlone+this.concentrationWithInput*outputInvasionRate);

	    return debug;
	  }
	  
	  
	  @Override
	  protected double aloneFlux() {
	    double concIn = this.from.getConcentration();
	    double concOut = this.to.getConcentration();

	    double debug = ((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex * 
	      dangleLSlowdown*this.concentrationWithInput + 
	      outputExtraStab*((Double)this.parent.K.get(((PseudoExtendedSequence)this.to).originalSequence)).doubleValue() * Constants.Kduplex * 
	      this.concentrationExtended -  this.concentrationAlone * Constants.Kduplex * (inputSlowdown*concIn + concOut);

	    return debug;
	  }

	  @Override
	  protected double inputFlux() {
	    double concIn = this.from.getConcentration();
	    double concOut = this.to.getConcentration();
	    
	    return inputSlowdown*Constants.Kduplex * concIn * this.concentrationAlone
	       - this.concentrationWithInput * (
	      dangleLSlowdown*((Double)this.parent.K.get(this.from)).doubleValue() * Constants.Kduplex 
	      + this.poly*parent.polConc + outputInvasionRate*Constants.Kduplex * concOut);
	  }

	  
	  @Override
	  protected double extendedFlux() {
	    
		  double concOut = this.to.getConcentration();
	   
	    return (this.poly * parent.polConc + outputInvasionRate*Constants.Kduplex * 
	  	      concOut) *
	      this.concentrationWithInput +  this.concentrationAlone * Constants.Kduplex * concOut  - 
	      outputExtraStab*((Double)this.parent.K.get(((PseudoExtendedSequence)this.to).originalSequence)).doubleValue() * Constants.Kduplex * 
	      this.concentrationExtended;
	  }

	  
	  @Override
	  public  double getEnzymeUsage(ENZYME type){
		  double value = 0.0;
		  double[] kms = parent.se.enzymeKms[type.value];
		  if (kms[SaturationEvaluator.TALONE] > 0.0){
				value += concentrationAlone/kms[SaturationEvaluator.TALONE];
		  }
		  if (kms[SaturationEvaluator.TIN] > 0.0){
				value += concentrationWithInput/kms[SaturationEvaluator.TIN];
		  }
		  if (kms[SaturationEvaluator.TEXT] > 0.0){
				value += concentrationExtended/kms[SaturationEvaluator.TEXT];
		  }
		  return value;
	  }


	  public double[] getStates() {
	    double[] ans = { this.concentrationAlone, this.concentrationWithInput, 
	      this.concentrationExtended };
	    return ans;
	  }

	  @Override
	  public void setStates(double[] values) throws InvalidConcentrationException {
	    if (values.length != numberOfStates) {
	      System.err
	        .println("Error: wrong internal values setting for template " + 
	        this);
	      return;
	    }
	    for (int i = 0; i < numberOfStates; i++) {
	      if ((values[i] < 0.0D) || (values[i] > this.totalConcentration+1))
	      {
	        values[i] = 0.0D;
	      }
	    }

	    this.concentrationAlone = values[0];
	    this.concentrationWithInput = values[1];
	    
	    this.concentrationExtended = values[2];
	  }
	  

	  public String toString()
	  {
	    return "Pseudo"+from.ID;
	  }
	  
	  public String getName()
	  {
		  return  "Pseudo"+from.ID;
	  }

}
