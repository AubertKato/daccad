package model.chemicals;

public class Enzyme {

	String name;
	double Vm;
	double[] Km;
	
	public Enzyme(String name, double Vm, double[] Km){
		this.name = name;
		this.Vm = Vm;
		this.Km = Km;
	}
	
	public String toString(){
		String str="";
		for(int i=0; i<Km.length; i++){
			str+=" Km["+i+"]="+Km[i];
		}
		return name+" Vm="+Vm+str;
	}
	
}
