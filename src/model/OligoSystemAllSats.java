package model;

import utils.TemplateFactory;
import model.SaturationEvaluator.ENZYME;
import model.Constants;
import model.OligoGraph;
import model.OligoSystem;
import model.chemicals.SequenceVertex;
import model.chemicals.Template;

/**
 * This class has a much more generic implementation of enzymatic saturation, using the SaturationEvaluator class
 */

public class OligoSystemAllSats<E> extends OligoSystem<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected SaturationEvaluator<E> se;
	
	public OligoSystemAllSats(OligoGraph<SequenceVertex, E> graph){
		super(graph);
	    this.se = graph.se;
	}
	
	public OligoSystemAllSats(OligoGraph<SequenceVertex, E> graph, SaturationEvaluator<E> se) {
		super(graph);
		this.se = se;
		graph.se = se;
		//Make sure that the "default" Kms (not the Kis) are set properly in the constants
		Constants.exoKmSimple = se.enzymeKms[ENZYME.EXO.value][SaturationEvaluator.SIGNAL];
		Constants.exoKmInhib = se.enzymeKms[ENZYME.EXO.value][SaturationEvaluator.INHIB];
		Constants.polKm = se.enzymeKms[ENZYME.POL.value][SaturationEvaluator.TIN];
		Constants.polKmBoth = se.enzymeKms[ENZYME.POL.value][SaturationEvaluator.TBOTH];
		Constants.nickKm = se.enzymeKms[ENZYME.NICK.value][SaturationEvaluator.TEXT];
	}
	
	public OligoSystemAllSats(OligoGraph<SequenceVertex, E> graph, TemplateFactory<E> tf) {
		super(graph,tf);
	    this.se = graph.se;
	}
	
	public OligoSystemAllSats(OligoGraph<SequenceVertex, E> graph, TemplateFactory<E> tf, SaturationEvaluator<E> se) {
		super(graph,tf);
		this.se = se;
		graph.se = se;
		//Make sure that the "default" Kms (not the Kis) are set properly in the constants
		Constants.exoKmSimple = se.enzymeKms[ENZYME.EXO.value][SaturationEvaluator.SIGNAL];
		Constants.exoKmInhib = se.enzymeKms[ENZYME.EXO.value][SaturationEvaluator.INHIB];
		Constants.polKm = se.enzymeKms[ENZYME.POL.value][SaturationEvaluator.TIN];
		Constants.polKmBoth = se.enzymeKms[ENZYME.POL.value][SaturationEvaluator.TBOTH];
		Constants.nickKm = se.enzymeKms[ENZYME.NICK.value][SaturationEvaluator.TEXT];
	}

	@Override
	protected void setObservedPolyKm() {
		double value;
		if(graph.coupling){
			value = se.getCurrentEnzymeSaturation(ENZYME.POL, this);
			//System.out.println("Sat pol: "+value);
			for (Template<E> temp: this.templates.values()){
				//System.out.println("New pol activity: "+Constants.polVm/(Constants.polKm*value));
				//System.out.println("New polboth activity: "+Constants.polVm/(Constants.polKmBoth*value));
				temp.setAllocatedPoly(value);
			}
			} else {
				for (Template<E> temp: this.templates.values()) {
					value =1+ temp.getPolyUsage();
					//perceivedPoly = new Enzyme("poly", this.poly.activity/value, this.poly.basicKm, this.poly.optionalValue);
					temp.setAllocatedPoly(value);
			}
		}
	}
	
	@Override
	protected void setObservedNickKm() {
		double value;
		if(graph.coupling){
			value = se.getCurrentEnzymeSaturation(ENZYME.NICK, this);
			//System.out.println("Sat nick: "+value);
			for (Template<E> temp: this.templates.values()){
				//System.out.println("New pol activity: "+Constants.polVm/(Constants.polKm*value));
				//System.out.println("New polboth activity: "+Constants.polVm/(Constants.polKmBoth*value));
				temp.setAllocatedNick(value);
			}
			} else {
				for (Template<E> temp: this.templates.values()) {
					value =1+ temp.getNickUsage();
					//perceivedPoly = new Enzyme("poly", this.poly.activity/value, this.poly.basicKm, this.poly.optionalValue);
					temp.setAllocatedNick(value);
			}
		}
	}
	
	@Override
	protected double computeExoKm(double myKm){
		double value = se.getCurrentEnzymeSaturation(ENZYME.EXO, this);
		//System.out.println("Sat exo: "+value);
		return (myKm*value);
	}
	
	public void setSaturationEvaluator(SaturationEvaluator<E> se){
		this.se = se;
	}
	
}
