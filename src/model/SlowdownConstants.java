package model;

import model.chemicals.Template;

/**
 * Constants for dangle and stacking slowdowns. Dangle happens when a single strand is attach 
 * to a template. Stacking happens when both the input and output strands are attached to the
 * template. 
 * 
 * We consider that a double-stranded template (no nick) cannot denature. However, this reaction
 * could be added in the future, but the appropriate slowdown would be template-dependent and
 * should be defined in {@link OligoGraph} and {@link Template}. 
 */

public class SlowdownConstants {
	
	/**
	 * Dangle slowdown for an inhibitor on its inhibited template.
	 */
	public static double inhibDangleSlowdown = 0.5;
	
	/**
	 * Static method that takes the two nucleotides at the nick of a template and returns the appropriate
	 * slowdowns. Note that this sequence is from the point of view of the input and output strands, so it
	 * should be the last nucleotide of the input, followed by the first nucleotide of the output.
	 * 
	 * Nucleotides are noted with a capital letter: A, T, G or C.
	 * @param s the two nucleotides sequence at the nick.
	 * @return an array of length three: stacking slowdown, dangle slowdown for the input and dangle
	 * slowdown for the output.
	 */
	public static double[] getSlowdown(String s){
		double[] values = {Constants.baseStack,Constants.baseDangleL,Constants.baseDangleR}; //default
		
		if(s.equals("AA")){
			values[0] = 0.16;
			values[1] = 0.93;
			values[2] = 0.55;
		}
		
		if(s.equals("AT")){ 
			values[0] = 0.12;
			values[1] = 0.45;
			values[2] = 1.31;
		}
		
		if(s.equals("AC")){
			values[0] = 0.04;
			values[1] = 2.32;
			values[2] = 0.65;
		}
		
		if(s.equals("AG")){
			values[0] = 0.16;
			values[1] = 0.99;
			values[2] = 0.47;
		}
		
		//J'en suis la
		
		if(s.equals("TA")){
			values[0] = 1.2;
			values[1] = 0.39;
			values[2] = 0.49;
		}
		
		if(s.equals("TT")){ 
			values[0] = 0.16;
			values[1] = 0.43;
			values[2] = 0.78;
		}
		
		if(s.equals("TC")){
			values[0] = 0.16;
			values[1] = 0.39;
			values[2] = 0.25;
		}
		
		if(s.equals("TG")){
			values[0] = 0.62;
			values[1] = 0.50;
			values[2] = 0.33;
		}
		
		if(s.equals("CA")){
			values[0] = 0.62;
			values[1] = 0.41;
			values[2] = 0.48;
		}
		
		if(s.equals("CT")){ 
			values[0] = 0.16;
			values[1] = 0.42;
			values[2] = 1.05;
		}
		
		if(s.equals("CC")){
			values[0] = 0.12;
			values[1] = 0.48;
			values[2] = 0.55;
		}
		
		if(s.equals("CG")){
			values[0] = 0.19;
			values[1] = 0.67;
			values[2] = 1.13;
		}
		
		if(s.equals("GA")){
			values[0] = 0.16;
			values[1] = 0.41;
			values[2] = 0.62;
		}
		
		if(s.equals("GT")){ 
			values[0] = 0.04;
			values[1] = 0.23;
			values[2] = 1.43;
		}
		
		if(s.equals("GC")){
			values[0] = 0.04;
			values[1] = 0.33;
			values[2] = 0.76;
		}
		
		if(s.equals("GG")){
			values[0] = 0.12;
			values[1] = 0.50;
			values[2] = 0.65;
		}
		
		if(s.equals("Custom")){
			return null;
		}
		
		return values;
	}

}
