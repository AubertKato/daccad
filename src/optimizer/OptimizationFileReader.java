package optimizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class OptimizationFileReader {
	
	
	@SuppressWarnings("resource") //All readers are closed
	public static ArrayList<optimizer.OptimizerCMAES.ReferencePoint> readFile(int index, File file){
		ArrayList<optimizer.OptimizerCMAES.ReferencePoint> points = new ArrayList<optimizer.OptimizerCMAES.ReferencePoint>();
		FileReader in = null;
		BufferedReader reader = null;
		try {
			in = new FileReader(file);
			reader = new BufferedReader(in);
			String line = null;
			line = reader.readLine();
			String[] split;
			while (line != null){
				split = line.split("\t");
				if(split.length==2){
					points.add(new optimizer.OptimizerCMAES.ReferencePoint(index, Integer.parseInt(split[0]), Double.parseDouble(split[1])));
				} else {
					JOptionPane.showMessageDialog(null, "Unreadable file");
					return new ArrayList<optimizer.OptimizerCMAES.ReferencePoint>();
				}
				line = reader.readLine();
			}
			reader.close();
			in.close();
		} catch (Exception e){
			JOptionPane.showMessageDialog(null, "Unreadable file");
			
			e.printStackTrace();
		}
		//if(reader != null) reader.close();
		return points;
	}
	
	public static void saveFile(ArrayList<optimizer.OptimizerCMAES.ReferencePoint> points, File file){
		FileWriter fw;
		try {
			fw = new FileWriter(file);
			String res = "";
			for(optimizer.OptimizerCMAES.ReferencePoint point : points){
				res+=point.time+"\t"+point.target+"\n";
			}
			fw.write(res);
			fw.flush();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
}
