package optimizer;

public class Constants {

	public static double sigma = 1e-1;
	public static int modifiedParams = 2;
	public static double variance = 2;
	public static double tmax = 1;
	public static double thresholdRatio = 2; //The higher, the more precision
	public static double valueMin = 0;
	public static double valueMax = 100;
}
