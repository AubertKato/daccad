package cli;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import model.Constants;
import model.OligoGraph;
import model.OligoSystemAllSats;
import model.chemicals.SequenceVertex;
import utils.GraphJsonDeserializer;


/**
 * Convenience class for requesting a simulation.
 * @author N. Aubert-Kato
 *
 */
public class CLIEvaluator {
	
	public static String outputSuffix = ".dat";

	public static void main(String[] args) {
		
		if(args.length >= 2){
			//String outputfilename = args[1].split("\\.")[0]+outputSuffix;//we don't want the graph or txt extension
			OligoGraph<SequenceVertex,String> graph = GraphJsonDeserializer.getOligograph(args[1]);
			try {
				BufferedReader br = new BufferedReader(new FileReader(args[0]));
				String line;
				
				while((line = br.readLine()) != null){
					String[] paramPair = line.split("\\s*=\\s*");
					String trimmedParamName = paramPair[0].trim();
					if(trimmedParamName.startsWith("#") || paramPair.length != 2) continue; //Incorrect format or comment
					else {
						Constants.readConfigFromString(Constants.class,trimmedParamName, paramPair[1]);
				
					}
					
				}
				br.close();
				
				

			} catch (FileNotFoundException e) {

				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			OligoSystemAllSats oc = new OligoSystemAllSats(graph);
			double[][] results = oc.calculateTimeSeries(null);
			
			for(int i = 0; i<results[0].length; i++) {
				StringBuilder sb = new StringBuilder("");
				for(int j = 0; j<results.length-1; j++) {
					sb.append(results[j][i]+", ");
				}
				sb.append(results[results.length-1][i]);
				System.out.println(sb.toString());
			}
	    }
		
	}

}
