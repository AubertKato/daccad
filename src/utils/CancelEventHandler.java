package utils;

import org.apache.commons.math3.ode.events.EventHandler;

public class CancelEventHandler implements EventHandler {

	public int fire = Short.MAX_VALUE;
	
	@Override
	public Action eventOccurred(double arg0, double[] arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return EventHandler.Action.STOP;
	}

	@Override
	public double g(double t, double[] arg1) {
		// TODO Auto-generated method stub
		return fire - t;
	}

	@Override
	public void init(double arg0, double[] arg1, double arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resetState(double arg0, double[] arg1) {
		// TODO Auto-generated method stub

	}

}
