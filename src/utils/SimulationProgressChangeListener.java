package utils;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JProgressBar;

public class SimulationProgressChangeListener implements PropertyChangeListener {

	private JProgressBar myBar;
	
	public SimulationProgressChangeListener(JProgressBar bar){
		this.myBar = bar;
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		if(arg0.getPropertyName() == "progress"){
			//System.err.println("Fu yeah");
			this.myBar.setValue((Integer)arg0.getNewValue());
		}

	}

}
