package utils;

import model.OligoGraph;
import model.PseudoTemplateGraph;
import model.chemicals.PseudoExtendedSequence;
import model.chemicals.SequenceVertex;

public class GraphUtils {
	
		  public static OligoGraph<SequenceVertex, String> initGraph(){
		    OligoGraph<SequenceVertex, String> g = new OligoGraph<SequenceVertex,String>();
		      g.initFactories(new VertexFactory<SequenceVertex>(g){
		        
		      public SequenceVertex create() {
		        SequenceVertex newvertex = associatedGraph.popAvailableVertex();
		        if (newvertex == null){
		          newvertex = new SequenceVertex(associatedGraph.getVertexCount() + 1);
		        } else {
		          newvertex = new SequenceVertex(newvertex.ID);
		        }
		        return newvertex;
		      }

		      @Override
		      public SequenceVertex copy(SequenceVertex original) {
		         SequenceVertex ret = new SequenceVertex(original.ID);
		         ret.inputs = original.inputs;
		         return ret;
		      }   
		      }, new EdgeFactory<SequenceVertex,String>(g){
		        public String createEdge(SequenceVertex v1, SequenceVertex v2){
		          return v1.ID+"->"+v2.ID;
		        }
		        public String inhibitorName(String s){
		          return "Inhib"+s;
		        }
		      });
		      return g;
		  }
		  
		  public static OligoGraph<SequenceVertex, String> initPseudoTemplateGraph(){
				PseudoTemplateGraph<SequenceVertex, String> g = new PseudoTemplateGraph<SequenceVertex,String>();

			    g.initFactories(new VertexFactory<SequenceVertex>(g){
			    	
					public SequenceVertex create() {
						SequenceVertex newvertex = associatedGraph.popAvailableVertex();
						if (newvertex == null){
							newvertex = new SequenceVertex(associatedGraph.getVertexCount() + 1);
						} else {
							newvertex = new SequenceVertex(newvertex.ID);
						}
						return newvertex;
					}

					@Override
					public SequenceVertex copy(SequenceVertex original) {
						 SequenceVertex ret = new SequenceVertex(original.ID);
						 ret.inputs = original.inputs;
						 return ret;
					} 	
			    }, new EdgeFactory<SequenceVertex,String>(g){
			    	public String createEdge(SequenceVertex v1, SequenceVertex v2){
			    		if(PseudoExtendedSequence.class.isAssignableFrom(v2.getClass())){
			    			return "Pseudo"+v1.ID;
			    		}
			    		return v1.ID+"->"+v2.ID;
			    	}
			    	public String inhibitorName(String s){
			    		return "Inhib"+s;
			    	}
			    });
			    return g;
			}
}
