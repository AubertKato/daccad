package utils;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComboBox;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;

public class MyChartMouseListener extends MouseAdapter implements ChartMouseListener {

	protected boolean isOn = false;
	protected ChartPanel panel;
	protected JComboBox myCombo;
	
	MyChartMouseListener(ChartPanel panel,JComboBox myCombo){
		this.panel = panel;
		this.myCombo = myCombo;
	}
	@Override
	public void chartMouseClicked(ChartMouseEvent arg0) {
		
		if(isOn){
			//Register point
			
			
			((PlotExpData)panel.getParent()).addPoint(arg0.getTrigger().getX(),arg0.getTrigger().getY(),myCombo.getSelectedIndex());
			
			
			}
		
		panel.getParent().repaint();
	}

	@Override
	public void chartMouseMoved(ChartMouseEvent arg0) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void mouseReleased(MouseEvent arg0){
		panel.getParent().repaint();
	}
	
	public void toggle(boolean bool){
		this.isOn = bool;
	}
	
	public boolean isOn(){
		return this.isOn;
	}

}
