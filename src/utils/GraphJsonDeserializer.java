package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import model.OligoGraph;
import model.chemicals.SequenceVertex;

/**
 * Deserializer for Json graphs, compatible with the BioNEAT format.
 * For now limited to basic graphs
 * @author N. Aubert-Kato
 *
 */
public class GraphJsonDeserializer implements JsonDeserializer<OligoGraph<SequenceVertex,String>>{

	@Override
	public OligoGraph<SequenceVertex, String> deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		OligoGraph<SequenceVertex, String> graph = GraphUtils.initGraph();
		JsonObject jobject = json.getAsJsonObject();
		
		JsonArray nodes = jobject.getAsJsonArray("nodes");
		HashMap<String,SequenceVertex> nameEq = new HashMap<String,SequenceVertex>();
		//We need to remember inhibitors for properly setting up inhibitions
		HashMap<SequenceVertex,String> inhibitors = new HashMap<SequenceVertex, String>();
		for (JsonElement jsonNode : nodes) {
			JsonObject jobj = jsonNode.getAsJsonObject();
			SequenceVertex s = graph.getVertexFactory().create();
			if(jobj.get("type").getAsInt() == 2) {
				//is an inhibitor
				s.setInhib(true);
				inhibitors.put(s, jobj.get("name").getAsString());
			}
			//register s
			nameEq.put(jobj.get("name").getAsString(), s);
			graph.addSpecies(s, jobj.get("parameter").getAsDouble());
			s.initialConcentration = jobj.get("initialConcentration").getAsDouble();
			//Other parameters are ignored for now
		}

		JsonArray connections = jobject.getAsJsonArray("connections");
		for (JsonElement jsonConnetion : connections) {
			//Those are templates
			JsonObject jsonConnectionObject = jsonConnetion.getAsJsonObject();
			if(jsonConnectionObject.get("enabled").getAsBoolean()) {
				SequenceVertex from = nameEq.get(jsonConnectionObject.get("from").getAsString());
				SequenceVertex to = nameEq.get(jsonConnectionObject.get("to").getAsString());
				graph.addActivation(graph.getEdgeFactory().createEdge(from, to), from, to,
						jsonConnectionObject.get("parameter").getAsDouble());
			}
			
		}
		
		for(SequenceVertex inhib : inhibitors.keySet()) {
			//Current format: I s1 T s2
			String[] seqNames = inhibitors.get(inhib).substring(1).split("T");
			graph.addInhibition(graph.getEdgeFactory()
					.createEdge(nameEq.get(seqNames[0]), nameEq.get(seqNames[1])), inhib);
		}

		// Enzymatic parameters ignored for now TODO
		return graph;
	}
	
	@SuppressWarnings("unchecked")
	public static OligoGraph<SequenceVertex,String> getOligograph(String filename){
		Gson gson = new GsonBuilder().registerTypeAdapter(OligoGraph.class, new GraphJsonDeserializer()).create();
		OligoGraph<SequenceVertex,String> graph = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			graph = (OligoGraph<SequenceVertex,String>) gson.fromJson(br, OligoGraph.class);
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return graph;
	}
	
	
	public static void main(String[] args) {
		OligoGraph<SequenceVertex,String> graph = getOligograph(args[0]);
		
		if(graph != null) {
			System.out.println(graph);
			System.out.println(graph.getInhibitions());
		}
	}

}
