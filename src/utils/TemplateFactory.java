package utils;

import model.chemicals.Template;

public abstract class TemplateFactory<E> implements Cloneable {

	public abstract Template<E> create(E edge);
	
}
