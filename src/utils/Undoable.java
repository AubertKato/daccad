package utils;

public interface Undoable {

	public void undo();
}
