package utils;

import model.Constants;
import model.PseudoTemplateGraph;
import model.chemicals.ModTemplate;
import model.chemicals.PseudoExtendedSequence;
import model.chemicals.Pseudotemplate;
import model.chemicals.SequenceVertex;
import model.chemicals.Template;
import model.chemicals.TemplateWithModSpecies;

public class PseudoTemplateCapableTemplateFactory<E> extends DefaultTemplateFactory<E> {

	public PseudoTemplateCapableTemplateFactory(PseudoTemplateGraph<SequenceVertex, E> graph) {
		super(graph);
	}
	
	@Override
	public Template<E> create(E e) {
		if(!PseudoTemplateGraph.class.isAssignableFrom(graph.getClass())){
			//Somehow the graph value was changed and is now not correct.
			//Return default
			System.out.println("WARNING: incompatible graph class");
			return super.create(e);
		}
		
		PseudoTemplateGraph<SequenceVertex,E> gr = (PseudoTemplateGraph<SequenceVertex,E>) graph; //for convenience
		//Case 1: pseudoTemplate
		
		if(gr.isPseudoTemplate(e)){
			SequenceVertex input = gr.getPseudoTemplateInput(e);
			
			return new Pseudotemplate<E>(gr,(double) gr.getTemplateConcentration(e), gr.getDangleL(e),input,
					((PseudoExtendedSequence) gr.getExtendedSpecies(input)),gr.getInputSlowdown(e));
		}
		
		double dangleL = graph.dangle?graph.getDangleL(e):Constants.baseDangleL;
		double dangleR = graph.dangle?graph.getDangleR(e):Constants.baseDangleR;
		SequenceVertex from = graph.getSource(e);
		SequenceVertex to = graph.getDest(e);
		SequenceVertex inhib = (graph.getInhibition(e)==null?null:graph.getInhibition(e).getLeft());
		
		//Case 2: template with potentially extended species
		if(gr.hasPseudoTemplate(from)||gr.hasPseudoTemplate(to)){
			return new TemplateWithModSpecies<E>(graph, graph.getTemplateConcentration(e), graph.getStacking(e), dangleL, dangleR,
					from, to, inhib, (PseudoExtendedSequence) gr.getExtendedSpecies(from), (PseudoExtendedSequence) gr.getExtendedSpecies(to),gr.getInputSlowdown(e));
		}
		
		//Default
		return new ModTemplate<E>(graph, graph.getTemplateConcentration(e), graph.getStacking(e), dangleL, dangleR, 
				graph.getSource(e), graph.getDest(e), (graph.getInhibition(e)==null?null:graph.getInhibition(e).getLeft()), gr.getInputSlowdown(e));
	}

}
