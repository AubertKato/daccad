package utils;

import model.OligoGraph;

public abstract class VertexFactory<V> implements Cloneable {

	public OligoGraph<V,?> associatedGraph;
	
	public VertexFactory(OligoGraph<V,?> graph){
		this.associatedGraph = graph;
	}
	
	public abstract V copy(V original);
	public abstract V create();
}
