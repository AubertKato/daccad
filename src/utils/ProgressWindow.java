package utils;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JProgressBar;

import optimizer.OptimizerCMAES;

/**
*
* @author dextra
*/
public class ProgressWindow extends javax.swing.JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private OptimizerCMAES opti;
	
   public ProgressWindow(OptimizerCMAES opti) {
       this.opti = opti;
	   initComponents();   
   }

   
  
   private void initComponents() {

       jScrollPane = new javax.swing.JScrollPane();
       jTextArea = new javax.swing.JTextArea();
       jPanelProgress = new javax.swing.JPanel();
       //jProgressBar1 = new javax.swing.JProgressBar();
       PlotExpData = new javax.swing.JPanel();

       //setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

       jTextArea.setColumns(20);
       jTextArea.setRows(5);
       jTextArea.setEditable(false);
       jScrollPane.setViewportView(jTextArea);

       jPanelProgress.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
     
       javax.swing.BoxLayout jPanelBoxLayout = new javax.swing.BoxLayout(jPanelProgress,javax.swing.BoxLayout.Y_AXIS);
       jPanelProgress.setLayout(jPanelBoxLayout);

       javax.swing.GroupLayout PlotExpDataLayout = new javax.swing.GroupLayout(PlotExpData);
       PlotExpData.setLayout(PlotExpDataLayout);
       PlotExpDataLayout.setHorizontalGroup(
           PlotExpDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGap(0, 158, Short.MAX_VALUE)
       );
       PlotExpDataLayout.setVerticalGroup(
           PlotExpDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGap(0, 0, Short.MAX_VALUE)
       );

       javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
       getContentPane().setLayout(layout);
       layout.setHorizontalGroup(
           layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGroup(layout.createSequentialGroup()
               .addContainerGap()
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                   .addComponent(jScrollPane)
                   .addGroup(layout.createSequentialGroup()
                       .addComponent(jPanelProgress, 200, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                       .addComponent(PlotExpData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                       .addGap(0, 0, Short.MAX_VALUE)))
               .addContainerGap())
       );
       layout.setVerticalGroup(
           layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
               .addContainerGap()
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                   .addComponent(jPanelProgress, 200, 200, Short.MAX_VALUE)
                   .addComponent(PlotExpData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
               .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
               .addComponent(jScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addContainerGap())
       );

       this.addWindowListener(new WindowListener(){

		@Override
		public void windowActivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosed(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosing(WindowEvent arg0) {
		      opti.stopNow();
			
		}

		@Override
		public void windowDeactivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowOpened(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}
    	   
       });
       
       pack();
   }

   /**
    * @param args the command line arguments
    */
   public static void main(String args[]) {
       /* Set the Nimbus look and feel */
       //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
       /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
        * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
        */
       try {
           for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
               if ("Nimbus".equals(info.getName())) {
                   javax.swing.UIManager.setLookAndFeel(info.getClassName());
                   break;
               }
           }
       } catch (ClassNotFoundException ex) {
           java.util.logging.Logger.getLogger(ProgressWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
       } catch (InstantiationException ex) {
           java.util.logging.Logger.getLogger(ProgressWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
       } catch (IllegalAccessException ex) {
           java.util.logging.Logger.getLogger(ProgressWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
       } catch (javax.swing.UnsupportedLookAndFeelException ex) {
           java.util.logging.Logger.getLogger(ProgressWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
       }
       //</editor-fold>

       /* Create and display the form */
       java.awt.EventQueue.invokeLater(new Runnable() {
           public void run() {
              ProgressWindow pw = new ProgressWindow(null);
              pw.setVisible(true);
              JProgressBar bar1 = new JProgressBar(0,100);
  			  bar1.setValue(0);
              pw.jPanelProgress.add(bar1);
              JProgressBar bar2 = new JProgressBar(0,100);
  			  bar2.setValue(10);
              pw.jPanelProgress.add(bar2);
              JProgressBar bar3 = new JProgressBar(0,100);
  			  bar3.setValue(50);
              pw.jPanelProgress.add(bar3);
              
           }
       });
   }
   // Variables declaration - do not modify
   private javax.swing.JPanel PlotExpData;
   public javax.swing.JPanel jPanelProgress;
   //private ArrayList<javax.swing.JProgressBar> jProgressBars;
   private javax.swing.JScrollPane jScrollPane;
   public javax.swing.JTextArea jTextArea;
   // End of variables declaration
}
