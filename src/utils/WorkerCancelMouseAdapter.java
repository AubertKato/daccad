package utils;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.Future;

public class WorkerCancelMouseAdapter extends MouseAdapter {

	private Future<?> worker;

	public WorkerCancelMouseAdapter(Future<?> worker){
		this.worker = worker;
	}
	
	@Override
	public void mouseClicked(MouseEvent e){
		
		worker.cancel(true);
	}
}
