package utils;

import graphical.animation.AnimatedSequences;

import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;

import model.OligoGraph;
import model.OligoSystem;
import model.chemicals.SequenceVertex;

import org.apache.commons.math3.ode.FirstOrderIntegrator;

/**
 * Unused class
 * @author
 *
 */

public class MyEvalutationWorker extends SwingWorker implements PluggableWorker{

	private OligoSystem syst;
	
	
	public MyEvalutationWorker(OligoSystem syst){
		this.syst = syst;
	}
	
	@Override
	protected Object doInBackground() throws Exception {
		setProgress(0);
		//JFrame plotPanel = new JFrame();
		return  syst.calculateTimeSeries(this);
	
	}
	
	@Override
	protected void done(){
		
	}
	
	public void setCustomProgress(int time){
		//System.out.println(time);
		this.firePropertyChange("progress", time-1, time);
		//setProgress(time);
		//cop.setProgress(time);
	}

	@Override
	public void cancel() {
		super.cancel(true);
	}

}
