package utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

import model.OligoGraph;
import model.OligoSystem;
import model.chemicals.SequenceVertex;

public class BifurcationDiagram {
	
	//Bifurcation at steady state
	
	Color state1 = Color.BLUE; //Condition 1 filled
	Color state2 = Color.ORANGE; //Condition 2 filled
	Color statenone = Color.BLACK; // In the case there is nothing
	
	double step = 10.0; // in nM
	
	double rangemin = 0.0;
	double rangemax = 200;
	
	String target1 = "1->1";
	String target2 = "2->2";
	
	int size = (int) Math.round((rangemax-rangemin)/step) +1;
	
	Color[][] diagram = new Color[size][size];
	
	JFileChooser fc = new JFileChooser();
	
	private void initGraph(OligoGraph<SequenceVertex,String> g){
		g.initFactories(new VertexFactory<SequenceVertex>(g){
	    	
			public SequenceVertex create() {
				SequenceVertex newvertex = associatedGraph.popAvailableVertex();
				if (newvertex == null){
					newvertex = new SequenceVertex(associatedGraph.getVertexCount() + 1);
				} else {
					newvertex = new SequenceVertex(newvertex.ID);
				}
				return newvertex;
			}

			@Override
			public SequenceVertex copy(SequenceVertex original) {
				 SequenceVertex ret = new SequenceVertex(original.ID);
				 ret.inputs = original.inputs;
				 return ret;
			} 	
	    }, new EdgeFactory<SequenceVertex,String>(g){
	    	public String createEdge(SequenceVertex v1, SequenceVertex v2){
	    		return v1.ID+"->"+v2.ID;
	    	}
	    	public String inhibitorName(String s){
	    		return "Inhib"+s;
	    	}
	    });
	}
	
	public static void main(String[] args){
		BifurcationDiagram bifu = new BifurcationDiagram();
		OligoGraph<SequenceVertex,String> target = new OligoGraph<SequenceVertex,String>();
		bifu.initGraph(target);
		
		int result = bifu.fc.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION){
			File open = bifu.fc.getSelectedFile();
			CodeGenerator.openGraph(target, null, open);
			
			//System.out.println(target.getTemplateConcentration(bifu.target2));
			target.setTemplateConcentration(bifu.target1, bifu.rangemin);
			target.setTemplateConcentration(bifu.target2, bifu.rangemin);
			
			for(int i = 0; i<bifu.size; i++){
				for(int j = 0; j<bifu.size; j++){
					target.setTemplateConcentration(bifu.target2, target.getTemplateConcentration(bifu.target2)+bifu.step);
					OligoSystem model = new OligoSystem(target);
					double[][] res = model.calculateTimeSeries(null);
					int pos1 = Arrays.asList(model.giveNames()).indexOf("s1");
					int pos2 = Arrays.asList(model.giveNames()).indexOf("s2");
					if(pos1 == -1){
						pos1 = 0;
					}
					if(pos2 == -1 || pos2==pos1){
						pos2 = 1;
					}
					Color pixel1 = (res[pos1][res[pos1].length-1]>res[pos1][0]?bifu.state1:bifu.statenone);
					Color pixel2 = (res[pos2][res[pos2].length-1]>res[pos2][0]?bifu.state2:bifu.statenone);
					float[] rgb1 = pixel1.getRGBComponents(null);
					float[] rgb2 = pixel2.getRGBComponents(null);
					bifu.diagram[i][j] = new Color(rgb1[0]+rgb2[0],rgb1[1]+rgb2[1],rgb1[2]+rgb2[2]);
				}
				target.setTemplateConcentration(bifu.target2, bifu.rangemin);
				target.setTemplateConcentration(bifu.target1, target.getTemplateConcentration(bifu.target1)+bifu.step);
			}
			
			JFrame visual = new JFrame("Bifurcation Diagram");
			JPanel colors = new JPanel();
			colors.setLayout(new GridLayout(bifu.size,bifu.size));
			for(int i = 0; i<bifu.size; i++){
				for(int j = 0; j<bifu.size; j++){
					JButton truc = new JButton();
					truc.setPreferredSize(new Dimension(100,100));
					truc.setBackground(bifu.diagram[i][j]);
					colors.add(truc);
				}
			}
			visual.add(colors);
			visual.pack();
			visual.setVisible(true);
		}
	}

}
