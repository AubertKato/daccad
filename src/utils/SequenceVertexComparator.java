package utils;

import java.util.Comparator;

import model.chemicals.SequenceVertex;

public class SequenceVertexComparator implements Comparator<SequenceVertex> {

	@Override
	public int compare(SequenceVertex o1, SequenceVertex o2) {
		return o1.ID - o2.ID;
	}

}
