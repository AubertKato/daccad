package utils;

import model.Constants;
import model.OligoGraph;
import model.chemicals.SequenceVertex;
import model.chemicals.Template;

public class DefaultTemplateFactory<E> extends TemplateFactory<E> {

	protected OligoGraph<SequenceVertex,E> graph;
	
	public DefaultTemplateFactory(OligoGraph<SequenceVertex,E> graph){
		this.graph = graph;
	}
	
	@Override
	public Template<E> create(E e) {
		double dangleL = graph.dangle?graph.getDangleL(e):Constants.baseDangleL;
		double dangleR = graph.dangle?graph.getDangleR(e):Constants.baseDangleR;
		return new Template<E>(graph,graph.getTemplateConcentration(e),graph.getStacking(e), dangleL, dangleR, graph.getSource(e),graph.getDest(e),(graph.getInhibition(e)==null?null:graph.getInhibition(e).getLeft()));
	}

}
