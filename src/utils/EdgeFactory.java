package utils;

import model.OligoGraph;

public abstract class EdgeFactory<V,E> implements Cloneable{

	public OligoGraph<V,E> associatedGraph;
	
	public EdgeFactory(OligoGraph<V,E> graph){
		this.associatedGraph = graph;
	}
	
	public abstract E createEdge(V v1, V v2);
	
	public abstract E inhibitorName(E e);
	
}
