package utils;

public interface PluggableWorker {

	
	public void setCustomProgress(int time);
	public void cancel();
}
